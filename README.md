# BSSP BIS15
## Copies

- [bitbucket](https://bitbucket.org/_clemo_/bssp-bis15/) 
- [gitlab](https://git.nwt.fhstp.ac.at/is151014/bssp-bis15)

## Contents
### ue1
Contains a Backup and Restore-Programm.

These Programms use their own data-format: 

```
┏━━━━━━━━━━━━━━━━━━━━━━━ MAGIC ━━━━━━━━━━━━━━┳━━━━━━━━━━━┯━━━━━ BLOCK ━━━━━━┯━━━━━━━━━━━━━━━━━━━┓
┃ OWNER-Surename without \0│ OWNER-TIMNESTAMP┃ INODE-DATA│ FILEPATH with \0 │ PAYLOAD (optional)┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━┷━━━━━━━━━━━━━━━━━┻━━━━━━━━━━━┷━━━━━━━━━━━━━━━━━━┷━━━━━━━━━━━━━━━━━━━┛
```

The Inode-Data preserves File-Mode/File-Type and the Filename. The FILEPATH-Filed preservers the whole Path.  
A File consists at least of MAGIC and one Block, but is not limitted to any Block-Count.

Files are located in `backuo_and_resotre_ue1`.

### ue2
Contains a shell.

The shell was later extended to a shell-server. So the Shell is a Shell and also reachable over the network!

Since this was a groupwork with Fabifri &rarr; it has his Port number and he is listed in info aswell.

Files are located in `nnnice_shell_ue1`

### ue3
Constists of a chat server and client.

My version is stored as `chat_rewritten` and uses its own message-format:

```
┏━━━━━━━━━━━┯━━━━━━━━━━━ META ━━━━━━━━━┳━━━━━━━ PAYLOAD ━━━━━━━━━┓
┃ TIMESTAMP │ USER-SIZE │ PAYLOAD-SIZE ┃ USERNAME \0│ MESSAGE \0 ┃
┗━━━━━━━━━━━┷━━━━━━━━━━━┷━━━━━━━━━━━━━━┻━━━━━━━━━━━━┷━━━━━━━━━━━━┛
```

Fabifri's version is stored as `ue3_group` and uses simple messages (simple NULL-Terminated string).

1. send a message with your username
2. send the messages

The `chat_rewritten`-clients ask it you want to use the default mode. If you answer (type in anything) it is compartible with fabifri.

Todo: f there is tiem there will be another server programm which is much simpler

### ue4
Constists of `shell_rewritten` which is a reworked-verson of ue2 and `ue4_group` which contains the "trojan".

The rewritten version consists of two versions:

- `shell_server.c` which works with processes
- `threador.c` which works with threads

The logfile-location was changed to temp-dir.

Files are located in `shell_rewritten`.  
The trojan can be found in the `trojan_ue4` directory.

### ue5
Consists of copies of itself in various versions:

- A: `a_clemens`: Two programms talking over a message-queue
- B: `b_clemens`: Two programms talking over a shared memory
- C: `c_clemens`: One programm is talking to N-other programms over shared memory
- D: `d_clemens`: One programmes is talking over a message-queue with another programm which converts the text to uppercase and sends it over a named pipe to a third programm
- E: `e_clemens`: On programm talking over a message-queue to an other programm which converts text to lower-case and sinds it over one pipe and to upper-case on a second pipe.

A and B were programmed in the lecture.

### ue6, ue7, ue8
is a kernel-module and is documented using doxygen.

Files are located in `kernel_module`.
