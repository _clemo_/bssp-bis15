#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#define BUFFSIZE 1024

int read_block(int fd);
int CheckMagic(int fd);

typedef struct stat stat_t;

int main(int argc, const char *argv[])
{
	// CHECK INPUTS
	if (argc == 1) {
		fprintf(stderr, "Please give me some filnames!\n");
		return 0;
	}

	// open archive
	int arg_num;
	for (arg_num = 1; arg_num < argc; ++arg_num){

		int archive_fd = open(argv[arg_num], O_RDONLY);
		if (archive_fd < 0) {
			fprintf(stderr, "Could not open file %s\n", argv[arg_num]);
			exit(1);
		}

		umask(0000); // remove umask to ensuer that we set the right mode

		int magic = CheckMagic(archive_fd);
		if (magic == -1){
				fprintf(stderr, "courld not read from FD\n");
				exit(2);
		}else if(magic == 0){
				printf("Wrong Archive format!\n");
				exit(1);
		}else{
				printf("Found correct magic! JUNG%08x\n", (unsigned int)magic);
		}

		while(read_block(archive_fd) > 0 )
			;
		close(archive_fd);
	}

	return 0;
}


/**
 * @brief   read_block block from fd and extract data
 * @param   fd filedescritor to read
 * @returns -1 for errot, 0 for no block read, 1 successully read block
 */
int read_block(int fd){
	if(fd < 0) return -1;

	// read block
	//   read inode
	stat_t target_struct;
	ssize_t res = read(fd, &target_struct, sizeof(target_struct));
	if( res < 0 || (res > 0 && ((size_t)res) < sizeof(target_struct))) {
		fprintf(stderr, "Could not read file from FD %d\n", fd);
		return -1;
	}else if(res == 0){ // wenn EOF
		return 0;
	}

	//   read filename
	char filename[BUFFSIZE];
	size_t used = 0;
	ssize_t read_bytes = 0;
	do{
		read_bytes = read(fd, &filename[used], 1);
		used += 1;
	}while(
			read_bytes > 0 &&            // solange nichtfd kaputt
			filename[used - 1] != '\0' &&  // bis zum 0
			used < BUFFSIZE              // und noch im Buffer
	);
	printf("[DBG] found filename %s\n", filename);

	if (read_bytes < 0) {
		fprintf(stderr, "Error while reading FD %d\n", fd);
		return -1;
	}else if ( used >= BUFFSIZE ) {
		filename[BUFFSIZE-1] = '\0';
		fprintf(stderr, "Filename larger than buffer! -> skipping file %s \n", filename);
		return -1;
	}

	if( S_ISREG(target_struct.st_mode) ){
		printf("[DBG] create file %s\n", filename);

		if ( target_struct.st_nlink > 1 ){
			fprintf(stderr, "ATTENTION: file %s is no longer a hardlink!\n", filename);
		}
		int new_file_fd = creat(filename, target_struct.st_mode);
		if ( new_file_fd < 0 ) {
			fprintf(stderr, "Could not create file %s\n", filename);
			return -1;
		}

		printf("[DBG] read %zd bytes payload\n", target_struct.st_size);

		char read_buffer[BUFFSIZE];
		size_t to_write, remaining_bytes, written_bytes = 0;
		do{
			remaining_bytes = target_struct.st_size - written_bytes;
			to_write = BUFFSIZE < remaining_bytes ? BUFFSIZE : remaining_bytes;
			printf("[DBG] chunk-size: %zd (read: %zd)\n", to_write, written_bytes);
			if( read(fd, read_buffer, to_write) != ((ssize_t)to_write) ){
				fprintf(stderr, "Could not read enough bytes!\n");
				return -1;
			}
			if ( write(new_file_fd, read_buffer, to_write) != ((ssize_t)to_write) ){
				fprintf(stderr, "Could not wirte all bytes to file %s\n", filename);
				return -1;
			}
			written_bytes += to_write;
		}while( ((ssize_t)written_bytes) < target_struct.st_size);
		close(new_file_fd);
		printf("file %s restored\n" , filename);
	}else if( S_ISDIR(target_struct.st_mode) ){
		printf("[DBG] process dir %s\n", filename);
		if (mkdir(filename, target_struct.st_mode) < 0 ){
			fprintf(stderr, "Could not create directory %s with mode: %o\n\t%s\n",
					filename, target_struct.st_mode,
					strerror(errno));
		}else{
			printf("directory %s restored\n", filename);
		}
	}else{
		fprintf(stderr, "Filetype not supported! (%s, %o)\n",
				filename, target_struct.st_mode);
		return 1;
	}
	// TODO owner und gruppe setzten!
	return 1;
}

int CheckMagic(int fd){
	char* owner_name = "JUNG";

	struct tm owner_birthdate;
	owner_birthdate.tm_sec = 0;
	owner_birthdate.tm_min = 0;
	owner_birthdate.tm_hour= 0;
	owner_birthdate.tm_mday= 6;
	owner_birthdate.tm_mon = 0;
	owner_birthdate.tm_year= 94;
	owner_birthdate.tm_isdst=-1; // lookup in db

	time_t owner_birthdate_epoch = mktime(&owner_birthdate);


	char name_buff[strlen(owner_name)];
	time_t epoch_buffer;

	if(
			read(fd, name_buff, strlen(owner_name)) < 0 ||
			read(fd, &epoch_buffer, sizeof(owner_birthdate_epoch)) < 0
		){
		fprintf(stderr, "writing magic failed!\n");
		return -1;
	}
	printf("[DBG] read magic from fd %d\n", fd);

	if (
			memcmp(&epoch_buffer, &owner_birthdate_epoch, sizeof(time_t)) != 0 ||
			memcmp(name_buff, owner_name, strlen(owner_name) != 0) // no  \0 -> no strcmp
		){
		printf("[DBG] it's not my archive format! epoch=%08x instead of %08x\n",
				(unsigned)epoch_buffer, (unsigned)owner_birthdate_epoch);
		unsigned i;
		for (i = 0; i < strlen(owner_name); ++i){
			printf("[DBG] name[%d]: %c should be %c\n", i, name_buff[i], owner_name[i]);
		}

		return 0;
	}
	return owner_birthdate_epoch;
}
