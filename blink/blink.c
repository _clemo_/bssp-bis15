#include "rawio.c"
#include <time.h>
#include <stdlib.h>

int main(int argc, const char *argv[])
{
  srand(time(NULL));
  int colors[] = {41,42,43,44,45,47,101,102,103, 100, 104, 105, 106, 107};
  int amount = sizeof(colors)/sizeof(int);
  char buffer[100];
  clearscr();
  int col = 0;
  for(;;){
    col = rand() % amount;
    sprintf(buffer, "\e[%dm%*s", colors[col], get_cols()-1," ");
    int l;
    for(l = 0; l < get_lines(); ++l){
      int c;
      writestr_raw(buffer, 0, l);
    }
    usleep(100000);
  }
  return 0;
}
