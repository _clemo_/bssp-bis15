#define PORT 4014

#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#define MAXWAITQUEUE 10

//struct client
typedef struct{
  int fd;
  pthread_mutex_t lock;
  int slot;
}client_t;

typedef struct{
  time_t timestamp;
  size_t user_length;
  size_t message_lenth;
}message_meta_t;
int read_message_meta(message_meta_t*, int);

struct{
  pthread_mutex_t lock;
  size_t clients_slots;
  client_t** clients;
  int socket_fd;
  struct sockaddr_in addr;
}server_state;
int server_state_init(int port);

client_t* client_init(int fd);
int client_add(client_t* c);
void client_remove(client_t* c);
int clients_broadcast(message_meta_t* meta, char* user, char* message);
void* client_handler(void* client);
void system_anncounce(char* text);

void attatch_signals();

int main()
{
  if( server_state_init(PORT) < 0 ){
    printf("Could not start server!\n");
    exit(1);
  }

  attatch_signals();
  for(;;){
    printf("awaiting client\n");

    size_t sock_size = sizeof(server_state.addr);
    int client_fd = accept(server_state.socket_fd, (struct sockaddr * restrict) & server_state.addr, (socklen_t * restrict) &sock_size);
    client_t* c = client_init(client_fd);
    client_add(c);
    pthread_t thread_id;
    int thread_error = pthread_create(&thread_id, NULL, client_handler, (void*) c);
    if(!thread_error){
      printf("started client thread!\n");
    }
  }

  return 0;
}

int read_message_meta(message_meta_t* meta, int fd){
  ssize_t s = read(fd, meta, sizeof(message_meta_t));
  if(s < 0)
    return -1;
  if( ((size_t)s) != sizeof(message_meta_t) )
    return -1;
  return 0;
}

client_t* client_init(int fd){
  client_t* c = malloc(sizeof(client_t));
  c->fd = fd;
  c->lock = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
  return c;
}

int server_state_init(int port){
  server_state.clients_slots = 0;
  server_state.socket_fd = -1;
  server_state.lock = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;
  server_state.clients = malloc(sizeof(client_t*));

  server_state.addr.sin_addr.s_addr = INADDR_ANY;
  server_state.addr.sin_port = htons(port);
  server_state.addr.sin_family = AF_INET;

  server_state.socket_fd = socket(PF_INET, SOCK_STREAM, 0);
  if (server_state.socket_fd == -1) {
    fprintf(stderr, "Socket-Error: %s\n", strerror(errno));
    return 1;
  }
  int yes = 1;
  if ( setsockopt(server_state.socket_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1  ) {
    fprintf(stderr, "could not set socketoptions!\n");
  }
  if (bind(server_state.socket_fd, (struct sockaddr *) &server_state.addr, sizeof(struct sockaddr_in)) == -1) {
    fprintf(stderr, "Bind-Error: %s\n", strerror(errno));
    return 2;
  }
  if (listen(server_state.socket_fd, MAXWAITQUEUE) == -1) {
    fprintf(stderr, "Listen-Error: %s\n", strerror(errno));
    return 3;
  }
  return 0;
}

int server_state_next_slot(){
  unsigned int i = 0;
  for(i = 0; i < server_state.clients_slots; ++i){
    if(server_state.clients[i] == NULL)
      return i;
  }
  return -1;
}

void system_anncounce(char* text){
  char line_buff[1024];
  memset(line_buff, '\0', 1024);
  snprintf(line_buff, 1024, "\e[90m%s\e[0m", text);

  message_meta_t m;
  char* u = "SYSTEM";
  m.timestamp = time(0);
  m.user_length = strlen(u) +1;
  m.message_lenth=strlen(line_buff)+1;

  clients_broadcast(&m, u, line_buff);
}

int client_add(client_t* c){
  pthread_mutex_lock(&server_state.lock);
  ssize_t slot = server_state_next_slot();
  if(slot == -1){
    slot = server_state.clients_slots;
    server_state.clients_slots += 1;
    server_state.clients = realloc(
        server_state.clients,
        sizeof(client_t*)*(server_state.clients_slots)
    );
  }
  server_state.clients[slot] = c;
  pthread_mutex_unlock(&server_state.lock);
  c->slot = slot;

  char line_buff[1024];
  memset(line_buff, '\0', 1024);
  snprintf(line_buff, 1023, "Client id %d connected", c->slot);
  system_anncounce(line_buff);

  return slot;
}

void client_remove(client_t* c){
  if(c == NULL)
    return;
  pthread_mutex_lock(&c->lock);
  pthread_mutex_lock(&server_state.lock);
  server_state.clients[c->slot] = NULL;
  pthread_mutex_unlock(&server_state.lock);
  if( shutdown(c->fd, SHUT_RDWR) )
    close(c->fd);
  pthread_mutex_unlock(&c->lock);

  char line_buff[1024];
  memset(line_buff, '\0', 1024);
  snprintf(line_buff, 1023, "Client id %d disconnected", c->slot);
  system_anncounce(line_buff);

  free(c);
}

int clients_broadcast(message_meta_t* meta, char* user, char* message){
  unsigned int i;
  size_t transmitted_total = 0;
  size_t total_bytes = sizeof(message_meta_t);
  ssize_t transmitted = 0;
  total_bytes += meta->user_length + meta->message_lenth;

  printf("relay message for user %s\n", user);
  char* buffer = malloc(total_bytes);
  transmitted_total = sizeof(message_meta_t);
  memcpy(buffer                    , meta, transmitted_total);
  memcpy(buffer + transmitted_total, user, meta->user_length);
  transmitted_total += meta->user_length;
  memcpy(buffer + transmitted_total, message, meta->message_lenth);
  printf("datablock of %zd bytes completd\n", total_bytes);

  transmitted_total = 0;
  client_t* c;
  for(i = 0; i < server_state.clients_slots; ++i) {
    printf("access client table at position %d\n",i); fflush(stdout);

    c = server_state.clients[i];
    pthread_mutex_lock(&server_state.lock);
    if(c != NULL){
      printf("found client at position %d\n",i); fflush(stdout);
      pthread_mutex_lock(&c->lock);
      transmitted = write(c->fd, buffer, total_bytes);
      pthread_mutex_unlock(&c->lock);
    }
    pthread_mutex_unlock(&server_state.lock);
    if(transmitted < 0){
      fprintf(stderr, "could not send data to client %d\n", c->slot);
      continue;
    }else{
      transmitted_total += (size_t) transmitted;
    }
    printf("%zd bytes transmitted until now\n", transmitted_total); fflush(stdout);
  }
  free(buffer);
  return 0;
}

void* client_handler(void* client){
  client_t* c = (client_t*) client;
  message_meta_t meta;
  ssize_t read_bytes;
  int success;
  char* user = NULL;
  char* message = NULL;
  printf("Start serving client%d\n", c->slot);
  for(;;){
    success = read_message_meta(&meta, c->fd);
    if( success < 0 ){
      fprintf(stderr, "got invalid meta-package -> drop client %d!\n", c->slot);
      break;
    }
    printf("meta-data resceived: %zd b for name, %zd b for msg\n", meta.user_length, meta.message_lenth);

    user = malloc(meta.user_length);
    message = malloc(meta.message_lenth);

    read_bytes = read(c->fd, user, meta.user_length);
    if( read_bytes < 0 || (size_t) read_bytes != meta.user_length){
      fprintf(stderr, "got wrong user-size %zd instead of %zd\n", read_bytes, meta.user_length);
      break;
    }
    read_bytes = read(c->fd, message, meta.message_lenth);
    if( read_bytes < 0 ){
      fprintf(stderr, "got wrong payload %zd instead of %zd\n", read_bytes, meta.message_lenth);
      break;
    }
    printf("got whole message\n");

    clients_broadcast(&meta, user, message);
    printf("relayed message\n");

    free(user);
    user = NULL;
    free(message);
    message = NULL;
  }
  printf("destroy client %d\n", c->slot);
  client_remove(c);
  return NULL;
}

static void sigint_handler(){
	printf("\nshutting server down!\n");
	system_anncounce("SERVER IS  GOING DOWN");
	if(server_state.clients != NULL){
		unsigned i;
		for(i = 0; i < server_state.clients_slots; ++i)
			if(server_state.clients[i] != NULL){
			  client_remove(server_state.clients[i]);
			}
	}
	fsync(server_state.socket_fd);
	shutdown(server_state.socket_fd, SHUT_RDWR);
	close(server_state.socket_fd);
	exit(0);
}

void attatch_signals(){
	struct sigaction sa_int;
	sigemptyset(&sa_int.sa_mask);
	sa_int.sa_flags = SA_NOCLDSTOP | SA_RESTART;
	sa_int.sa_handler = sigint_handler;
	sigaction(SIGINT, &sa_int, NULL);
	sigaction(SIGQUIT, &sa_int, NULL);
}
