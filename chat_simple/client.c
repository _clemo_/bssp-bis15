#include "rawio.c"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <pthread.h>
#include <time.h>

#define MAXLINE 1024
#define BUFF_SIZE 1024

void  ask_user(char* text, char* buffer, size_t buffer_size);
void* handle_server(void* params);
typedef struct{
	int server_fd;
}handle_server_params_t;

void draw_box(int start_col, int start_row, int width, int height, char* box_header);
void draw_error(char* text);

typedef struct history_s{
	char* text;
	struct history_s* next;
}history_t;
history_t* history = NULL;
pthread_mutex_t history_lock = PTHREAD_MUTEX_INITIALIZER;

void add_message(char* text, int prio);
int render_messages(int start_col, int start_row, int width, int height);

static int _default_renderer_s_x;
static int _default_renderer_s_y;
static int _default_renderer_w;
static int _default_renderer_h;
void set_default_render_rect(int x, int y, int width, int height){
  _default_renderer_s_x = x;
  _default_renderer_s_y = y;
  _default_renderer_w = width;
  _default_renderer_h = height;
}

void DRAW_BASIC_LAYOUT(){
	int max_col = get_cols();
	int max_row = get_lines();
	draw_box(0, 0, max_col, max_row-3, "Nachrichten");
	draw_box(0, max_row-3, max_col, 3, "Eingabe");

	set_default_render_rect(1,1,max_col-2, max_row-4);
}

void render_default_window(){
	if(render_messages(_default_renderer_s_x, _default_renderer_s_y, _default_renderer_w, _default_renderer_h))
		DRAW_BASIC_LAYOUT();
}

int send_message(int fd, char* user, char* msg);

typedef struct{
  time_t timestamp;
  size_t user_length;
  size_t message_lenth;
}message_meta_t;
int read_message_meta(message_meta_t*, int);
int read_message_meta(message_meta_t* meta, int fd);
int read_input(char* buffer, size_t buffer_size, int x, int y, int line_width);

int send_default_mode(int fd, char* text);
char* read_default_mode(int fd);

int main()
{
	char line_buffer[MAXLINE];
	memset(line_buffer, '\0', MAXLINE);

	ask_user("Please enter username: ", line_buffer, MAXLINE);
	char* user_name = strdup(line_buffer);

	in_addr_t ip_address;
	do{
		ask_user("Please enter ip of server: ", line_buffer, MAXLINE);
	}while( (ip_address = inet_addr(line_buffer)) == INADDR_NONE);

	long int port = 0;
	do{
		ask_user("Please enter port: ", line_buffer, MAXLINE);
		errno = 0;
		port = strtol(line_buffer, NULL, 10);
	}while(errno != 0 && port > 0);
	
	struct sockaddr_in server_address;
	server_address.sin_addr.s_addr = ip_address;
	server_address.sin_port = htons(port);
	server_address.sin_family = AF_INET;

	clearscr();
	DRAW_BASIC_LAYOUT();

	snprintf(line_buffer, MAXLINE-1, "Welcome %s.", user_name);
	add_message(line_buffer, 2);
	add_message("Connecting to server...", 2);
	render_default_window();

	int socket_fd;
	if ( (socket_fd = socket(AF_INET, SOCK_STREAM, 0))  == -1){
		add_message("aborted", 3);
		snprintf(line_buffer, MAXLINE-1, "Could not create socket %s\n", strerror(errno));
		draw_error(line_buffer);
		render_default_window();
		exit(2);
	}else
		add_message("Phase 1: ok", 1);

	render_default_window();

	if ( connect(
					socket_fd,
					(const struct sockaddr *) &server_address,
					sizeof(server_address)
			) == -1 ){
		add_message("aborted", 3);
		snprintf(line_buffer, MAXLINE-1, "Could not create socket %s\n", strerror(errno));
		draw_error(line_buffer);
		render_default_window();
		exit(3);
	}else
		add_message("Phase 2: ok - now connected", 1);

	render_default_window();

	if(send_default_mode(socket_fd, user_name) > 0){
		add_message("Phase 3: ok - sent username", 1);
		render_default_window();
	}

	handle_server_params_t* thread_args = malloc(sizeof(handle_server_params_t));
	thread_args->server_fd = socket_fd;

	pthread_t thread_id;
	int error = pthread_create(
			&thread_id,
			NULL,
			handle_server,
			(void*) thread_args
	);
	if(error){
		snprintf(line_buffer, MAXLINE-1, "Could not create thread %s\n", strerror(errno));
		draw_error(line_buffer);
		add_message("aborting - shutting down connection...", 3);
		render_default_window();
		shutdown(socket_fd, SHUT_RDWR);
		close(socket_fd);
		exit(5);
	}

	int max_col = get_cols();
	int max_row = get_lines();

	int res;
	for(;;){
		res = read_input(line_buffer, MAXLINE, 1, max_row-2, max_col-3);
		if(res == -1)
			break;
		if(strcmp(line_buffer, "/quit") == 0){
			break;
		}
		if(send_default_mode(socket_fd, line_buffer) == -1)
			break;
	}

	add_message("shutting down...", 1);
	if(shutdown(socket_fd, SHUT_RDWR) != -1)
		close(socket_fd);
	exit(0);

	return 0;
}


int send_default_mode(int fd, char* text){
	return write(fd, text, strlen(text)+1);
}

char* read_default_mode(int fd){
	size_t size = BUFF_SIZE;
	char* buffer = malloc(size);
	char c;
	ssize_t got;
	size_t len = 0;
	do{
		got = read(fd, &c, 1);
		if(got <= 0){
			free(buffer);
			return NULL;
		}else if(got == 0 || c == '\n' || c == '\r'){
			c = '\0';
		}

		buffer[len] = c;
		len += 1;
		
		if(len >= size){
			size+=BUFF_SIZE;
			buffer = realloc(buffer, size);
		}
	}while(c != '\0');
	return buffer;
}

void ask_user(char* text, char* buffer, size_t buffer_size){
	printf("%s", text);
	fflush(stdout);
	if (fgets(buffer, buffer_size, stdin) == NULL ){
		fprintf(stderr, "Could not read vom stdin!\n");
		exit(1);
	}

	size_t buffer_len = strlen(buffer);
	if(buffer[buffer_len-1] == '\n'){
		buffer[buffer_len-1] = '\0';
	}
}

int read_input(char* line_buffer, size_t buffer_size, int x, int y, int line_width){
	memset(line_buffer, '\0', buffer_size);
	memset(line_buffer, ' ', line_width);
	writestr_raw(line_buffer, x, y);
	writestr_raw("", x, y);

	memset(line_buffer, '\0', buffer_size);
	if( gets_raw(line_buffer, line_width, x, y)  == NULL){
		add_message("could not read from input", 2);
		render_default_window();
		return -1;
	}

	unsigned i, m;
	for(i = 0, m = strlen(line_buffer); i < m; ++i){
		switch(line_buffer[i]){
			case '\b':
			case '\t':
				line_buffer[i] = ' ';
				break;
			case '\r':
			case '\n':
				line_buffer[i] = '\0';
				break;
		}
	}

	return 0;
}

void* handle_server(void* params){
	handle_server_params_t par = *((handle_server_params_t*)params);

	char buffer[BUFF_SIZE];
	memset(buffer, 0x00, BUFF_SIZE);

	for(;;){
		char* msg = read_default_mode(par.server_fd);
		if(msg == NULL){
			add_message("Lost connection to server", 2);
			render_default_window();
			break;
		}
		if(strlen(msg) > 0){
			add_message(msg, 0);
			render_default_window();
		}
		free(msg);
	}

	add_message("Terminated thread", 1);
	render_default_window();
	exit(0);
	return NULL;
}

void draw_box(int start_col, int start_row, int width, int height, char* box_header){
	char* left_up  = "\e[37m╭\e[0m";
	char* left_down= "\e[37m╰\e[0m";
	char* horizont = "\e[37m─\e[0m";
	char* right_up = "\e[37m╮\e[0m";
	char* vertical = "\e[37m│\e[0m";
	char* right_down="\e[37m╯\e[0m";
	char* left_end  ="\e[37m╴\e[0m";
	char* right_end ="\e[37m╶\e[0m";

	int max_col = get_cols();
	int max_row = get_lines();
	int i;

	for(i = 1; i <= (width-2) && i < max_col; ++i){
		writestr_raw(horizont, start_col+i, start_row);
		writestr_raw(horizont, start_col+i, start_row+height-1);
	}
	for(i = 1; i <= (height-2) && i < max_row; ++i){
		writestr_raw(vertical, start_col,         start_row+i);
		writestr_raw(vertical, start_col+width-1, start_row+i);
	}

	writestr_raw(left_up, start_col, start_row);
	writestr_raw(right_up, start_col+width-1, start_row);
	writestr_raw(left_down, start_col, start_row+height-1);
	writestr_raw(right_down, start_col+width-1, start_row+height-1);

	if(box_header != NULL){
		unsigned header_len = strlen(box_header)+1;
		int offs = (width-header_len)/2;
	  writestr_raw(left_end, start_col+offs, start_row);
	  writestr_raw(box_header, start_col+offs+1, start_row);
	  writestr_raw(right_end, start_col+offs+1+strlen(box_header), start_row);
	}
}

void draw_error(char* text){
	static int line_num = 0;
	char line[MAXLINE];
	snprintf(line, MAXLINE, "\e[1;91m%s\e[0m", text);
	line[MAXLINE-1] = '\0';
	writestr_raw(line, 0, line_num++);
}

void add_message(char* text, int prio){
	char line[MAXLINE];
	switch(prio){
		case 1: // green
			snprintf(line, MAXLINE, "\e[32m%s\e[0m", text);
			break;
		case 2: // yellow
			snprintf(line, MAXLINE, "\e[33m%s\e[0m", text);
			break;
		case 3: // red
			snprintf(line, MAXLINE, "\e[91m%s\e[0m", text);
			break;
		default:
			strcpy(line, text);
			break;
	}
	line[MAXLINE-1] = '\0';

	history_t* elem = malloc(sizeof(history_t));
	elem->text = strdup(line);
	pthread_mutex_lock(&history_lock);
	elem->next = history;
	history = elem;
	pthread_mutex_unlock(&history_lock);
}

int render_messages(int start_col, int start_row, int width, int height){
	start_col++;
	start_row--;
	height--;
	width--;
	char flush_line[width+1];

	snprintf(flush_line, width+1, "\e[0m%*s", width-1, " ");
	flush_line[width] = '\0';

	history_t* elem = history;
	int used_height = 0;
	int force_redraw = 0;
	while(used_height < height && elem != NULL){
		writestr_raw(flush_line, start_col, start_row+height-(used_height));
		writestr_raw(elem->text, start_col, start_row+height-(used_height));
		if(strlen(elem->text) >= (unsigned)width)
			force_redraw = 1;
		elem = elem->next;
		used_height += 1;
	}
	return force_redraw;
}
