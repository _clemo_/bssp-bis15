#define PORT 4014

#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#define MAXWAITQUEUE 10
#define MAX_LINE_LENGTH 2048

//struct client
typedef struct{
	int fd;
	char* name;
}client_t;

struct{
	pthread_mutex_t lock;

	struct{
		size_t usage;
		client_t** arr;
		size_t size;
	}client_db;
	int socket_fd;

	struct sockaddr_in addr;
}server_state;

int server_state_init(int port);
void client_add(client_t* c);
int clients_broadcast(char* user, char* message, int has_lock);
client_t* client_init(int fd);
void* client_handler(void* c);
void attatch_signals();

int main()
{
	if( server_state_init(PORT) < 0 ){
		printf("Could not start server!\n");
		exit(1);
	}
	printf("[DBG] server_fd=%d\n", server_state.socket_fd);

	attatch_signals();
	for(;;){
		printf("awaiting client on port %d\n", PORT);

		size_t sock_size = sizeof(server_state.addr);
		int client_fd = accept(server_state.socket_fd, (struct sockaddr * restrict) & server_state.addr, (socklen_t * restrict) &sock_size);
		client_t* c = client_init(client_fd);
		client_add(c);
		pthread_t thread_id;
		int thread_error = pthread_create(&thread_id, NULL, client_handler, (void*) c);
		if(!thread_error){
			printf("started client thread!\n");
		}
	}

	return 0;
}

client_t* client_init(int fd){
	client_t* c = malloc(sizeof(client_t));
	c->fd = fd;
	c->name = NULL;
	return c;
}

int server_state_init(int port){
	server_state.client_db.usage = 0;
	server_state.client_db.size = 1;
	server_state.client_db.arr = malloc(sizeof(client_t*));
	server_state.socket_fd = -1;
	server_state.lock = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;

	server_state.addr.sin_addr.s_addr = INADDR_ANY;
	server_state.addr.sin_port = htons(port);
	server_state.addr.sin_family = AF_INET;

	server_state.socket_fd = socket(PF_INET, SOCK_STREAM, 0);
	if (server_state.socket_fd == -1) {
		fprintf(stderr, "Socket-Error: %s\n", strerror(errno));
		return 1;
	}
	int yes = 1;
	if ( setsockopt(server_state.socket_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1	) {
		fprintf(stderr, "could not set socketoptions!\n");
	}
	if (bind(server_state.socket_fd, (struct sockaddr *) &server_state.addr, sizeof(struct sockaddr_in)) == -1) {
		fprintf(stderr, "Bind-Error: %s\n", strerror(errno));
		return 2;
	}
	if (listen(server_state.socket_fd, MAXWAITQUEUE) == -1) {
		fprintf(stderr, "Listen-Error: %s\n", strerror(errno));
		return 3;
	}
	return 0;
}

void client_add(client_t* c){
	pthread_mutex_lock(&server_state.lock);

	if(server_state.client_db.usage >= server_state.client_db.size){
		server_state.client_db.size += 1;
		server_state.client_db.arr = realloc(server_state.client_db.arr, sizeof(client_t*)*server_state.client_db.size);
		if(server_state.client_db.arr == NULL){
			fprintf(stderr, "could not add client - something seems broken...\n");
			//TODO quit server
		}
	}
	server_state.client_db.arr[server_state.client_db.usage] = c;
	server_state.client_db.usage += 1;
	printf("server info: client on slot %zd connected\n", 
			server_state.client_db.usage-1);
	pthread_mutex_unlock(&server_state.lock);
}

void system_anncounce(char* text, int has_lock){
	char line_buff[1024];
	snprintf(line_buff, 1024, "\e[90m%s\e[0m", text);

	char* u = "SYSTEM";
	clients_broadcast(u, line_buff, has_lock);
}

void client_remove(client_t* c, int has_lock){
	if(c == NULL)
		return;
	unsigned i;
	int fd = -1;
	char buff[1024];
	buff[0] = '\0';

	if(!has_lock)
		pthread_mutex_lock(&server_state.lock);
	client_t** arr = server_state.client_db.arr;
	size_t used    = server_state.client_db.usage;

	for(i = 0; i < used; ++i){
		if(arr[i] != c)
			continue;
		if(arr[i]->name != NULL){
			snprintf(buff, 1024, "User %s disconnected", arr[i]->name);
			free(arr[i]->name);
		}
		fd = arr[i]->fd;
		free(arr[i]);
		arr[i] = NULL;
		size_t to_move = ((used-1) - i)*sizeof(client_t*);
		if(to_move > 0){
			printf("[DBG] move %zd from %u to %u\n", to_move, i+1,i);
			memmove(&arr[i], &arr[i+1], to_move);
			printf("[DBG] moved mem\n");
		}
		arr[used-1] = NULL;
		server_state.client_db.usage -= 1;
		break;
	}
	
	if(strlen(buff) > 0){
		printf("[DBG] announce disconnect\n");
		system_anncounce(buff, 1);
	}
	if(!has_lock)
		pthread_mutex_unlock(&server_state.lock);
	
	if( fd != -1 ){
		if( shutdown(fd, SHUT_RDWR) == 0){
			printf("gracefully closed connection\n");
		}
		close(fd);
	}
}

int clients_broadcast(char* user, char* message, int has_lock){
	unsigned int i;
	time_t timestamp;
	time(&timestamp);
	
	char* time_string = ctime(&timestamp);
	time_string[strlen(time_string)-1]= '\0'; // remove \n from end
	
	char* tmp = strchr(message, '\n');
	if(tmp != NULL)
		tmp = '\0';

	char buffer[MAX_LINE_LENGTH];
	
	snprintf(buffer, MAX_LINE_LENGTH-1, "\e[2m%s\e[0m \e[36m%s\e[0m: %s", 
			time_string, user, message);
	buffer[MAX_LINE_LENGTH-1] = '\0';
	size_t buffer_len = strlen(buffer);

	ssize_t send_bytes;
	if(!has_lock)
		pthread_mutex_lock(&server_state.lock);
	client_t** arr = server_state.client_db.arr;
	size_t used    = server_state.client_db.usage;
	printf("[DBG] send to %zd clients\n", used);
	for(i = 0; i < used; ++i){
		printf("[DBG] access %u: %p\n", i, arr[i]);
		if(arr[i] != NULL){
			send_bytes = write(arr[i]->fd, buffer, buffer_len+1);
			if(send_bytes > 0){
				if(arr[i]->name != NULL){
					printf("send msg to '%s'\n", arr[i]->name);
				}
			}else{
				printf("could not send msg to %d\n", i);
				client_remove(arr[i], 1);
				--i;
			}
		}
	}
	
	if(!has_lock)
		pthread_mutex_unlock(&server_state.lock);
	return 0;
}

int rec_string(int fd, char* buff, size_t buff_size){
	unsigned i;
	ssize_t read_bytes = 0;
	printf("[DBG] read into %p from fd %d\n", buff, fd);
	for(i = 0; i < buff_size; ++i){
		printf("[DBG] read to buffer at position %u\n", i);
		read_bytes = read(fd, &buff[i], 1);
		if(read_bytes <= 0)
			break;
		else
			printf("[DBG] got %zd bytes\n", read_bytes);
		if(buff[i] == '\0')
			break;
	}
	if(read_bytes <= 0){
		printf("terminating buffer, got: %zd\n", read_bytes);
		buff[i] = '\0';
		return read_bytes;
	}
	return 1;
}

void* client_handler(void* client){
	client_t* c = (client_t*) client;
	int res;
	printf("Start serving client\n");

	char line_buffer[MAX_LINE_LENGTH];
	printf("[DBG] allocated buffer at %p\n", line_buffer);
	res = rec_string(c->fd, line_buffer, MAX_LINE_LENGTH);
	if(res > 0){
		c->name = strdup(line_buffer);
		snprintf(line_buffer, MAX_LINE_LENGTH, "User %s connected",
				c->name);
		system_anncounce(line_buffer, 0);
		printf("%s\n", line_buffer);

		for(;;){
			res = rec_string(c->fd, line_buffer, MAX_LINE_LENGTH);
			if( res <= 0 ){
				fprintf(stderr, "got invalid package -> drop client\n");
				break;
			}
			if(strlen(line_buffer) > 0){
				printf("relay message '%s' for '%s'\n", line_buffer, c->name);
				clients_broadcast(c->name, line_buffer, 0);
			}
		}
	}else{
		printf("Could not aquire username!\n");
	}

	client_remove(c, 0);
	return NULL;
}

static void sigint_handler(){
	printf("\nshutting server down!\n");

	pthread_mutex_lock(&server_state.lock);
	system_anncounce("SERVER IS	GOING DOWN", 1);
	if(server_state.client_db.arr != NULL){
		unsigned i;
		for(i = 0; i < server_state.client_db.usage; ++i)
			if(server_state.client_db.arr[i] != NULL){
				client_remove(server_state.client_db.arr[i], 1);
			}
	}
	pthread_mutex_unlock(&server_state.lock);
	
	fsync(server_state.socket_fd);
	shutdown(server_state.socket_fd, SHUT_RDWR);
	close(server_state.socket_fd);
	exit(0);
}

void attatch_signals(){
	struct sigaction sa_int;
	sigemptyset(&sa_int.sa_mask);
	sa_int.sa_flags = SA_NOCLDSTOP | SA_RESTART;
	sa_int.sa_handler = sigint_handler;
	sigaction(SIGINT, &sa_int, NULL);
	sigaction(SIGQUIT, &sa_int, NULL);
}
