/**
 * @file cdrv.c
 * @brief Character-Device-Driver to acces a 1kb buffer
 */

#define DEBUG                  ///< switch on more outputs @see [pr_devel](http://www.crashcourse.ca/wiki/index.php/Printk_and_variations#pr_devel.28.29_and_pr_debug.28.29)

#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/version.h>
#include <linux/errno.h>
#include <linux/printk.h>
#include <linux/moduleparam.h>
#include <linux/proc_fs.h>
#include <linux/types.h>
#include <linux/cdev.h>
#include <linux/semaphore.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <linux/seq_file.h>
#include <linux/time64.h>
#include <linux/timekeeping.h>
#include <linux/delay.h>

#include "my_ioctl.h"

MODULE_LICENSE("Dual BSD/GPL");

#define DRVNAME KBUILD_MODNAME ///< drivername = module-name

#define BUFFER_SIZE 1024       ///< size of the storage per device
#define MINOR_START 0          ///< number of first minor
#define MINOR_COUNT 5          ///< amount of minors to spwan

#define PROC_DIR_NAME "is151014" ///< name for the folder under `/proc`
#define PROC_FILE_NAME "info"    ///< name for the file under `/proc`PROC_DIR_NAME

#define MIN_VALUE_OF(a,b) ( (a) > (b) ? (b) : (a))  ///< returns the smaller one of two values

/**
 * @brief   CLAIM_SEMAPHORE claims the semaphore from `dev->sync` and if not possible it prints an error to kernel-log and returns -ERSTARTSYS
 * @returns nothing -> pre-processor-macro
 */
#define CLAIM_SEMAPHORE()                                   \
	do{                                                       \
		if (down_interruptible(&dev->sync)) {                   \
			pr_warn("cdrv: read: could not claim semaphore\n");   \
			return -ERESTARTSYS;                                  \
		}                                                       \
	}while(0)

// protoytpes
static int __init cdrv_init(void);
static void __exit cdrv_exit(void);

// handler
static int mydev_open(struct inode* inode, struct file* filp);
static int mydev_release(struct inode* inode, struct file* filp);
static ssize_t mydev_read(struct file* filep, char __user* buff, size_t count, loff_t* offset);
static ssize_t mydev_write(struct file* filep, const char __user* buff, size_t count, loff_t* offset);
static long mydev_ioctl(struct file* filep, unsigned int cmd, unsigned long arg);

// for /proc @seed procfs
static int my_seq_file_open(struct inode* inode, struct file* filp);
static void* my_start(struct seq_file* sf, loff_t* pos);
static void* my_next(struct seq_file* sf, void* v, loff_t* pos);
static int my_show(struct seq_file* sf, void* it);
static void my_stop(struct seq_file* sf, void* it);

// register module
module_init(cdrv_init);
module_exit(cdrv_exit);


/**
 * @brief   my_cdev is a struct that holds all the **information for a single device**
 */
typedef struct my_cdev{
	char* buffer;              ///< pointer to the actual data, on start NULL
	size_t current_length;     ///< how many bytes in the bfufer are in use
	bool already_open;         ///< indicates that the device is already opened

	struct semaphore sync;     ///< used to ensure correct read/write access

	long count_open_read;      ///< used in ioctl
	long count_open_write;     ///< used in ioctl (redundency with already_open is on purpose)

	u64  total_count_open;     ///< how often `open(2)` was called per /dev/mydev regardless of the success of the call
	u64  total_count_close;    ///< how often `close(2)` was called per /dev/mydev regardless of the success of the call
	u64  total_count_read;     ///< how often `read(2)` was called per /dev/mydev regardless of the success of the call
	u64  total_count_write;    ///< how often `write(2)` was called per /dev/mydev regardless of the success of the call
	u64  total_count_clear;    ///< how often `ioctl(2)` was used to clear the buffer

	struct timespec64 time_used_write; ///< aggregates the time used for the write-function
	struct timespec64 time_used_read;  ///< aggregates the time used for the read-function

	unsigned long allowed_readers;

	wait_queue_head_t waiting;       ///< used for blocking read/write
	wait_queue_head_t ioctl_cleared; ///< used for blocking write

	struct cdev cdev;          ///< device visible to the kernel
}my_cdev_t;

static my_cdev_t* my_devs = NULL; ///< poitner to my_cdev for each minor

static struct class *my_class;    ///< device-class used for cleanup

static struct proc_dir_entry* proc_parent = NULL; // /proc/is151014
static struct proc_dir_entry* proc_entry = NULL;  // /proc/is151014/info

static void update_used_time(struct timespec64* update, struct timespec64* start, struct timespec64* end){
	struct timespec64 timediff = timespec64_sub(*end, *start);
	*update = timespec_add(*update, timediff);
}

/**
 * @brief   cleanup_devices_buffers frees all buffers from the devices if present
 * @param   count amount of devices that should be freed
 */
static void cleanup_devices_buffers(int count){
	if(my_devs == NULL)
		return;
	for(--count; count >= 0; --count)
		if( my_devs[count].buffer	!= NULL ) {
			kfree(my_devs[count].buffer), my_devs[count].buffer = NULL;
			pr_devel("cdrv: cleanup buffers: freed buffer of %d\n", count);
		}
	pr_devel("cdrv: cleanup buffers: cleanup buffers done\n");
}

/**
 * @brief   cleanup_devices_devs delets and destroys als cdev's from devices
 * @param   count amount of devices that should be destroyed
 */
static void cleanup_devices_devs(int count){
	if(my_devs == NULL)
		return;
	for(--count; count >= 0; --count) {
		device_destroy(my_class, my_devs[count].cdev.dev);
		cdev_del(&my_devs[count].cdev);
			pr_devel("cdrv: cleanup devices: cleaned device %d\n", count);
	}
	pr_devel("cdrv: cleanup devices: cleanup devices done\n");
}

/**
 * @brief   cdrv_init inizializes the module
 * @returns errno on failure
 * @see man to semaphore: http://www.makelinux.net/ldd3/chp-5-sect-3
 */
static int __init cdrv_init(void)
{
	int error_code = 0;
	int j;

	int result;
	int i;
	dev_t first_dev;
	struct device* current_device = NULL;

	// function handlers
	// attention if not static your programm WILL FAIL
	// if this is not static after init all function handlers are gone
	static struct file_operations mydev_fcalls =
	{
		.owner = THIS_MODULE,
		.open = mydev_open,
		.release = mydev_release,
		.read = mydev_read,
		.write = mydev_write,
		.unlocked_ioctl = mydev_ioctl
	};

	static struct file_operations myproc_fcalls =
	{
		.owner = THIS_MODULE,
		.open = my_seq_file_open,
		.read = seq_read,
		.llseek = seq_lseek,
		.release = seq_release
	};

	pr_devel("cdrv: init: loading");

	result = alloc_chrdev_region( &first_dev, MINOR_START, MINOR_COUNT, DRVNAME );

	if (result) {
		return result;
	}

	pr_devel("cdrv: init: major %d, minor %d\n", MAJOR(first_dev), MINOR(first_dev));

	my_devs = kmalloc(sizeof(my_cdev_t) * MINOR_COUNT, GFP_KERNEL);
	if (my_devs == NULL){
		error_code = -ENOMEM;
		goto fail_region_allocated;
	}

	// create a class (directory) in /sys/class
	my_class = class_create(THIS_MODULE, "my_driver_class");
	if (IS_ERR(my_class)) {
		pr_warn("cdrv: init: could not create class %s\n", "my_driver_class");
		error_code = (signed long) my_class;
		goto fail_devs_allocated;
	}

	proc_parent = proc_mkdir(PROC_DIR_NAME, NULL);
	if(proc_parent == NULL){
		//TODO ERRORHANDLING
		pr_warn("cdrv: init: create proc failed\n");
		goto fail_class_created;
	}
	proc_entry = proc_create_data(PROC_FILE_NAME, 0664, proc_parent, &myproc_fcalls, NULL);
	if(proc_entry == NULL){
		//TODO ERRORHANDLING
		pr_warn("cdrv: init: create proc-data failed\n");
		goto fail_proc_dir_created;
	}

	// init all minors
	for(i = 0; i < MINOR_COUNT; i++){
		dev_t cur_devnr = MKDEV(MAJOR(first_dev), MINOR(first_dev) +i);
		// da internes verhalten nicht genau bekannt. ansonsten würde MINOR_START +i reichen
		// laut header-files müsste auch `first_dev + i` das komplette MKDEV ersetzten

		cdev_init(&my_devs[i].cdev, &mydev_fcalls);  // pro device syscalls registrieren
		my_devs[i].cdev.owner = THIS_MODULE;         // für kernel

		// init own structure
		my_devs[i].current_length = 0;
		my_devs[i].buffer = NULL;
		my_devs[i].already_open = 0;
		my_devs[i].count_open_read  = 0;
		my_devs[i].count_open_write = 0;

		my_devs[i].total_count_open  = 0;
		my_devs[i].total_count_close = 0;
		my_devs[i].total_count_read  = 0;
		my_devs[i].total_count_write = 0;
		my_devs[i].total_count_clear = 0;

		my_devs[i].time_used_write = (struct timespec64) {0, 0};
		my_devs[i].time_used_read =  (struct timespec64) {0, 0};

		my_devs[i].allowed_readers = 0;

		sema_init(&my_devs[i].sync, 1);
		init_waitqueue_head(&my_devs[i].waiting);
		init_waitqueue_head(&my_devs[i].ioctl_cleared);

		current_device = device_create(my_class, NULL, cur_devnr, NULL, "mydev%d", MINOR(cur_devnr));
		if(IS_ERR(current_device)){
			pr_warn("cdrv: init: device_create for minor %d failed!\n", MINOR(cur_devnr));
			error_code = (signed long) current_device;
			goto fail_cdevs_created;
		}

		error_code = cdev_add(&my_devs[i].cdev, cur_devnr, 1); // makes the device "live"
		if (error_code < 0 ) {
			pr_warn("cdrv: init: could not add device %d\n", i);
			goto fail_cdevs_registered;
		}
	}

	pr_devel("cdrv: init: loaded successfull\n");
	return 0;

	// cleanups [did not know how to test them yet]
fail_cdevs_registered:
	device_destroy(my_class, my_devs[i].cdev.dev);

fail_cdevs_created:
	for(j = 0; j <= i; ++j){
		if (j < i)
			device_destroy(my_class, my_devs[j].cdev.dev);
		cdev_del(&my_devs[j].cdev);
	}

//fail_proc_file_created:
	remove_proc_entry(PROC_FILE_NAME, proc_parent);

fail_proc_dir_created:
	remove_proc_entry(PROC_DIR_NAME, NULL);

fail_class_created:
	class_destroy(my_class);

fail_devs_allocated:
	kfree(my_devs);
	my_devs = NULL;

fail_region_allocated:
	unregister_chrdev_region(first_dev, MINOR_COUNT);
	return error_code;
}


/**
 * @brief   cleanup_partial performs a partial cleanup for `count` devices
 * @param   count the amount of devices that should  be clear
 */
static void cleanup_partial(int count){
	dev_t first_dev = 0;
	if (my_devs != NULL) {
		first_dev = my_devs[0].cdev.dev;
		cleanup_devices_buffers(count);
		cleanup_devices_devs(count);

		class_destroy(my_class);
		if( first_dev != 0 ){
			unregister_chrdev_region(first_dev, MINOR_COUNT);
			pr_devel("cdrv: cleanup: unregistered devices\n");
		}
		kfree(my_devs), my_devs = NULL;
		pr_devel("cdrv: cleanup: finished cleanup for %d elems\n", count);
	}
}
/**
 * @brief   cdrv_exit unloads the module
 * @note    you cant unload a semaphore! - there is no such command, and since it's statically allocated we can't free it
 * @see     wiki for printk and derivates: http://www.crashcourse.ca/wiki/index.php/Printk_and_variations#What.27s_pr_fmt_all_about.3F
 */
static void __exit cdrv_exit(void)
{
	pr_devel("cdrv: exit: Goodbye, kernel world!\n");

	pr_devel("cdrv: exit: delete proc\n");
	remove_proc_entry(PROC_FILE_NAME, proc_parent);
	remove_proc_entry(PROC_DIR_NAME, NULL);

	pr_devel("cdrv: exit: cleanup rest\n");
	cleanup_partial(MINOR_COUNT);
	//sema_destroy(&my_devs[i].sync); // lol you cant destroy a semphore and since it's static allocated you can't free it

	pr_devel("cdrv: exit: cleaned up my character driver\n");
}

static int mydev_open(struct inode* inode, struct file* filp){
	// zeiger auf struct über container_of
	my_cdev_t* dev = container_of(inode->i_cdev, my_cdev_t, cdev);
	filp->private_data = dev;

	CLAIM_SEMAPHORE();
	dev->total_count_open += 1;

	// check if opened with write permissions
	if (filp->f_mode & FMODE_WRITE) { // WRITE
		if (dev->buffer == NULL) { // first run
			dev->buffer = kmalloc(BUFFER_SIZE, GFP_KERNEL);
		}else if(dev->already_open){
			pr_info("cdrv: open: device already opened!\n");
			up(&dev->sync);
			return -EBUSY;
		}
		dev->already_open = 1;
		pr_devel("cdrv: open: write-locked device\n");
		dev->count_open_write += 1;
	}
	if(filp->f_mode & FMODE_READ){   // READ
		if(dev->buffer == NULL){
			pr_info("cdrv: open: device must be opened for write before you can read!\n");
			up(&dev->sync);
			return -ENOSR;
		}
		if(dev->allowed_readers > 0 && dev->count_open_read > dev->allowed_readers){
			pr_devel("cdrv: open: allowed readers were %lu but there are %ld\n", dev->allowed_readers, dev->count_open_read);
			up(&dev->sync);
			return -EBUSY;
		}
		dev->count_open_read += 1;
	}
	up(&dev->sync);
	pr_devel("cdrv: open: FMODE_NDLAY: %d, may not block %d, O_NDELAY: %d, O_NONBLOCK: %d\n", filp->f_flags & FMODE_NDELAY, filp->f_flags & MAY_NOT_BLOCK, filp->f_flags & O_NDELAY, filp->f_flags & O_NONBLOCK);
	return 0;
}

static int mydev_release(struct inode* inode, struct file* filp){
	my_cdev_t* dev = filp->private_data;
	wake_up_all(&dev->ioctl_cleared);

	CLAIM_SEMAPHORE();
	dev->total_count_close += 1;

	if (filp->f_mode & FMODE_WRITE) {   // WRITE
		dev->already_open = 0;
		pr_devel("cdrv: release: released write-lock\n");
		dev->count_open_write -= 1;

		pr_devel("cdrv: release: waking up clients\n");
		wake_up_all(&dev->waiting);
		pr_devel("cdrv: release: woke up the readers\n");
	}

	if (filp->f_mode & FMODE_READ) {   // READ
		dev->count_open_read  -= 1;
	}
	up(&dev->sync);
	return 0;
}


#define UPDATE_READ_CONSUMED_TIME() do{                                                     \
	syscall_end = current_kernel_time64();                                                    \
	update_used_time(&dev->time_used_read, &syscall_start, &syscall_end);                     \
	pr_devel("cdrv: read: syscall finished, total_consumed time for this call: %ld,%09ld\n",\
			dev->time_used_read.tv_sec,                                                           \
			dev->time_used_read.tv_nsec                                                           \
	);                                                                                        \
}while(0)

/**
 * @brief   mydev_read reads from character-device
 * @param   filep (provided by kernel) FILE to be read
 * @param   buff target-buffer from read(2)
 * @param   count amounts of bytes requested by read(2)
 * @param   offset last position (provided by kernel) gets incremented by the return-value of this function
 * @see     [copy_to_user man-page](https://www.fsl.cs.sunysb.edu/kernel-api/re256.html)
 * @returns bytes copied into user-buffer or -ERRNO;
 */
static ssize_t mydev_read(struct file* filep, char __user* buff, size_t count, loff_t* offset){
	my_cdev_t* dev = filep->private_data;
	unsigned long not_copied;
	size_t to_read;
	size_t already_read = 0;
	struct timespec64 syscall_start;
	struct timespec64 syscall_end;
	syscall_start = current_kernel_time64();
	pr_devel("cdrv: read: offset: %lld, count: %zd\n", *offset, count);

	CLAIM_SEMAPHORE();
	dev->total_count_read += 1;
	up(&dev->sync);

	if (buff == NULL) {
		pr_devel("cdrv: read: got NULL buffer\n");
		UPDATE_READ_CONSUMED_TIME();
		return -EINVAL;
	}else if(count == 0) {
		pr_devel("cdrv: read: nothing requested\n");
		UPDATE_READ_CONSUMED_TIME();
		return 0;
	}else if(*offset > BUFFER_SIZE) {
		pr_devel("cdrv: given offset %lld was out of buffer %d\n", *offset, BUFFER_SIZE);
		UPDATE_READ_CONSUMED_TIME();
		return -ESPIPE;
	}else if(*offset == BUFFER_SIZE) {
		pr_devel("cdrv: read: offset was exactly buffersize (%lld)\n", *offset);
		UPDATE_READ_CONSUMED_TIME();
		return 0;
	}
	if (*offset + count > BUFFER_SIZE) {
		count -= (*offset + count) - BUFFER_SIZE;
		pr_devel("cdrv: read: request way too much - reduced count to %zd\n", count);

		if(count == 0){
			pr_devel("cdrv: read: due to reduction nothing requested\n");
			syscall_end = current_kernel_time64();
			return 0;
		}
	}

	#ifdef DEBUG
		mdelay(1);
	#endif

	pr_devel("cdrv: read: FMODE_NDLAY: %d, may not block %d, O_NDELAY: %d, O_NONBLOCK: %d\n", filep->f_flags & FMODE_NDELAY, filep->f_flags & MAY_NOT_BLOCK, filep->f_flags & O_NDELAY, filep->f_flags & O_NONBLOCK);

	do{
		pr_devel("cdrv: read: already read: %zd, count=%zd\n", already_read, count);
		CLAIM_SEMAPHORE();

		if ( *offset >= dev->current_length ) {
			pr_devel("cdrv: read: open write-processes: %ld\n", dev->count_open_write);
			if (filep->f_flags & O_NONBLOCK || dev->count_open_write == 0) {
				UPDATE_READ_CONSUMED_TIME();
				up(&dev->sync);
				pr_devel("cdrv: read: offset was greater-equals current available characters (%lld requested from %zd buffer)\n", *offset, dev->current_length);
				pr_devel("cdrv: read: amount of writers was: %ld\n", dev->count_open_write);
				return already_read;
			}else{
				pr_devel("cdrv: read: now at offset %lld and nothing more in buffer\n", *offset);
				up(&dev->sync);
				pr_devel("cdrv: read: wait for queue %lld/%zd\n", *offset, dev->current_length);
				syscall_end = current_kernel_time64();
				pr_info("cdrv: read: waiting until file gets modified %ld.%09ld\n", syscall_end.tv_sec, syscall_end.tv_nsec);
				wait_event_interruptible(dev->waiting, (*offset < dev->current_length || dev->count_open_write == 0));
				pr_devel("cdrv: read: woke up");
				syscall_end = current_kernel_time64();
				pr_info("cdrv: read: file was modified %ld.%09ld\n", syscall_end.tv_sec, syscall_end.tv_nsec);
				pr_devel("cdrv: read: offset: %lld, current_length: %zd\n", *offset, dev->current_length);
				continue;
			}
		}
		to_read = MIN_VALUE_OF( count-already_read, dev->current_length - *offset );

		pr_devel("cdrv: read: read %zd bytes\n", to_read);
		not_copied = copy_to_user(buff, (dev->buffer + *offset), (unsigned long) to_read);
		if (not_copied != 0) {
			pr_devel("cdrv: read: could not read %lu bytes\n", not_copied);
			to_read -= not_copied;
		}
		pr_devel("cdrv: read: old offset: %lld, delta: %zd\n", *offset, to_read);
		*offset      += to_read;
		already_read += to_read;
		pr_devel("cdrv: read: [fyi] current_length: %zd, copied %zd, requested: %zd \n", dev->current_length, to_read, count);
		up(&dev->sync);
	}while(already_read < count);

	CLAIM_SEMAPHORE();
	UPDATE_READ_CONSUMED_TIME();
	up(&dev->sync);

	pr_devel("cdrv: read: finished\n");
	return already_read;
}

#define UPDATE_WRITE_CONSUMED_TIME() do{                                                    \
	syscall_end = current_kernel_time64();                                                    \
	update_used_time(&dev->time_used_write, &syscall_start, &syscall_end);                    \
	pr_devel("cdrv: write: syscall finished, total_consumed time for this call: %ld,%09ld\n", \
			dev->time_used_read.tv_sec,                                                           \
			dev->time_used_read.tv_nsec                                                           \
	);                                                                                        \
}while(0)

/**
 * @brief   mydev_write writes to character device
 * @param   filep (provided by kernel) FILE to be read
 * @param   buff source-buffer from write(2)
 * @param   count amounts of bytes that should be written from write(2)
 * @param   offset last position (provided by kernel) gets incremented by the return-value of this function
 * @returns bytes written to character device or -ERRNO
 */
static ssize_t mydev_write(struct file* filep, const char __user* buff, size_t count, loff_t* offset){
	my_cdev_t* dev = filep->private_data;
	size_t to_copy;
	size_t copied_total = 0;
	size_t final_position;
	unsigned long not_copied;
	struct timespec64 syscall_start;
	struct timespec64 syscall_end;
	syscall_start = current_kernel_time64();

	pr_devel("cdrv: write: requested count: %lu, offset: %lld\n", count, *offset);
	CLAIM_SEMAPHORE();
	dev->total_count_write += 1;

	if (buff == NULL){
		pr_devel("cdrv: write: got NULL buffer\n");
		UPDATE_WRITE_CONSUMED_TIME();
		up(&dev->sync);
		return -EINVAL;
	}else if (*offset > BUFFER_SIZE) {
		pr_devel("cdrv: write: given offset %lld was out of buffer %d\n", *offset, BUFFER_SIZE);
		UPDATE_WRITE_CONSUMED_TIME();
		up(&dev->sync);
		return -ESPIPE;
	}else if(*offset == BUFFER_SIZE && (filep->f_flags & O_NONBLOCK)) {
		pr_devel("cdrv: write: offset was exactly buffersize (%lld)\n", *offset);
		UPDATE_WRITE_CONSUMED_TIME();
		up(&dev->sync);
		return 0;
	}else if(count == 0) {
		pr_devel("cdrv: write: user requested 0 bytes\n");
		UPDATE_WRITE_CONSUMED_TIME();
		up(&dev->sync);
		return 0;
	}
	up(&dev->sync);

	#ifdef DEBUG
		mdelay(1);
	#endif

	while(count > 0){
		if(*offset >= BUFFER_SIZE){
			if(filep->f_flags & O_NONBLOCK){
				UPDATE_WRITE_CONSUMED_TIME();
				up(&dev->sync);
				return copied_total;
			}else{
				pr_devel("cdrv: write: wait for ioctl\n");
				syscall_end = current_kernel_time64();
				pr_info("cdrv: write: waiting started until the buffer get flushed %ld.%09ld\n", syscall_end.tv_sec, syscall_end.tv_nsec);
				wait_event_interruptible(dev->ioctl_cleared, (dev->current_length != BUFFER_SIZE));
				pr_devel("cdrv: write: ioctl done\n");
				syscall_end = current_kernel_time64();
				pr_info("cdrv: write: waiting finished the buffer was flushed %ld.%09ld\n", syscall_end.tv_sec, syscall_end.tv_nsec);
				*offset = 0;
			}
		}

		CLAIM_SEMAPHORE();
		to_copy = MIN_VALUE_OF(count, (BUFFER_SIZE - *offset));
		pr_devel("cdrv: write: copy %zd bytes\n", to_copy);

		not_copied = copy_from_user(dev->buffer + *offset, buff+copied_total, to_copy);
		if(not_copied != 0){
			pr_devel("cdrv: write: could not copy %lu bytes\n", not_copied);
			to_copy -= not_copied;
		}

		final_position = *offset + to_copy;
		if(dev->current_length < final_position)
			dev->current_length = final_position;
		*offset += to_copy;

		count -= to_copy;
		up(&dev->sync);
		copied_total += to_copy;
		pr_devel("cdrv: write: current_length: %zd, copied %zd, requested: %zd, offset: %lld \n", dev->current_length, to_copy, count, *offset);
	}

	if(to_copy > 0){
		pr_devel("cdrv: write: waking up clients\n");
		wake_up_all(&dev->waiting);
		pr_devel("cdrv: write: woke up the readers\n");
	}

	CLAIM_SEMAPHORE();
	UPDATE_WRITE_CONSUMED_TIME();
	up(&dev->sync);

	return copied_total;
}

/**
 * @brief   mydev_ioctl function for handling `ioctl`-calls
 * @param   filep pointer to the `/dev/mydev`-file
 * @param   cmd the command issued by the user
 * @param   arg parameter passed by user
 * @note    in this function a dirty-read is possible because the semaphore is only used in "reset"
 * @see my_ioctl.h for the defines
 * @see [ioctl macros from `uapi/asm-generic/ioctl.h`](http://lxr.free-electrons.com/source/include/uapi/asm-generic/ioctl.h#L86)
 * @see [howto waitqueue in kernel](https://rplinux.blogspot.co.at/2009/10/kernel-wait-queues.html) and `wait_event_interruptible`
 * @returns usualy nothing, except a specific value was requested.
 *
 * this ioctl checks if the command contains the magic 'z' (=172)
 *
 * ioctl values:
 *
 * action     | cmd |              MACRO
 * :----------|----:|------------------:
 * Read count |    5|    IOC_OPENREADCNT
 * Write count|    6|   IOC_OPENWRITECNT
 * reset      |    7|       IOC_NR_CLEAR
 */
static long mydev_ioctl(struct file* filep, unsigned int cmd, unsigned long arg){
	unsigned long temp;
	my_cdev_t* dev = filep->private_data;

	pr_devel("cdrv: ioctl: got cmd %u\n", cmd);
	if (_IOC_TYPE(cmd) != IOC_NR_TYPE) {
		pr_info("cdrv: ioctl: got wrong magic! was %u instead of 'z' = 172\n", _IOC_TYPE(cmd));
		return -EINVAL;
	}

	// no sema-lock: because i don't care about possible dirty read
	switch( _IOC_NR(cmd) ){
		case IOC_NR_OPENREADCNT:
			pr_info("cdrv: ioctl: count open read: %lu\n", dev->count_open_read);
			return dev->count_open_read;
		case IOC_NR_OPENWRITECNT:
			pr_info("cdrv: ioctl: count open write: %lu\n", dev->count_open_write);
			return dev->count_open_write;
		case IOC_NR_CLEAR:
			pr_info("cdrv: ioctl: resetting device\n");
			CLAIM_SEMAPHORE();
			dev->current_length = 0;
			dev->total_count_clear += 1;
			up(&dev->sync);
			wake_up_all(&dev->ioctl_cleared);
			pr_devel("cdrv: ioctl: completed clear\n");
			return 0;
		case IOC_NR_MAX_READERS:
			pr_devel("cdrv: ioctl: setting max-reader count...\n");
			CLAIM_SEMAPHORE();
			pr_devel("cdrv: ioctl: before=%lu\n", dev->allowed_readers);
			temp = dev->allowed_readers;
			dev->allowed_readers = arg;
			pr_devel("cdrv: ioctl: after=%lu\n", dev->allowed_readers);
			up(&dev->sync);
			return temp;
		default:
			pr_info("cdrv: ioctl: unknown command %d\n", _IOC_NR(cmd));
			return -ENOIOCTLCMD;
	}
}


static int my_seq_file_open(struct inode* inode, struct file* filp){
	static struct seq_operations my_seq_ops =
	{
		.start = my_start,
		.next = my_next,
		.stop = my_stop,
		.show = my_show
	};

	return seq_open(filp, &my_seq_ops);
}

static void* my_start(struct seq_file* sf, loff_t* pos){
	pr_info("cdrv: my_start: start\n");
	pr_devel("cdrv: my_start: pos=%lld\n", *pos);
	if(*pos == 0){
		return my_devs;
	}

	return NULL;
}

static void* my_next(struct seq_file* sf, void* v, loff_t* pos){
	pr_info("cdrv: my_next: next\n");
	(*pos)++;
	if( *pos >= MINOR_COUNT ) {
		return NULL;
	}
	return &my_devs[*pos];
}

/**
 * @brief   my_show prints the "iterator" for the /proc file
 * @param   sf seq-file-struct whith relevant infos
 * @param   it points of result of my_next --> points to current device
 * @returns 0
 */
static int my_show(struct seq_file* sf, void* it){
	my_cdev_t* dev = (my_cdev_t*) it;
	int index = MINOR(dev->cdev.dev);

	// index:
	// 1) MINOR
	// 2) dev-my_devs
	pr_devel("cdrv: my_show: current index: %d\n", index);

	CLAIM_SEMAPHORE();
	if(index == 0){
		seq_printf(sf, "DEVICE-NAME ╻ CURRENT LENGTH ╻ READ ╻ WRITE ╻ OPEN ╻ CLOSE ╻ CLEAR ╻ TIME READ (s) ╻ TIME WRITE (s) \n");
		seq_printf(sf, "━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━╇━━━━━━╇━━━━━━━╇━━━━━━╇━━━━━━━╇━━━━━━━╇━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━\n");
	}
	seq_printf(sf,   "mydev%d      │ %14zd │ %4llu │ %5llu │ %4lld │ %5lld │ %5lld │ %9ld,%03ld │ %10ld,%03ld \n",
			index, dev->current_length, dev->total_count_read, dev->total_count_write,
			dev->total_count_open, dev->total_count_close, dev->total_count_clear,
			dev->time_used_read.tv_sec, dev->time_used_read.tv_nsec/1000000,
			dev->time_used_write.tv_sec, dev->time_used_write.tv_nsec/1000000
	);
	up(&dev->sync);
	return 0;
}

static void my_stop(struct seq_file* sf, void* it){
	pr_info("cdrv: my_stop: stop\n");
	// possible cleanups here -> not neccessary here
}
