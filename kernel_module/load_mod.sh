#!/bin/bash

if [ -e "cdrv.ko" ] ; then
	if [ $(lsmod | grep cdrv | wc -l) -gt 0 ] ; then
		echo "unload module"
		sudo rmmod cdrv
	fi
	sudo insmod cdrv.ko
	echo "loaded module"
else
	echo "please make first"
fi
