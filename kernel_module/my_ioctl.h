/**
 * @file my_ioctl.h
 * @brief all the nice defines for interacting with the driver
 */

#ifndef MY_IOCTL_H
#define MY_IOCTL_H

#include <asm-generic/ioctl.h>

#define IOC_NR_TYPE          'z'  ///< the magic-value for the device

#define IOC_NR_OPENREADCNT     5  ///< command to query current amount of connected readers
#define IOC_NR_OPENWRITECNT    6  ///< command to query current amount of connected writers
#define IOC_NR_CLEAR           7  ///< command to tell the driver to clear the buffer
#define IOC_NR_MAX_READERS     8  ///< command to set the maximum reader count (0 for infinite)

#define IOC_OPENREADCNT   _IO(IOC_NR_TYPE, IOC_NR_OPENREADCNT)     ///< macro to query the reader-count (magic and command)
#define IOC_OPENWRITECNT  _IO(IOC_NR_TYPE, IOC_NR_OPENWRITECNT)    ///< macro to query the writer-count (magic and command)
#define IOC_CLEAR         _IO(IOC_NR_TYPE, IOC_NR_CLEAR)           ///< macro to command the writer to drop the buffer (magic and buffer)
#define IOC_SET_READERS   _IOW(IOC_NR_TYPE, IOC_NR_MAX_READERS, unsigned long) ///< macro to set max amount of readers



#define DEV_IOSCLRBUF IOC_CLEAR

#endif
