

## file operations
### function prototypes
see `/usr/src/linux-headers-4.8.0-26/include/linux/fs.h` line 1681

### manpages 

see https://www.fsl.cs.sunysb.edu/kernel-api/re940.html 

see http://www.makelinux.net/books/lkd2/ch11lev1sec4

see http://www.linuxcertif.com/man/9/device_create/

###ernos
see `man errno`  
see `/usr/src/linux-headers-4.8.0-26/include/uapi/asm-generic/errno-base.h`

errors are signed longs, becase the `IS_ERR` from `linux/err.h` uses it this way

