#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "cdrv_ioctl.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define DEV_NAME "/dev/mydev0"

int main()
{
	int wfd, rfd, readBytes;
	char buff[1024];
	char wbuff[1024];

	memset(wbuff, 'a', 1024);
	wbuff[1019] = 'y';
	wbuff[1020] = 'e';
	wbuff[1021] = 'a';
	wbuff[1022] = 'y';
	wbuff[1023] = '\0';


	wfd = open(DEV_NAME, O_WRONLY);
	rfd = open(DEV_NAME, O_RDONLY);

	write(wfd, "123", 3);

	switch(fork())
	{
		case 0:
			close(wfd);
			readBytes = read(rfd, buff, 4);
			if(readBytes < 4)
			{
				fprintf(stderr, "4 bytes lesen: Fehler beim Lesen!\n");
				return -1;
			}
			buff[4] = '\0'; // last byte should 0-term
			printf("4 bytes lesen:success. Output: %s\n", buff);

			readBytes = read(rfd, buff, 1024);
			if(readBytes != 1020)
			{
				fprintf(stderr, "1020 bytes lesen: Error reading the file. Only read %d \n", readBytes);
				return -1;
			}
			buff[1021] = '\0'; // last byte shouls be 0-term
			printf("1020 bytes lesen: success. Output: %s\n", buff);
			close(rfd);
			return 0;
			break;
		default:
			break;
	}

	write(wfd, "34", 2);
	// test blocking write of 1024 bytes
	switch(fork())
	{
		case 0:
			if( write(wfd, wbuff, 1024) != 1024)
			{
				fprintf(stderr, "1024 bytes write: Error on writing 1024 bytes.\n");
				return -1;
			}
			printf("1024 bytes write: success. Output: 1024 bytes written\n");
			close(wfd);
			return 0;
			break;
		default:
			getc(stdin);
			ioctl(wfd, DEV_IOSCLRBUF);
			break;
	}

	close(rfd);
	rfd = open(DEV_NAME, O_RDONLY);
	readBytes = read(rfd, buff, 4); // read the last 4 bytes
	if(readBytes < 4)
	{
				fprintf(stderr, "4 bytes read: Fehler beim Lesen!\n");
				return -1;
	}
	buff[4] = '\0'; // last byte should 0-term
	printf("4 bytes read:success. Output: %s\n", buff);

	
	close(rfd);
	close(wfd);
}
