#!/bin/bash

if [ $(lsmod | grep cdrv | wc -l) -gt 0 ] ; then
	sudo rmmod cdrv
	echo "module unloaded"
else
	echo "module is not active"
fi
