# Angaben
## Archivere das Archiv (10)
Erweitern Sie Ihr Backup-Programm dahingehend, dass das Tool den User fragt ob die Datei wirklich gesichert werden soll, wenn es eine Backup-Datei von ihrem Tool antrifft.

## Maximum File Size (7)
Erweitern Sie Ihr Backup-Programm dahingehend, dass Sie über die Environment-Variable `MAXFILESIZE` steuern können wie groß eine Datei sein darf die Sie archivieren.

## Maximum Backup Size (10)
Erweitern Sie Ihr Backup-Programm sodass Sie über die Environment-Variable `MAXBACKUPSIZE` steuern können wie viele Bytes Ihre Archiv-Datei maximal sein darf.

## Characterdevice (20)
Erweitern Sie Ihr Backup-Programm so, dass Sie Character-Davices sichern und wiederherstellen können (Major und Minor-Nummer reichen als Identifikation für den Nod)
<!-- http://man7.org/linux/man-pages/man2/mknod.2.html -->

## `--list` Archiv-Inhalt (15)
Erweitern Sie ihr Restor-Programm so, dass über den Kommandozeilen-Schalter `--list` ein "`ls -l`"-ähnlich der Inhalt des Archiv-Files ausgegeben wird

## `!!` in Shell (10)
Implementieren Sie in Ihrem Threaded-Shell-Server den internen Befehl `!!` welcher das vorherige Kommando erneut ausführt.

## Git or not Git? (10)
Erweitern Sie ihren Threaded-Shell-Server so, dass es ein internes Kommando `isgit` gibt welches prüft ob oder aktueller Ordner Teil eines Git-Repositiories ist.

Erweitern Sie anschließend ihren Prompt dahingehend, das dem User dieser Sachverhalt angezeigt wird.

## Shell per FIFO (8)
Erweitern Sie ihren Shellserver (egal welchen), sodass dieser Kommandos aus der benannten Pipe `/tmp/shell_sock` liest und abarbeitet. Der Output soll dabei am Server dargestellt werden.

## Log or Log (10)
Erweitern Sie ihren Shellserver (egal welchen) dahingehend, dass Sie beim Start prüfen ob die benannte Pipe `/tmp/MATNR_LOG` existiert und benutzt werden kann (ohne Warten zu müssen!).  
Sollte diese Kriterien zutreffen soll die History des Shell-Servers zusätzlich in diese benannte Pipe gespeichert werden.

## Defrag Kernöl (20)
Erweitern Sie ihr Kernel-Modul so, dass Sie mit einem IOCTL-Befehl die Größe der Buffer steuern können.  
Die Größe eines jeden Gerätes soll so gesetzt werden, dass nur mehr so viele Bytes gespeichert werden können wie sich aktuell in dem jeweiligen Gerät befinden.  
Geben Sie anschließend die Anzahl der Bytes zurück die sie "einsparen" können.

Beachten Sie, dass der Inhalt dabei nicht verändert werden darf.

## Kernö info (8)
Erweitern Sie ihr Kernel-Modul so, dass Sie in ihrem `/proc`-File sehen wie viele Bytes von jedem Device benutzt werden, sowie die maximal-Kapazität des jeweiligen Gerätes.


