## Vorbereitung
1. [mbedTLS](https://tls.mbed.org/) runterladen und entpacken
2. `make` ausführen.
3. beachte die verzeichnisstruktur:
	- Wurzel
		- mbedtls ordner
		- YOUR_APP ordner

## Angabe
Schreibe ein Programm das folgende Parameter bekommt

- Pfad zu einer Datei (input)
- Zeichnkette
- Pfad zu der Ausgabedatei

Prüfe die Parameter:
- die Input-Datei muss exisiteren
- Die zeichnkette muss 26 zeichen lang sein.

Das Programm sollte die Input-Datei komplett in den RAM laden um nachher einzelne Werte abzufragen.

Das programm soll nun prüfen ob die ersten 26 aus der Input-Datei (index 0 bis inclusive 25) zeichen = die angegebenen zeichnkette sind.
Das nächste zeichen (index 26) wird als Variable XXX abgespeichert

In die Variable YYY wird folgender Zahlewert abgelegt: 
`datei[26] + (datei[27] << 8) + (datei[28] << (2*8)) + (datei[29] << (3*8))`

Es wird ein 16 zeichen array angelegt, in welches das 31-47 zeichen (index 30 bis exclusive 46) aus der input-datei. Anschließend wird auf auf jede position im array die funktion "encode" aufgerufen (index=stelle in dem 16-zeichen aray; character=inhalt an der stelle in dem 16-zeichen array)

Anschlißend wird die Funktion "generate_sequence" mit dem Parameter XXX aufgerufen.

Danach wird YYY-byte Speicher allokiert.

Start-Pointer = adresse vom 46 index in der Datei.
Anschließend wird mit mbedtls AES128-CBC entschlüsselt (am ende der angabe befindet sich demo-code zum einsatz von mbedtls).
- Länge: YYY
- Input Start-pointer
- Output der YYY-byte große speicher

Anschließend wird die Output-Datei angelegt wenn diese nicht existiert.
Und der YYY-byte Große buffer hineingeschreiben.

### Funktion "genreate_sequence"
Parameter:
- zahlenwert (=XXX)

Die Funktion allokiert 16 Byte speicher und befüllt diese wie folgt:
zeichen an der Stelle i = i ^ zahlenwert

### Funktion "encode"

Parameter:
- Index
- Ein character

Die Funktion soll nun folgenden character errechnen: `((index * 2) & 0xff) ^ (ascii-wert von Eingabe-Character) ^ 0x55`

## Nützliche links
https://tls.mbed.org/api/group__encdec__module.html 


## Demo-Code für Einsatz von mbedTLS
```C
/*
How to encrypt data with AES-CBC mode
https://tls.mbed.org/kb/how-to/encrypt-with-aes-cbc

How to generate an AES key
https://tls.mbed.org/kb/how-to/generate-an-aes-key
*/

#include <mbedtls/aes.h>
#include <stdio.h>
#include <string.h>

void printHex(const void* buf, unsigned int length)
{
	const unsigned char* b = static_cast<const unsigned char*>(buf);

	for (unsigned int i = 0; i < length; i++) {
		printf(" %02x", b[i]);
	}
	printf("\n");
}

int main()
{
	mbedtls_aes_context aes;

	mbedtls_aes_init(&aes);

	unsigned char key[32] = "012345678901234567890123456789x"; // 256bit key
	if (mbedtls_aes_setkey_enc(&aes, key, 256)) {
		printf("set key failed\n");
	}

	unsigned char iv[16] = "012345678901234"; // 128bit IV
	unsigned char input [128];
	unsigned char output[128];
	memset(input, 0, 128);
	memset(output, 0, 128);
	memcpy(input, "0123456789012345", 16);

	if (mbedtls_aes_crypt_cbc(&aes, MBEDTLS_AES_ENCRYPT, 16, iv, input, output)) {
		printf("encrypt failed\n");
	}
	mbedtls_aes_free(&aes);

	int outlen = strlen(reinterpret_cast<const char*>(output));
	printf("output len %d\n", outlen);
	return 0;
}
```


```Makefile

all:
	g++ -I../mbedtls/include -c main.cpp
	g++ -L../mbedtls/library main.o -lmbedcrypto -lmbedtls -lmbedx509 -o testapp

clean:
	rm -rf main.o testapp
```

## Demo-Code2 für Einsatz von mbedTLS
```C
#include <mbedtls/cipher.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

typedef struct
{
    unsigned char iv[16];
    unsigned char key[32];
    uint16_t counter;
}
crypto_params;

void printHex(const void* buf, unsigned int length)
{
	const unsigned char* b = (const unsigned char*)(buf);
	for (unsigned int i = 0; i < length; i++) {
		printf(", 0x%02x", b[i]);
	}
	printf("\n");
}

size_t getAESOutputBufferSize(size_t data){
	return ((data/16)+1)*16;
}

typedef struct
{
	unsigned char* data;
	size_t size;
	size_t usage;
}buffer_t;

// MBEDTLS_DECRYPT / MBEDTLS_ENCRYPT
int crypt(crypto_params *params, unsigned char* data, size_t data_length,
		buffer_t* out_buf, mbedtls_operation_t operation)
{
	size_t counter_size = sizeof(params->counter);

	mbedtls_cipher_context_t ctx;
	int res = 0;
	mbedtls_cipher_init(&ctx);

	res = mbedtls_cipher_setup(&ctx,mbedtls_cipher_info_from_type(MBEDTLS_CIPHER_AES_256_CBC));
	if(res != 0) return res;

	res = mbedtls_cipher_setkey(&ctx, params->key,256, operation);
	if(res != 0) return res;

	res = mbedtls_cipher_set_padding_mode(&ctx, MBEDTLS_PADDING_PKCS7); //should be default
	if(res != 0) return res;

	size_t total_data = 0;
	unsigned char* input_data = NULL;

	if(operation == MBEDTLS_ENCRYPT){
		total_data = data_length+counter_size;
		input_data = (unsigned char*) malloc(total_data);
		unsigned char* ptr = input_data;
        memcpy(ptr,&params->counter, counter_size);    ptr += counter_size;
		memcpy(ptr,data, data_length);
	}else{
		total_data = data_length;
		input_data = data;
	}

	memset(out_buf->data,0x00,out_buf->size);

	res = mbedtls_cipher_set_iv(&ctx,params->iv,sizeof(params->iv));
	if(res != 0) return res;

	size_t len;
	res = mbedtls_cipher_update(&ctx,input_data,total_data,out_buf->data,&len);
	if(res != 0) return res;

	size_t finish_len;
	res = mbedtls_cipher_finish(&ctx,(out_buf->data+len),&finish_len);

	out_buf->usage = len + finish_len;
	if(operation == MBEDTLS_ENCRYPT){
		free(input_data);
	}else{
		memcpy(&params->counter,out_buf->data,counter_size);
		out_buf->usage -= counter_size;

        unsigned char* tmp = (unsigned char*) malloc(out_buf->usage);
        memcpy(tmp, out_buf->data+counter_size, out_buf->usage );
        memcpy(out_buf->data,tmp, out_buf->usage);
        memset(out_buf->data + out_buf->usage,0x00,out_buf->size - out_buf->usage);
        free(tmp);
	}

	mbedtls_cipher_free(&ctx);

	return res;
}

int main(int argc, char **argv) {
	unsigned char data[] = "whuiii";
	size_t input_size = strlen((char*) data)+1;
	crypto_params params;
	memset(params.iv, 0x00, sizeof(params.iv));
	strcpy((char*) params.key, "zqYsfNM2wtCThQfnQV9VX3NpMiASRRsY");
	params.counter = 1337;

	buffer_t output;
	output.size = getAESOutputBufferSize(input_size);
	output.data = (unsigned char*) malloc(output.size);
	output.usage = 0;

	printf("status: %d\n",crypt(&params, data, input_size, &output, MBEDTLS_ENCRYPT));

	printf("counter=%" PRIu16 "\ndata=%s\n",params.counter, data);
	printHex(output.data, output.usage);

	params.counter = 0;
	free(output.data);
	output.data = NULL;

	unsigned char data2[] = {0x13, 0xd7, 0x90, 0xc2, 0x7c, 0xda, 0xb2, 0x96, 0x23, 0x3b, 0x6b, 0xb2, 0x9b, 0x26, 0x97, 0x23};

	output.usage = 0;
	output.data = (unsigned char*) malloc(sizeof(data2));
	output.size = sizeof(data2);

	printf("status: %d\n",crypt(&params, data2, sizeof(data2), &output, MBEDTLS_DECRYPT ));
	printf("counter=%" PRIu16 "\ndata=%s\n",params.counter, output.data);

	free(output.data);
	output.data = NULL;

	return 0;
}
```
