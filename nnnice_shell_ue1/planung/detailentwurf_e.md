% Detailentwurf Ein- und Ausgabe-Umleitung
% Jung Clemens (is151014)
% 2016-10-26

# Ein Ausgabeumleitung

## Benutzung
Generell sollte bei der Planung von neuen Features von der Benutzbarkeit ausgegangen werden. 
Für einen durchschnittlichen Shell-User haben sich folgende grundlegende Formen der Umlenkung etabliert:

```zsh
command > output_file
command < input_file
command | command
```

Das heißt, es wird nur Ein- und/oder Ausgabe umgeleitet.

## Möglichkeiten
Am Einfachsten wäre wahrscheinlich die Umsetzung mittels ``dub2`` (siehe manpage) mit dessen Hilfe ein FileDescriptor durch einen anderen ersetzt werden kann.

Um die Implementierung ordentlich zu halten, sollte eine Liste von Kommandos erstellt werden, deren Argumente und deren Ein/Ausgabe FileDeskriptoren.

Diese Liste startet mit dem ersten Kommando und als Eingabe den FileDeskriptoren `0` und '-1' als Platzhalter für den Ausgabe-FileDeskriptoren.

Zuerst sollte über Flags geprüft werden ob umgelenkt werden soll. Dazu sind alle Tokens, die die Funktion ``segment_line`` liefert zu prüfen ob `<`, `>` oder `|` darin enthalten sind.
Sollte ein nächstes Kommando folgen wird eine _Pipe_ (siehe `man 2 pipe`) erzeugt. Diese Pipe wird beim vorherigen Kommando als Ausgabe und beim neuen Kommando als Eingabe eingetragen.  
Das letzte Kommando bekommt 1 als Ausgabe.

Sollte ein Operator folgen, der von einer Datei liest oder schreibt, so aktualisiert dieser die Werte auf einen mit `open` erzeugten FileDeskriptoren (je nach Richtung schreibend oder lesend).

Das Kommando `dub2` setzt diese Änderungen nach dem `exec` in dem jeweiligen Child-Prozess. Da unter Umständen mehr als ein Kommando zu starten ist, muss für jedes Kommando neu ge-`fork`-ed werden.

## Syscalls
Beim Umlenken werden vor allem IO-Syscalls benötigt, da pipes und files erzeugt werden müssen. Zudem muss jetzt nicht nur ein Sub-Prozess sondern mehrere gestartet werden.
