#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <time.h>

#define BUFFSIZE 1024
#define MAXPATHNAME 1024
#define PATH_SEPERATOR '/'

typedef struct stat stat_t;

void BackupNode(const char* src_name, const stat_t* target_inode, int target_fd);
int ProcessDir(const char* working_dir, const stat_t* target_inode, int target_fd);
int WriteMagic(int fd);

int main(int argc, const char *argv[])
{
	char* target = getenv("BACKUPTARGET");
	int target_fd = -1;
	stat_t target_inode;

	if (target == NULL) {
		fprintf(stderr, "Environment variable 'BACKUPTARGET' is not set.\n");
		exit(2);
	}

	target_fd = creat(target, 0640);

	if (target_fd < 0) {
		fprintf(stderr, "Could not create '%s'!\n", target);
		exit(3);
	}

	printf("[DBG] archive fd=%d\n", target_fd);
	if( WriteMagic(target_fd) < 0 ){
		exit(5);
	}

	if (fstat(target_fd, &target_inode) < 0) {
		fprintf(stderr, "Could not read inode '%s'(%d)\n", target, target_fd);
		exit(4);
	}

	// Parametercheck
	if ( argc == 1 ) {
		BackupNode(".", &target_inode, target_fd);
	}else{
		int i;
		for(i = 1; i < argc; ++i){
			BackupNode(argv[i], &target_inode, target_fd);
		}
	}

	close(target_fd);
	printf("All done\n");
	return 0;
}

int WriteMagic(int fd){
	char* owner_name = "JUNG";
	printf("[DBG] owner_name: %s (%lu)\n", owner_name, strlen(owner_name));

	struct tm owner_birthdate;
	owner_birthdate.tm_sec = 0;
	owner_birthdate.tm_min = 0;
	owner_birthdate.tm_hour= 0;
	owner_birthdate.tm_mday= 6;
	owner_birthdate.tm_mon = 0;
	owner_birthdate.tm_year= 94;
	owner_birthdate.tm_isdst=-1; // lookup in db


	time_t owner_birthdate_epoch = mktime(&owner_birthdate);

	if(
			write(fd, owner_name, strlen(owner_name)) < 0 ||
			write(fd, &owner_birthdate_epoch, sizeof(owner_birthdate_epoch)) < 0
		){
		fprintf(stderr, "writing magic failed!\n");
		return -1;
	}
	printf("[DBG] wrote magic to fd %d\n", fd);
	return 0;
}

int write_inode_and_name(const stat_t* src_inode, int target_fd, const char* src_name){
	// write inode
	if ( write(target_fd, src_inode, sizeof(stat_t)) != sizeof(stat_t) ){
		fprintf(stderr, "could not write inode info of %s\n", src_name);
		return -1;
	}

	// write name
	if ( write(target_fd, src_name, strlen(src_name) + 1) != (ssize_t)(strlen(src_name) + 1)){
		fprintf(stderr, "could not write filename of %s\n", src_name);
		return -1;
	}

	printf("[DBG] %s: wrote meta info to fd %d\n", src_name, target_fd);
	return 0;
}

int write_content(const char* src_name, int target_fd){
	int fd = -1;
	char buf[BUFFSIZE];
	ssize_t read_bytes = 0;

	fd = open(src_name, O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "Could not open file to read: %s\n", src_name);
		return -1;
	}

	while ( (read_bytes = read(fd, buf, BUFFSIZE)) > 0 ){
		if ( write(target_fd, buf, read_bytes) != read_bytes ) {
			fprintf(stderr, "could not wirte all bytes to targetfile! (src: %s)\n", src_name);
			return -1;
		}
		printf("[DBG] %s: copied %zd bytes\n", src_name, read_bytes);
	}

	close(fd);

	printf("[DBG] %s: wrote content\n", src_name);
	return 0;
}

void BackupNode(const char* src_name, const stat_t* target_inode, int target_fd){
	printf("[DBG] Archive-File: %s\n", src_name);

	stat_t src_inode;

	if (lstat(src_name, &src_inode) < 0) {
		fprintf(stderr, "Could not lstat '%s'\n", src_name);
		return;
	}

	if (
			target_inode->st_dev == src_inode.st_dev && // selbes device
			target_inode->st_ino == src_inode.st_ino    // selbe inode
	) {
		printf("ignored file %s: same inode\n", src_name);
		return;
	}

	if ( S_ISLNK(src_inode.st_mode) ) {
		printf("%s is symbolic link; currently not supported\n", src_name);
		return;
	}else if ( S_ISDIR(src_inode.st_mode) ) {
		ProcessDir(src_name, target_inode, target_fd);
	}else if ( S_ISREG(src_inode.st_mode) ) {
		printf("Archive regular file %s\n", src_name);
		printf("[DBG] Size of file: %ld\n", src_inode.st_size);

		if ( write_inode_and_name(&src_inode, target_fd, src_name) < 0 ){
			fprintf(stderr, "Could not write meta-info of %s\n", src_name);
			return;
		}else if( write_content(src_name, target_fd) < 0 ){
			fprintf(stderr, "Could not write file-content of %s\n", src_name);
			return;
		}
	} else if (S_ISFIFO(src_inode.st_mode)) {
		printf("Archive named pipe %s\n", src_name);
		if ( write_inode_and_name(&src_inode, target_fd, src_name) < 0 ){
			fprintf(stderr, "Could not write meta-info of %s\n", src_name);
			return;
		}
	} else {
		printf("%s is special file; currently not supproted\n", src_name);
		return;
	}
}

int ProcessDir(const char* working_dir, const stat_t* target_inode, int target_fd){
	printf("[DBG] processing dir %s\n", working_dir);
	DIR* directory = NULL;
	struct dirent* dir_entry = NULL;
	char line_buffer[MAXPATHNAME];
	size_t working_dir_len = strlen(working_dir);
	if(working_dir_len + 2 >= MAXPATHNAME){ // null + /
		fprintf(stderr, "working-path %s it to long\n", working_dir);
		return -1;
	}
	strncpy(line_buffer, working_dir, MAXPATHNAME);
	line_buffer[working_dir_len] = PATH_SEPERATOR;
	working_dir_len += 1;

	directory = opendir(working_dir);
	if (directory == NULL) {
		fprintf(stderr, "Could not open directory: %s\n", working_dir);
		return -1;
	}

	stat_t workdir_inode;
	if (lstat(working_dir, &workdir_inode) < 0 ) {
		fprintf(stderr, "Could not stat working-dir %s\n", working_dir);
		return -1;
	}
	if ( write_inode_and_name(&workdir_inode, target_fd, working_dir) < 0 ){
		fprintf(stderr, "Could not write meta-info of %s\n", working_dir);
		return -1;
	}

	while( (dir_entry = readdir(directory)) != NULL) {

		int dir_entry_name_length = strlen(dir_entry->d_name);
		printf("[DBG] entry: %s\n", dir_entry->d_name);
		strncpy(
				&line_buffer[working_dir_len],
				dir_entry->d_name,
				MAXPATHNAME-working_dir_len);
		line_buffer[working_dir_len+dir_entry_name_length+1] = '\0';
		if (dir_entry->d_type & DT_DIR ) {
			if( dir_entry->d_ino ==  workdir_inode.st_ino ||
					strcmp(dir_entry->d_name, "..") == 0 )
				continue;

			printf("[DBG] process dir-entry %s\n", line_buffer);
			ProcessDir(line_buffer, target_inode, target_fd);
		}else{
			printf("[DBG] process dir-entry-file %s\n", line_buffer);
			BackupNode(line_buffer, target_inode, target_fd);
		}
	}
	return 0;
}
