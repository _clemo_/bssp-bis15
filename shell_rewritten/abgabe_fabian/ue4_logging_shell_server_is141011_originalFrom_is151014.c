/**
 * \file
 * \brief Shellserver - process-based, pre-threaded
 * \author Burak Domink (is151041), Szing Marcus (is151033), Pointner Thomas (is151023), Fabian Friesenecker (is141011), Jung Clemens (is151014)
 * Threaded Shellsevrer REWRITTEN
 *
 * \bug the programm leaks some memory according to valgrind  (65 bytes just by strting and typing exit and 75 bytes if exit was issued pver the network)
 */
#define _GNU_SOURCE
#define _XOPEN_SOURCE 500
#define _POSIX_C_SOURCE 200112L

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/utsname.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include <signal.h>
#include <semaphore.h>
#include <fcntl.h>
#include <pthread.h>

#define MAX_INCOMING_QUEUE		6
#define PORTNUMBER            4033
#define MAX_LINE_LENGHT       1024
#define MAX_ARGUMENT_COUNT    100
#define LOGFILE               "/tmp/serverlog"
#define MAX_HISTORY_ENTRIES   1000

/**
 * Table of the current server state. It holds all the needed filedescriptors, and the current working state of the shell.
 */
typedef struct serverstate_s{
	char*   current_work_dir;  //!< working dir of the process
	pid_t   current_PID;		   //!< pid_t data type to store the PID from the fork process
	int     client_FD;
	struct{
		uid_t uid;
		uid_t euid;
		uid_t gid;
		uid_t egid;
	}user_info;
	char*   nodename;
	char*   ip_address;
	time_t  start_time;
	mode_t  current_umask;     //!< umask of the process
}serverstate_t;

typedef struct{
	serverstate_t* calling_state; //!< cller state needed for logging
	char** args;                  //!< arguments needed for logging -> pointer will be freed
	int argc;                     //!< argument coutner needed for logging
	pid_t pid;                    //!< pid to wait for
	char* line;                   //!< will be freed
}background_task_t;

int server_init(int portnum);
int client_disconnect(int fd);
int server_state_init(serverstate_t* state, int fd);
void shell_loop(serverstate_t* state);
void shell_promt(serverstate_t* state);
char* shell_readline();
char** shell_segement_line(char* line, int* out_count);
int shell_internals(serverstate_t* state, char** args, int argc);
mode_t get_umask();
void shell_register_signals();
int log_command(serverstate_t* state, char** args, int argc);
char** log_read(int* n);

void* handle_clients(void* );

serverstate_t* current_state = NULL;
sem_t logfile_sem;
sem_t errno_sem;
//TODO: create a global variable for the server wide logfile with ONE fucking FD. (casue server state is "per shell")

extern char **environ;

int main()
{
	if(sem_init(&logfile_sem, 1, 1) != 0){		//init logfile semaphore
		fprintf(stderr, "could not create logfile semapthore\n");
		exit(0);
	}

	if(sem_init(&errno_sem, 1, 1) != 0){		//init errno semaphore
		fprintf(stderr, "could not create errno semapthore\n");
		exit(0);
	}

	serverstate_t* server_state = malloc(sizeof(serverstate_t));
	if(server_state_init(server_state, 1) == -1){
		fprintf(stderr, "could not init server state\n");
		free(server_state);
		exit(1);
	}

	pthread_t threadID;
	int threadErrInfo = pthread_create(&threadID, NULL, handle_clients, NULL);
	if(threadErrInfo){
		fprintf(stderr, "Could not start SHELL-SERVER-Thread! %s\n", strerror(threadErrInfo));
	}

	server_state->ip_address = "LOCAL MACHINE";
	printf("initiated state for %s\n", server_state->ip_address);
	shell_loop(server_state);
	return 0;
}

void* handle_clients(void* args){
	int server_fd = server_init(PORTNUMBER);
	if ( server_fd == -1) {
		exit(1);
	}

	struct sockaddr_in client_addr;
	socklen_t client_addr_len = sizeof(client_addr);

	for(;;){
		printf("waiting for clients on port %d\n", PORTNUMBER);
		int client_fd = accept(server_fd, (struct sockaddr*) &client_addr, &client_addr_len); //CLIENTS ANNEHMEN
		if ( client_fd == -1)
		{
			fprintf(stderr, "Could not accept %s\n", strerror(errno));
			close(server_fd);
			exit(2);
		}
		printf("Client has connected.\n");

		pid_t pid = fork();

		if (pid == -1)		// -1: ON ERROR - no clone (child) exists.
		{
			fprintf(stderr, "Could not fork %s\n", strerror(errno));
			client_disconnect(client_fd);
			close(server_fd);
			exit(3);
		}
		else if(pid == 0)	// 0: if we are the clone (child)
		{
			shell_register_signals();

			serverstate_t* server_state;
			server_state = malloc(sizeof(serverstate_t));

			if(server_state_init(server_state, client_fd) == -1){
				client_disconnect(client_fd);
				exit(1);
			}
			server_state->ip_address = strdup(inet_ntoa(client_addr.sin_addr));
			printf("initiated state for %s\n", server_state->ip_address);

			/** stderr, stdin und stdout redirected to the client_fd e.g. THE NETWORK SOCKET */
			dup2(client_fd, 0);
			dup2(client_fd, 1);
			dup2(client_fd, 2);
			current_state = server_state;  //overwrite the server_state of the child

			shell_loop(server_state);
			client_disconnect(client_fd);
		}
		else				// some number: if we are the PARENT
		{
			printf("Spawned child process with PID %d\n", pid);
		}
	}
}

/**
 * init the main server sockets and stuff (to reuse socket)
 * @param portnum portnumber to use
 * @return -1 on ERROR, server filedescriptor as int.
 */
int server_init(int portnum)
{
	struct sockaddr_in server_addr;
	int err;

	server_addr.sin_addr.s_addr = INADDR_ANY;
	server_addr.sin_port        = htons(portnum);
	server_addr.sin_family      = AF_INET;

	int server_fd = socket(PF_INET, SOCK_STREAM, 0);
	if ( server_fd == -1) {
		fprintf(stderr, "Could not open socket %s\n", strerror(errno));
		return -1;
	}

	socklen_t server_addr_len = sizeof(server_addr);
	err = bind(server_fd, (struct sockaddr*) &server_addr, server_addr_len);
	if( err == -1){
		fprintf(stderr, "Could not bind on socket %s\n", strerror(errno));
		return -1;
	}

	err = listen(server_fd, MAX_INCOMING_QUEUE);
	if( err == -1){
		fprintf(stderr, "Could not listen on socket %s\n", strerror(errno));
		return -1;
	}
	/** CODE TO REUSE SOCKET **/
	int yes = 1;
	if(setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int))){
		fprintf(stderr, "Could declare reusable socket %s\n", strerror(errno));
		return -1;
	}

	return server_fd;
}

/**
 * function to disconnect the client gracefully (or at least clean) - comfort function, because the client gets notified that the server has disconnected him. And there will be no ghost TCP sessions in the server.
 * @param fd the clients filedescriptor which should be closed (comes from the state)
 * @return -1 on Error (but WHY???), 0 on successful disconnect.
 */
int client_disconnect(int client_fd)
{
	int err = 0;
	if(shutdown(client_fd, SHUT_RDWR) == -1)
		err = 1;
	if(close(client_fd) == -1)
		err = 1;
	return (err == 0 ? 0 : -1);
}

/**
 * Init the server state struct on a new server to start.
 * @param state server state which should be initialized.
 * @param fd the clients filedescriptor
 * @return -1 on ERROR, 0 on successful initialization.
 */
int server_state_init(serverstate_t* state, int fd)
{
	state->current_work_dir = get_current_dir_name(); // free me
	state->client_FD        = fd;

	struct utsname uname_buffer;
	if( uname(&uname_buffer) == -1) {
		fprintf(stderr, "could not collect user-date %s\n", strerror(errno));
		return -1;
	}
	state->nodename = strdup(uname_buffer.nodename);

	state->user_info.uid = getuid();
	state->user_info.euid = geteuid();
	state->user_info.gid = getgid();
	state->user_info.egid = getegid();

	sem_wait(&errno_sem);
	errno = 0;//LETZTER ERROR CODE VON EINEM SYSTEMCALL ABGELEGT (-1)
	time(&state->start_time);
	if(errno){
		fprintf(stderr, "could not collect time info %s\n", strerror(errno));
		sem_post(&errno_sem);
		return -1;
	}
	sem_post(&errno_sem);

	state->current_umask = get_umask();
	state->current_PID = 0;

	return 0;
}

/**
 * function to form and print out the shell prompt to stdout.
 * @param state server state to form the prompt output. (from assignment)
 */
void shell_promt(serverstate_t* state)
{
	printf("\e[36m%s\e[0m \e[33m%s\e[0m >> ", state->nodename, state->current_work_dir);
	fflush(stdout);
}

/**
 * reads one line and makes error handling, cleanups (terminates the string when backslash r appears.) 
 * @return returns a nice null-terminated string (or at leas the pointer to it on the heap), returns NULL on error.
 * @note free me
 */
char* shell_readline()
{
	char line_buffer[MAX_LINE_LENGHT];

	if( fgets(line_buffer, MAX_LINE_LENGHT, stdin) == NULL )
		return NULL;

	size_t line_length = strlen(line_buffer);

	if(line_buffer[line_length - 1] == '\n'){
		line_length -= 1;
		line_buffer[line_length] = '\0';
	}
	/** TERMINITE STRING AT backslash-r **/
	char* p = strchr(line_buffer, '\r');
	if(p != NULL)
	{
	*p = '\0';
	}
	//TODO FREE ME!!!
	return strdup(line_buffer);
}

/**
 * segment line into a nice vector of char pointers.
 * @param line the line to dissect
 * @param argument_count the int pointer in which the number of elements in the list is written to.
 * @return a list (vector) of entered commands
 * @note do not free strings -> they are created with strtok. but you need to free the whole vector
 */
char** shell_segement_line(char* line, int* argument_count)
{
	char** vector = malloc(sizeof(char*) * MAX_ARGUMENT_COUNT);
	int i = 0;
	vector[i] = strtok(line, " \t");
	while(vector[i] != NULL && i < MAX_ARGUMENT_COUNT){
		i += 1;
		vector[i] = strtok(NULL, " \t");
	}
	if(i >= MAX_ARGUMENT_COUNT)
		vector[MAX_ARGUMENT_COUNT - 1] = NULL;

	/** write the number of elements into out_count */
	*argument_count = i;

	return vector;
}
/**
 * function to print out the UID/EUID/GID/EGID. (from assignment)
 * @param state server state to read out needed UID/EUID/GID/EGID infos.
 */
void print_id(serverstate_t* state)
{
	struct passwd* tmp_user;
	struct group* tmp_group;

	printf("ID:\n");
	printf("\tuid: %d (%s)\n", state->user_info.uid,
		((tmp_user = getpwuid(state->user_info.uid)) == NULL ? "" : tmp_user->pw_name)
	);
	printf("\teuid: %d (%s)\n", state->user_info.euid,
		((tmp_user = getpwuid(state->user_info.euid)) == NULL ? "" : tmp_user->pw_name)
	);
	printf("\tgid: %d (%s)\n", state->user_info.gid,
		((tmp_group = getgrgid(state->user_info.gid)) == NULL ? "" : tmp_group->gr_name)
	);
	printf("\tegid: %d (%s)\n", state->user_info.egid,
		((tmp_group = getgrgid(state->user_info.egid)) == NULL ? "" : tmp_group->gr_name)
	);
}

/**
 * function to print all the infos. (from assignment)
 * @param state server state to read out the current shell infos. Needed for PID and shell start time.
 */
void print_info(serverstate_t* state)
{
	printf("INFO ZU SOCKEN-SCHELL\n");
	printf("\tShell von: Burak Domink (is151041), Szing Marcus (is151033), Pointner Thomas (is151023)\e[1;30m, Jung Clemens (is151014)\e[0m\n");
	printf("\tPID: %d\n", getpid());
	printf("\tShell läuft seit %s\n", ctime(&state->start_time));
}

/**
 * function to change the working dir.
 * @param state server state to be altered by the get_current_dir_name function.
 * @param args directory to change to
 * @param argc argument count to check for errors in syntax.
 * @return -1 on ERROR, 0 on successful set of the current work dir.
 */
int change_working_dir(serverstate_t* state, char** args, int argc)
{
	if(argc != 2){
		printf("Bitte cd mit dem neuen Pfad aufrufen\n");
		return -1;
	}

	if(chdir(args[1]) == 0){
		free(state->current_work_dir);
		state->current_work_dir = get_current_dir_name();	// FREE ME!!!
		return 0;
	}else{
		printf("CD did not work %s\n", strerror(errno));
		return -1;
	}
}

/**
 * function which returns the umask. needed in various scenarious (e.g. to print the umask, to write it to the server state, etc...)
 * @return the umask in mode_t type.
 */
mode_t get_umask()
{
	mode_t res = umask(0);
	umask(res);
	return res;
}

/**
 * function to print the environment
 */
void print_environment()
{
	char** ptr = environ;
	printf("ENVIRONMENT\n");
	while(*ptr != NULL){
		printf("%s\n", *ptr);
		ptr += 1;
	}
}

/**
 * function to handle internal shell commands
 * @param state server state is needed for some comments and may be altered.
 * @param args the vector of stored arguments
 * @param argc count of arguments (because we need to check in some commands if they have the right AMOUNT of args)
 * @return -1 if error, 0 if no match on an internal comment, 1 if internal command AND successful, and 2 if we need to exit
 */
int shell_internals(serverstate_t* state, char** args, int argc)
{
	if(strcmp(args[0], "pwd") == 0){
		printf("%s\n", state->current_work_dir);
	}else if(strcmp(args[0], "id") == 0){
		print_id(state);
	}else if(strcmp(args[0], "exit") == 0){
		client_disconnect(state->client_FD);
		return 2;
	}else if(strcmp(args[0], "info") == 0){
		print_info(state);
	}else if(strcmp(args[0], "cd") == 0){
		if( change_working_dir(state, args, argc) == -1)
			return -1;
	}else if(strcmp(args[0], "umask") == 0){
		if(argc == 1){
			printf("%04o\n", state->current_umask);
		}else if(argc == 2){
			mode_t um;
			sem_wait(&errno_sem);
			errno = 0;
			sscanf(args[1], "%04o", &um);
			if(errno){
				fprintf(stderr, "please pass ocatal value for umask!\n");
				return -1;
				sem_post(&errno_sem);
			}
			sem_post(&errno_sem);
			printf("set umask to %04o\n", um);
			umask(um);			// call allways succeed!
			state->current_umask = get_umask();
			printf("umask is now: %04o\n", state->current_umask);
		}else{
			printf("please use only umask to query umask or umask NEW_UMASk to set a mask!\n");
		}
	}else if(strcmp(args[0], "printenv") == 0){
		print_environment();
	}else if(strcmp(args[0], "setpath") == 0){
		if(argc == 1){
			printf("current PATH is %s\n", getenv("PATH"));
		}else if(argc == 2){
			setenv("PATH", args[1], 1);		// no error handling necessary, because all possible errors of setenv are handled (see man setenv)
		}else{
			fprintf(stderr, "Please enter new path!\n");
			return -1;
		}
	}else if(strcmp(args[0], "getprot") == 0){
		if(argc != 2){
			fprintf(stderr, "please enter amount of history entries\n");
			return -1;
		}else{
			// IDC about locking because worst case is that we miss a line in history
			int amount = atoi(args[1]);
			int numberHistoryEntries = amount;
			char** history = log_read(&numberHistoryEntries); 	//create the history table
			for( ; amount > 0 && numberHistoryEntries > 0; --numberHistoryEntries, --amount){
				printf("%d: %s", numberHistoryEntries, history[numberHistoryEntries-1]);
			}
			int i = 0;
			for (i = 0; i < numberHistoryEntries; i++){
				if (history[i] != NULL){
					free (history[i]);
				}
			}
			free(history);
		}
	}else {
		return 0;
	}
	return 1;
}

void* background_worker_waiter(void* arg){
	background_task_t* background_worker = (background_task_t*) arg;

	int status;
	waitpid(background_worker->pid, &status, 0);
	if(WIFEXITED(status)){
		if(WEXITSTATUS(status) != 0){
			printf("Job %d exited with error code %d\n", background_worker->pid, WEXITSTATUS(status));
		}else{
			printf("Job %d finished\n", background_worker->pid);
			log_command(background_worker->calling_state, background_worker->args, background_worker->argc);
		}
	}else{
		printf("Job %d died.\n", background_worker->pid);
	}

	free(background_worker->args);
	free(background_worker->line);
	free(arg);

	return NULL;
}

/**
 * main loop for the shell to function.
 * @param state the current serverstate global variable
 */
void shell_loop(serverstate_t* state)
{
	for(;;){
		shell_promt(state);
		char* line = shell_readline();
		if( line == NULL ){
			return;
		}
		int argument_count = -1;
		char** vector = shell_segement_line(line, &argument_count);

		if(argument_count == 0 || strcmp(vector[0], "") == 0)
			continue;

		/** determine if the process should be a background proccess */
		int is_background = 0;
		if( (strcmp(vector[argument_count -1], "&")) == 0 ) {
			is_background = 1;
			vector[argument_count - 1] = NULL;
			argument_count--;		//decrement argument_cound because we eliminate the trailing "&".

			/** if someone enters only a "&" for example */
			if(argument_count == 0 || strcmp(vector[0], "") == 0)
				continue;
		}

		/** determine if the process is an internal process and LOG IT IF SUCCESSFUL */
		int resultintern = shell_internals(state, vector, argument_count);
		if (resultintern == 1)
		{
			log_command(state, vector, argument_count);
			continue;
		}
		else if (resultintern == -1)
		{
			continue;
		}else if(resultintern == 2){
			free(line);
			free(vector);
			free(state);
			exit(0);
		}

		/** logic for handling external commands and maybe log them if successful */
		state->current_PID = fork();

		if(state->current_PID == -1) {	// -1: ON ERROR - no clone (child) exists.
			fprintf(stderr, "could not spwan child %s\n", strerror(errno));
			continue;
		}else if(state->current_PID == 0) {	// 0: if we are the clone (child) and overwrite ourselfes with the program the user wants to run
			execvp(vector[0], vector);
			fprintf(stderr, "something went terribly wrong %s\n", strerror(errno));
			exit(1);
		}else {	// some number: if we are the PARENT and this "some number" is our clones (child) PID.
			if(is_background){
				printf("started background job with pid %d\n", state->current_PID);
				background_task_t* background_worker = malloc(sizeof(background_task_t)); // free me in thread
				background_worker->calling_state = state;                                 // not free in thread
				background_worker->args = vector;                                         // free me in thread
				background_worker->argc = argument_count;
				background_worker->pid = state->current_PID;
				background_worker->line = line;                                           // free me in thread

				pthread_t threadID;
				int threadErrInfo = pthread_create(&threadID, NULL, background_worker_waiter, background_worker);
				if(threadErrInfo){
					fprintf(stderr, "Could not start WEITING-Thread! %s\n", strerror(threadErrInfo));
				}

				/** PID has to nulled here, because we want to terminate only foreground processes with CTRL+C */
				state->current_PID = 0;
			}else {
				int status;
				waitpid(state->current_PID, &status, 0);
				if(WIFEXITED(status)){
					if(WEXITSTATUS(status) != 0){
						printf("programm exited with error code %d\n", WEXITSTATUS(status));
					}else{
						log_command(state, vector, argument_count);
					}
				}else{
					printf("Job %d died.\n", state->current_PID);
				}

				free(line);
				free(vector);
				state->current_PID = 0;
			}
		}
	}
}

/**
 * SIGINT handler according to best practice.
 * @note there is no way to figure out the calling thread id - Siging can only kill all or none forground-processes
 */
static void sigint_handler()
{
	if(current_state != NULL){
		kill(current_state->current_PID, SIGKILL);
		printf("Process %d terminated\n",current_state->current_PID);
		current_state->current_PID = 0;
	}
}

/**
 * Initialization function for all the signal handlers. According to best practice.
 * @todo remame to attach_signals
 */
void shell_register_signals()
{
	/** SIGQUIT is handled like a SIGINT */
	struct sigaction sa_int;
	sa_int.sa_handler = sigint_handler;
	sigemptyset(&sa_int.sa_mask);
	sa_int.sa_flags = SA_RESTART;
	if(sigaction(SIGINT, &sa_int, NULL) == -1){
		fprintf(stderr, "could not attach to signal\n");
	}else{
		printf("sigint attached!\n");
	}
	if(sigaction(SIGQUIT, &sa_int, NULL) == -1){
		fprintf(stderr, "could not attach to signal\n");
	}else{
		printf("sigquit attached!\n");
	}
}

/**
 * Function that logs the command to a logfile.
 * @param state server state needed for the client IP.
 * @param args input a vector (list) of arguments
 * @param argc argument count as integer.
 * @todo rework with only one FD.
 * @return -1 on error, 0 on success
 */
int log_command(serverstate_t* state, char** args, int argc)
{
	char line_buffer[MAX_LINE_LENGHT];
	int log_fd;

	char msg_buffer[MAX_LINE_LENGHT];
	memset(msg_buffer, '\0', MAX_LINE_LENGHT);
	int i;
	for(i = 0; i < argc; ++i)
		strcat(msg_buffer, args[i]);

	size_t used_char = 0;
	strncpy(line_buffer, state->ip_address, MAX_LINE_LENGHT);
	used_char += strlen(state->ip_address);
	strncat(line_buffer, ": ", MAX_LINE_LENGHT - used_char);
	used_char += 2;
	strncat(line_buffer, msg_buffer,  MAX_LINE_LENGHT- used_char);
	used_char += strlen(msg_buffer);
	strncat(line_buffer, "\n", MAX_LINE_LENGHT - used_char);

	sem_wait(&logfile_sem);
  if((log_fd = open(LOGFILE, O_WRONLY|O_APPEND|O_CREAT, 0662)) == -1)
	{
		fprintf(stderr, "Logfile %s konnte nicht angelegt werden! %s\n", LOGFILE, strerror(errno));
  	sem_post(&logfile_sem);
		return -1;
	}
  if(write(log_fd, line_buffer, strlen(line_buffer)) == -1)
	{
		fprintf(stderr, "Kommando konnte nicht in das Logfile gschrieben werden!\n");
  	sem_post(&logfile_sem);
		return -1;
	}
	fsync(log_fd);
  sem_post(&logfile_sem);

	if(close(log_fd) == -1)
	{
		fprintf(stderr, "Fehler beim Schließen der Datei!\n");
		return -1;
	}
	return 0;
}

/**
 * Function that reads the logfile.
 * @param numberHistoryEntries int pointer
 * @return returns table of history or in error case a NULL pointer.
 */
char** log_read(int* numberHistoryEntries)
{
	ssize_t max_needed_slots = (*numberHistoryEntries > MAX_HISTORY_ENTRIES ? *numberHistoryEntries : MAX_HISTORY_ENTRIES);
	*numberHistoryEntries = -1;
	char** tableOfHistoryEntries = malloc(sizeof(char*) * max_needed_slots);
	FILE* historyFile = fopen(LOGFILE, "r+");
	if (historyFile == NULL) {
		fprintf(stderr, "Error while opening the history file! %s\n", strerror(errno));
		return NULL;
	}

	char line_buffer[MAX_LINE_LENGHT];

	/** routine to iterate through the list */
	do{
		*numberHistoryEntries += 1;		// dereference the pointer first and then increment on one. (+= wert links vom plus wird um den wert rechts vom ist erhöht.)

		if(fgets(line_buffer, MAX_LINE_LENGHT, historyFile) == NULL){	// error case empty file to return a NULL pointer.
			tableOfHistoryEntries[*numberHistoryEntries] = NULL;
		}else{
			tableOfHistoryEntries[*numberHistoryEntries] = strdup(line_buffer);  // move from stack (line_buffer of a MAX LINE LENGTH) to heap (uses only as much bytes as neccessary)
		}
		/** routine to check if the history table has reached its MAX_HISTORY_ENTRIES boundary. and delete the oldest entry!  */
		if((*numberHistoryEntries + 1) >= max_needed_slots){
			free(tableOfHistoryEntries[0]);
			tableOfHistoryEntries[0] = NULL;
			memmove(&tableOfHistoryEntries[0], &tableOfHistoryEntries[1], max_needed_slots - 1);		//move that stuff up
			*numberHistoryEntries = max_needed_slots- 1 ;
		}
	}while(tableOfHistoryEntries[*numberHistoryEntries] != NULL && strlen(tableOfHistoryEntries[*numberHistoryEntries]) > 0);
	return tableOfHistoryEntries;
}

