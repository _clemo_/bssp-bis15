/**
 * \file
 * \brief Shellserver - process-based, pre-threaded
 * \author Burak Domink (is151041), Szing Marcus (is151033), Pointner Thomas (is151023), Fabian Friesenecker (is141011), Jung Clemens (is151014)
 * Threaded Shellsevrer REWRITTEN
 *
 * \bug the programm leaks some memory according to valgrind  (65 bytes just by strting and typing exit and 75 bytes if exit was issued pver the network)
 */
#define _GNU_SOURCE
#define _XOPEN_SOURCE 500
#define _POSIX_C_SOURCE 200112L

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/utsname.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include <signal.h>
#include <semaphore.h>
#include <fcntl.h>
#include <pthread.h>

#define MAX_INCOMING_QUEUE		6
#define PORTNUMBER            4033
#define MAX_LINE_LENGHT       1024
#define MAX_ARGUMENT_COUNT    100
#define LOGFILE               "/tmp/serverlog"
#define MAX_HISTORY_ENTRIES   1000

/**
 * @brief Table of the current server state.
 * Exists for every client once. It holds all the needed filedescriptors, and the current working state of the shell.
 */
typedef struct serverstate_s{
	char*   current_work_dir;		//!< working dir of the process
	pid_t   current_PID;			//!< pid_t data type to store the PID from the fork process
	int     client_FD;
	struct{
		uid_t uid;
		uid_t euid;
		uid_t gid;
		uid_t egid;
	}user_info;
	char*   nodename;
	char*   ip_address;
	time_t  start_time;
	mode_t  current_umask;     //!< umask of the process
	char*   environ_Path;
	pthread_t  current_thread_id;
	FILE*  clientout;
	FILE*  clientin;
}serverstate_t;


/**
 * @brief: for general logging of background tasks. To make the waitpid more easy.
 */
typedef struct{
	serverstate_t* calling_state; //!< caller state needed for logging
	char** args;                  //!< arguments needed for logging -> pointer will be freed
	int argc;                     //!< argument coutner needed for logging
	pid_t pid;                    //!< pid to wait for
	char* line;                   //!< will be freed
}background_task_t;

int server_init(int portnum);
int client_disconnect(int fd);
int server_state_init(serverstate_t* state, int fd);
void shell_loop(serverstate_t* state);
void shell_promt(serverstate_t* state);
char* shell_readline();
char** shell_segement_line(char* line, int* out_count);
int shell_internals(serverstate_t* state, char** args, int argc);
mode_t get_umask();
void shell_register_signals();
int log_command(serverstate_t* state, char** args, int argc);
char** log_read(serverstate_t* state, int* n);
void* handle_clients(void* );

FILE* 			logfile_stream = NULL;		//< FILE* is buffered. (used instead of FD and manual buffered writer)
sem_t 			logfile_sem;
sem_t 			errno_sem;
pthread_mutex_t environ_mutex;				//< to lock our "faked" environment.

extern char **environ;

int main(){
	if(sem_init(&logfile_sem, 1, 1) != 0){		//init logfile semaphore
		fprintf(stderr, "could not create logfile semapthore\n");
		exit(0);
	}

	sem_wait(&logfile_sem);
	logfile_stream=  fopen(LOGFILE, "a+");
	sem_post(&logfile_sem);

	if(sem_init(&errno_sem, 1, 1) != 0){		//init errno semaphore
		fprintf(stderr, "could not create errno semapthore\n");
		exit(0);
	}

	environ_mutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;

	//EASY SIGNAL HANDLING - not using sigaction any more because it's kinda bullshitty.
	signal(SIGINT, SIG_IGN);
	signal(SIGQUIT, SIG_IGN);
	signal(SIGCHLD, SIG_IGN);

	serverstate_t* server_state = malloc(sizeof(serverstate_t));

	if(server_state_init(server_state, 1) == -1){
		fprintf(stderr, "could not init server state\n");
		free(server_state);
		exit(1);
	}

	pthread_t threadID;
	int threadErrInfo = pthread_create(&threadID, NULL, handle_clients, NULL);
	if(threadErrInfo){
		fprintf(stderr, "Could not start SHELL-SERVER-Thread! %s\n", strerror(threadErrInfo));
	}

	//this creates the local shell on the server.
	shell_loop(server_state);
	return 0;
}

void* handle_session(void* args){
	serverstate_t* server_state = (serverstate_t*) args;
	shell_loop(server_state);

	client_disconnect(server_state->client_FD);
	return NULL;
}

void* handle_clients(void* args){
	//get rid of compiler-warnings
	if( args != NULL ){
		fprintf(stderr, "you passed too much info - i wont deal with it!\n");
	}

	int server_fd = server_init(PORTNUMBER);
	if ( server_fd == -1 ) {
		exit(1);
	}

	struct sockaddr_in client_addr;
	socklen_t client_addr_len = sizeof(client_addr);

	for(;;){
		printf("waiting for clients on port %d\n", PORTNUMBER);
		int client_fd = accept(server_fd, (struct sockaddr*) &client_addr, &client_addr_len); //CLIENTS ANNEHMEN
		if ( client_fd == -1 ) {
			fprintf(stderr, "Could not accept %s\n", strerror(errno));
			close(server_fd);
			exit(2);	//dafuq: the whole fucking server gets exited, when one client goofes up the connection.
		}
		printf("Client has connected.\n");
			
		serverstate_t* server_state;		// free me in thread
		server_state = malloc(sizeof(serverstate_t));
		if( server_state_init(server_state, client_fd) == -1 ){
			client_disconnect(client_fd);
			exit(1);	//dafuq: exit the server if server_state_init fails badly. (uname or time cannot be read)
		}
		server_state->ip_address = strdup(inet_ntoa(client_addr.sin_addr));	

		int thread_error;
		thread_error = pthread_create(
				&server_state->current_thread_id,
				NULL,
				handle_session,
				server_state
		);
		
		if (thread_error) {
			fprintf(stderr, "Could not create thread %s\n", strerror(thread_error));
			client_disconnect(client_fd);
			close(server_fd);
			exit(3);
		} else {
			printf("spwaned worker id %ld\n", server_state->current_thread_id);
		}
	}
}

/**
 * init the main server sockets and stuff (to reuse socket)
 * @param portnum portnumber to use
 * @return -1 on ERROR, server filedescriptor as int.
 */
int server_init(int portnum) {
	struct sockaddr_in server_addr;
	int err;

	server_addr.sin_addr.s_addr = INADDR_ANY;
	server_addr.sin_port        = htons(portnum);
	server_addr.sin_family      = AF_INET;

	int server_fd = socket(PF_INET, SOCK_STREAM, 0);
	if ( server_fd == -1) {
		fprintf(stderr, "Could not open socket %s\n", strerror(errno));
		return -1;
	}

	socklen_t server_addr_len = sizeof(server_addr);
	err = bind(server_fd, (struct sockaddr*) &server_addr, server_addr_len);
	if( err == -1){
		fprintf(stderr, "Could not bind on socket %s\n", strerror(errno));
		return -1;
	}

	err = listen(server_fd, MAX_INCOMING_QUEUE);
	if( err == -1){
		fprintf(stderr, "Could not listen on socket %s\n", strerror(errno));
		return -1;
	}
	/** CODE TO REUSE SOCKET **/
	int yes = 1;
	if(setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int))){
		fprintf(stderr, "Could declare reusable socket %s\n", strerror(errno));
		return -1;
	}


	return server_fd;
}

/**
 * function to disconnect the client gracefully (or at least clean) - comfort function, because the client gets notified that the server has disconnected him. And there will be no ghost TCP sessions in the server.
 * @param fd the clients filedescriptor which should be closed (comes from the state)
 * @return -1 on Error, 0 on successful disconnect.
 */
int client_disconnect(int client_fd)
{
	int err = 0;
	if(shutdown(client_fd, SHUT_RDWR) == -1)
		err = 1;
	if(close(client_fd) == -1)
		err = 1;
	return (err == 0 ? 0 : -1);
}

/**
 * Init the server state struct on a new server to start.
 * @param state server state to be initialized.
 * @param fd the clients filedescriptor
 * @return -1 on ERROR, 0 on successful initialization.
 */
int server_state_init(serverstate_t* state, int fd)
{
	state->current_work_dir = get_current_dir_name();	// free me
	state->client_FD        = fd;

	struct utsname uname_buffer;
	if( uname(&uname_buffer) == -1) {
		fprintf(stderr, "could not collect user-date %s\n", strerror(errno));
		return -1;
	}
	state->nodename = strdup(uname_buffer.nodename);	// free me

	state->user_info.uid = getuid();
	state->user_info.euid = geteuid();
	state->user_info.gid = getgid();
	state->user_info.egid = getegid();

	sem_wait(&errno_sem);
	errno = 0;	//LETZTER ERROR CODE VON EINEM SYSTEMCALL ABGELEGT (-1)
	time(&state->start_time);
	if( errno != 0 ){
		fprintf(stderr, "could not collect time info %s\n", strerror(errno));
		sem_post(&errno_sem);
		return -1;
	}
	sem_post(&errno_sem);

	state->current_umask = get_umask();
	state->current_PID = 0;

	state->environ_Path = strdup(getenv("PATH"));	// free me
	state->clientout = stdout;
	state->clientin  = stdin;
	state->ip_address= NULL;

	return 0;
}

/**
 * function to form and print out the shell prompt to stdout.
 * @param state server state to form the prompt output. (from assignment)
 */
void shell_promt(serverstate_t* state)
{
	fprintf(state->clientout, "\e[36m%s\e[0m \e[33m%s\e[0m >> ", state->nodename, state->current_work_dir);
	fflush(state->clientout);
}

/**
 * reads one line and makes error handling, cleanups (terminates the string when backslash r appears.) 
 * @return returns a nice null-terminated string (or at leas the pointer to it on the heap), returns NULL on error.
 * @note free me
 */
char* shell_readline(serverstate_t* state)
{
	char line_buffer[MAX_LINE_LENGHT];
	memset(line_buffer, '\0', MAX_LINE_LENGHT);

	size_t i = 0;
	ssize_t bytes;
	char IAC_COMMAND = '\0';                               // telnet workarount
	do{
		bytes = read(state->client_FD, &line_buffer[i], 1);
		if(bytes == -1)
			return NULL;
		if(bytes == 0)
			break;
		
		if( (unsigned char) line_buffer[i] > 127){           // telnet workaround
			if(IAC_COMMAND == '\0'){
				IAC_COMMAND = line_buffer[i];
			}
			putc(line_buffer[i], state->clientout);
		}

		if(line_buffer[i] == '\n')
			line_buffer[i] = '\0';

		i += 1;
	}while(line_buffer[i-1] != '\0' && i < MAX_LINE_LENGHT);
	if (i >= MAX_LINE_LENGHT)
		line_buffer[i-1] = '\0';
	if(IAC_COMMAND != '\0')                                // telnet workaround
		fflush(state->clientout);
	return strdup(line_buffer);
}

/**
 * segment line into a nice vector of char pointers.
 * @param line the line to dissect
 * @param argument_count the int pointer in which the number of elements in the list is written to.
 * @return a list (vector) of entered commands
 * @note do not free strings -> they are created with strtok. but you need to free the whole vector
 */
char** shell_segement_line(char* line, int* argument_count)
{
	char** vector = malloc(sizeof(char*) * MAX_ARGUMENT_COUNT);
	int i = 0;
	vector[i] = strtok(line, " \t\r");
	while(vector[i] != NULL && i < MAX_ARGUMENT_COUNT){
		i += 1;
		vector[i] = strtok(NULL, " \t\r");
	}
	if(i >= MAX_ARGUMENT_COUNT)
		vector[MAX_ARGUMENT_COUNT - 1] = NULL;

	/** write the number of elements into out_count */
	*argument_count = i;

	return vector;
}
/**
 * function to print out the UID/EUID/GID/EGID. (from assignment)
 * @param state server state to read out needed UID/EUID/GID/EGID infos.
 */
void print_id(serverstate_t* state)
{
	struct passwd* tmp_user;
	struct group* tmp_group;

	fprintf(state->clientout, "ID:\n");
	fprintf(state->clientout, "\tuid: %d (%s)\n", state->user_info.uid,
		((tmp_user = getpwuid(state->user_info.uid)) == NULL ? "" : tmp_user->pw_name)
	);
	fprintf(state->clientout, "\teuid: %d (%s)\n", state->user_info.euid,
		((tmp_user = getpwuid(state->user_info.euid)) == NULL ? "" : tmp_user->pw_name)
	);
	fprintf(state->clientout, "\tgid: %d (%s)\n", state->user_info.gid,
		((tmp_group = getgrgid(state->user_info.gid)) == NULL ? "" : tmp_group->gr_name)
	);
	fprintf(state->clientout, "\tegid: %d (%s)\n", state->user_info.egid,
		((tmp_group = getgrgid(state->user_info.egid)) == NULL ? "" : tmp_group->gr_name)
	);
}

/**
 * function to print all the infos. (from assignment)
 * @param state server state to read out the current shell infos. Needed for PID and shell start time.
 */
void print_info(serverstate_t* state)
{
	fprintf(state->clientout, "INFO ZU SOCKEN-SCHELL\n");
	fprintf(state->clientout, "\tShell von: Burak Domink (is151041), Szing Marcus (is151033), Pointner Thomas (is151023)\e[1;30m, Jung Clemens (is151014), Fabian Friesenecker (is141011)\e[0m\n");
	fprintf(state->clientout, "\tPID: %d\n", getpid());
	fprintf(state->clientout, "\tShell läuft seit %s\n", ctime(&state->start_time));
}

/**
 * function to change the working dir.
 * @param state server state to be altered by the get_current_dir_name function.
 * @param args directory to change to
 * @param argc argument count to check for errors in syntax.
 * @return -1 on ERROR, 0 on successful set of the current work dir.
 */
int change_working_dir(serverstate_t* state, char** args, int argc)
{
	
	char* old_ptr = state->current_work_dir;
	if(argc != 2){
		fprintf(state->clientout, "Bitte cd mit dem neuen Pfad aufrufen\n");
		return -1;
	}

	pthread_mutex_lock(&environ_mutex);
	if(chdir(state->current_work_dir) != 0){ // should never ever happen!
		fprintf(state->clientout, "could not cange to current dir!\n");
	}
	if(chdir(args[1]) == 0){
		state->current_work_dir = get_current_dir_name();	// FREE ME!!!
		pthread_mutex_unlock(&environ_mutex);
		free(old_ptr);
		return 0;
	}else{
		pthread_mutex_unlock(&environ_mutex);
		fprintf(state->clientout, "CD did not work %s\n", strerror(errno));
		return -1;
	}
}

/**
 * function which returns the umask. needed in various scenarious (e.g. to print the umask, to write it to the server state, etc...)
 * @return the umask in mode_t type.
 */
mode_t get_umask()
{	
	pthread_mutex_lock(&environ_mutex);
	mode_t res = umask(0);
	umask(res);
	pthread_mutex_unlock(&environ_mutex);
	return res;
}

/**
 * function to print the environment
 */
void print_environment(serverstate_t* state)
{
	pthread_mutex_lock(&environ_mutex);
	setenv("PATH", state->environ_Path, 1);
	char** ptr = environ;
	fprintf(state->clientout, "ENVIRONMENT\n");
	while(*ptr != NULL){
		fprintf(state->clientout, "%s\n", *ptr);
		ptr += 1;
	}
	pthread_mutex_unlock(&environ_mutex);
}

/**
 * function to handle internal shell commands
 * @param state server state is needed for some comments and may be altered.
 * @param args the vector of stored arguments
 * @param argc count of arguments (because we need to check in some commands if they have the right AMOUNT of args)
 * @return -1 if error, 0 if no match on an internal comment, 1 if internal command AND successful, and 2 if we need to exit
 */
int shell_internals(serverstate_t* state, char** args, int argc)
{
	if(strcmp(args[0], "pwd") == 0){
		fprintf(state->clientout, "%s\n", state->current_work_dir);
	}else if(strcmp(args[0], "id") == 0){
		print_id(state);
	}else if(strcmp(args[0], "exit") == 0){
		client_disconnect(state->client_FD);
		return 2;
	}else if(strcmp(args[0], "info") == 0){
		print_info(state);
	}else if(strcmp(args[0], "cd") == 0){
		if( change_working_dir(state, args, argc) == -1)
			return -1;
	}else if(strcmp(args[0], "umask") == 0){
		if(argc == 1){
			fprintf(state->clientout, "%04o\n", state->current_umask);
		}else if(argc == 2){
			mode_t um;
			sem_wait(&errno_sem);
			errno = 0;
			sscanf(args[1], "%04o", &um);
			if(errno){
				fprintf(state->clientout, "please pass ocatal value for umask!\n");
				return -1;
				sem_post(&errno_sem);
			}
			sem_post(&errno_sem);
			fprintf(state->clientout, "set umask to %04o\n", um);

			pthread_mutex_lock(&environ_mutex);
			umask(um);			// call allways succeed!
			state->current_umask = get_umask();		
			pthread_mutex_unlock(&environ_mutex);

			fprintf(state->clientout, "umask is now: %04o\n", state->current_umask);
		}else{
			fprintf(state->clientout, "please use only umask to query umask or umask NEW_UMASk to set a mask!\n");
		}
	}else if(strcmp(args[0], "printenv") == 0){
		print_environment(state);
	}else if(strcmp(args[0], "setpath") == 0){
		if(argc == 1){
			fprintf(state->clientout, "current PATH is %s\n", state->environ_Path);
		}else if(argc == 2){
			state->environ_Path = strdup(args[1]);
		}else{
			fprintf(state->clientout, "Please enter new path!\n");
			return -1;
		}
	}else if(strcmp(args[0], "getprot") == 0){
		if(argc != 2){
			fprintf(state->clientout, "please enter amount of history entries\n");
			return -1;
		}else{
			// IDC about locking because worst case is that we miss a line in history
			int amount = atoi(args[1]);
			int numberHistoryEntries = amount;
			char** history = log_read(state, &numberHistoryEntries); 	//create the history table
			for( ; amount > 0 && numberHistoryEntries > 0; --numberHistoryEntries, --amount){
				fprintf(state->clientout, "%d: %s", numberHistoryEntries, history[numberHistoryEntries-1]);
			}
			int i = 0;
			for (i = 0; i < numberHistoryEntries; i++){
				if (history[i] != NULL){
					free (history[i]);
				}
			}
			free(history);
		}
	}else {
		return 0;
	}
	return 1;
}

void* background_worker_waiter(void* arg){
	background_task_t* background_worker = (background_task_t*) arg;

	int status;
	waitpid(background_worker->pid, &status, 0);
	if(WIFEXITED(status)){
		if(WEXITSTATUS(status) != 0){
			fprintf(background_worker->calling_state->clientout, "Job %d exited with error code %d\n", background_worker->pid, WEXITSTATUS(status));
		}else{
			fprintf(background_worker->calling_state->clientout, "Job %d finished\n", background_worker->pid);
			log_command(background_worker->calling_state, background_worker->args, background_worker->argc);
		}
	}else{
		fprintf(background_worker->calling_state->clientout, "Job %d died.\n", background_worker->pid);
	}

	free(background_worker->args);
	free(background_worker->line);
	free(arg);

	return NULL;
}

/**
 * main loop for the shell to function.
 * @param state the current serverstate global variable
 */
void shell_loop(serverstate_t* state)
{
	signal(SIGINT, SIG_IGN);
	signal(SIGQUIT, SIG_IGN);
	
	if( state->ip_address != NULL) {
		fprintf(state->clientout, "handle client %s\n", state->ip_address);
		state->clientout = fdopen(state->client_FD, "w");
		state->clientin  = fdopen(state->client_FD, "r");
	}

	for(;;){
		shell_promt(state);
		char* line = shell_readline(state);

		if( line == NULL ){
			return;
		}
		int argument_count = -1;
		char** vector = shell_segement_line(line, &argument_count);

		if(argument_count == 0 || strcmp(vector[0], "") == 0)
			continue;

		/** determine if the process should be a background proccess */
		int is_background = 0;
		if( (strcmp(vector[argument_count -1], "&")) == 0 ) {
			is_background = 1;
			vector[argument_count - 1] = NULL;
			argument_count--;		//decrement argument_cound because we eliminate the trailing "&".

			/** if someone enters only a "&" for example */
			if(argument_count == 0 || strcmp(vector[0], "") == 0)
				continue;
		}

		/** determine if the process is an internal process and LOG IT IF SUCCESSFUL */
		int resultintern = shell_internals(state, vector, argument_count);
		if (resultintern == 1)
		{
			log_command(state, vector, argument_count);
			continue;
		}
		else if (resultintern == -1)
		{
			continue;
		}else if(resultintern == 2){
			free(line);
			free(vector);
			free(state);
			return;
		}

		/** logic for handling external commands and log them if successful */
		
		pthread_mutex_lock(&environ_mutex);
	
		if( chdir(state->current_work_dir ) != 0) {
			pthread_mutex_unlock(&environ_mutex);
			fprintf(state->clientout, "could not prepare environment for new process!\n");
			continue;
		}
		umask(state->current_umask);
		setenv("PATH", state->environ_Path, 1);

		state->current_PID = fork();

		if ( state->current_PID == -1 ) {	// -1: ON ERROR - no clone (child) exists.
			pthread_mutex_unlock(&environ_mutex);
			fprintf(state->clientout, "could not spwan child %s\n", strerror(errno));
			continue;
		} else if ( state->current_PID == 0 ) {	// 0: if we are the clone (child) and overwrite ourselfes with the program the user wants to run
			/** stderr, stdin und stdout redirected to the client_fd e.g. THE NETWORK SOCKET */
			dup2(state->client_FD, 0);
			dup2(state->client_FD, 1);
			dup2(state->client_FD, 2);
	
			signal(SIGINT, SIG_DFL);
			signal(SIGQUIT, SIG_DFL);
		
			execvp(vector[0], vector);
			fprintf(state->clientout, "something went terribly wrong with '%s' %s\n", vector[0], strerror(errno));
			exit(1);
		} else {	// some number: if we are the PARENT and this "some number" is our clones (child) PID.
			pthread_mutex_unlock(&environ_mutex);
			if(is_background){
				fprintf(state->clientout, "started background job with pid %d\n", state->current_PID);
				background_task_t* background_worker = malloc(sizeof(background_task_t)); // free me in thread
				background_worker->calling_state = state;                                 // not free in thread
				background_worker->args = vector;                                         // free me in thread
				background_worker->argc = argument_count;
				background_worker->pid = state->current_PID;
				background_worker->line = line;                                           // free me in thread

				pthread_t threadID;
				int threadErrInfo = pthread_create(&threadID, NULL, background_worker_waiter, background_worker);
				if(threadErrInfo){
					fprintf(state->clientout, "Could not start WAITING-Thread! %s\n", strerror(threadErrInfo));
				}
				umask(state->current_umask);
				setenv("PATH", state->environ_Path, 1);

				/** stderr, stdin und stdout redirected to the client_fd e.g. THE NETWORK SOCKET */
				dup2(state->client_FD, 0);
				dup2(state->client_FD, 1);
				dup2(state->client_FD, 2);

				signal(SIGINT, SIG_DFL);
				signal(SIGQUIT, SIG_DFL);

				execvp(vector[0], vector);
				fprintf(state->clientout, "something went wrong with '%s' %s\n", vector[0], strerror(errno));
				exit(1);
				break;
			default: // PARENT
				if(is_background){
					fprintf(state->clientout, "started background job with pid %d\n", state->current_PID);
					background_task_t* background_worker = malloc(sizeof(background_task_t)); // free me in thread
					background_worker->calling_state = state;                                 // not free in thread
					background_worker->args = vector;                                         // free me in thread
					background_worker->argc = argument_count;
					background_worker->pid = state->current_PID;
					background_worker->line = line;                                           // free me in thread

					pthread_t threadID;
					int threadErrInfo = pthread_create(&threadID, NULL, background_worker_waiter, background_worker);
					if(threadErrInfo){
						fprintf(state->clientout, "Could not start WEITING-Thread! %s\n", strerror(threadErrInfo));
					}

					/** PID has to nulled here, because we want to terminate only foreground processes with CTRL+C */
					state->current_PID = 0;
				} else {
					int status;
					waitpid(state->current_PID, &status, 0);
					if(WIFEXITED(status)){
						if(WEXITSTATUS(status) != 0){
							fprintf(state->clientout, "programm exited with error code %d\n", WEXITSTATUS(status));
						}else{
							log_command(state, vector, argument_count);
						}
					}else{
						fprintf(state->clientout, "Job %d died.\n", state->current_PID);
					}

					free(line);
					free(vector);
					state->current_PID = 0;
				}
				break;
		}
	}
}

/**
 * Function that logs the command to a logfile.
 * @param state server state needed for the client IP.
 * @param args input a vector (list) of arguments
 * @param argc argument count as integer.
 * @todo rework with only one FD.
 * @return -1 on error, 0 on success
 */
int log_command(serverstate_t* state, char** args, int argc)
{
	char line_buffer[MAX_LINE_LENGHT];

	char msg_buffer[MAX_LINE_LENGHT];
	memset(msg_buffer, '\0', MAX_LINE_LENGHT);
	int i;
	for(i = 0; i < argc; ++i)
		strcat(msg_buffer, args[i]);

	size_t used_char = 0;

	char* myip; 
	if(state->ip_address == NULL){
		myip = "LOCAL MACHINE";
	}else{
		myip = state->ip_address;
	}
	strncpy(line_buffer, myip, MAX_LINE_LENGHT);
	used_char += strlen(myip);
	
	strncat(line_buffer, ": ", MAX_LINE_LENGHT - used_char);
	used_char += 2;
	strncat(line_buffer, msg_buffer,  MAX_LINE_LENGHT- used_char);
	used_char += strlen(msg_buffer);
	strncat(line_buffer, "\n", MAX_LINE_LENGHT - used_char);

	sem_wait(&logfile_sem);
  if(logfile_stream != NULL)
	{
		fprintf(logfile_stream, "%s", line_buffer);
		fflush(logfile_stream);
	}
  sem_post(&logfile_sem);

	return 0;
}

/**
 * @brief Function that reads the logfile.
 * Table of MAX_HISTORY_ENTRIES is allocated in all circumstances or MORE - if the User requested more!
 * @todo: Rename MAX_HISTORY_ENTRIES to MIN_HISTORY_ENTRIES
 * @param numberHistoryEntries int pointer
 * @return returns table of history or in error case a NULL pointer.
 */
char** log_read(serverstate_t* state, int* numberHistoryEntries)
{
	ssize_t max_needed_slots = (*numberHistoryEntries > MAX_HISTORY_ENTRIES ? *numberHistoryEntries : MAX_HISTORY_ENTRIES);
	*numberHistoryEntries = -1;
	char** tableOfHistoryEntries = malloc(sizeof(char*) * max_needed_slots);
	FILE* historyFile = fopen(LOGFILE, "r+");
	if (historyFile == NULL) {
		fprintf(state->clientout, "Error while opening the history file! %s\n", strerror(errno));
		return NULL;
	}

	char line_buffer[MAX_LINE_LENGHT];

	/** routine to iterate through the list */
	do{
		*numberHistoryEntries += 1;		// dereference the pointer first and then increment on one. (+= wert links vom plus wird um den wert rechts vom ist erhöht.)

		if(fgets(line_buffer, MAX_LINE_LENGHT, historyFile) == NULL){	// error case empty file to return a NULL pointer.
			tableOfHistoryEntries[*numberHistoryEntries] = NULL;
		}else{
			tableOfHistoryEntries[*numberHistoryEntries] = strdup(line_buffer);  // move from stack (line_buffer of a MAX LINE LENGTH) to heap (uses only as much bytes as neccessary)
		}
		/** routine to check if the history table has reached its MAX_HISTORY_ENTRIES boundary. and delete the oldest entry!  */
		if((*numberHistoryEntries + 1) >= max_needed_slots){
			free(tableOfHistoryEntries[0]);
			tableOfHistoryEntries[0] = NULL;
			memmove(&tableOfHistoryEntries[0], &tableOfHistoryEntries[1], max_needed_slots - 1);		//move that stuff up
			*numberHistoryEntries = max_needed_slots- 1 ;
		}
	}while(tableOfHistoryEntries[*numberHistoryEntries] != NULL && strlen(tableOfHistoryEntries[*numberHistoryEntries]) > 0);
	return tableOfHistoryEntries;
}
