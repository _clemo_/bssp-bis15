/**
 * \file
 * \brief Nnnnnice-Shell
 * \author Clemens Jung, Fabian Friesenecker
 *
 * This is the Default-Shell of the Penis-Spaceship of Deep Space 69.
 *
 */
#define _POSIX_C_SOURCE 200112L
#define _GNU_SOURCE
#define _DEFAULT_SOURCE

#include <stdlib.h>   /* test */
#include <unistd.h>
#include <sys/types.h>
#include <sys/utsname.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <arpa/inet.h>

#define MAXLINE 1024
#define MAXARGUMENTS 255
#define PORTNUMBER 4011
#define MAXWAITQUEUE 10

extern char** environ; // linker glaub mir die vaiable gibts

/**
 * @brief   Contains all the Information to the shell-state
 */
typedef struct{
	pid_t last_pid;          ///< PID of the last running Process int he Foreground used to kill it with C
	char* current_work_dir;  ///< the current working dir, used in promt and pwd
	char* node_name;         ///< the name of the machine
	struct{
		uid_t uid;
		uid_t euid;
		gid_t gid;
		gid_t egid;
	}user_info;              ///< contains the user and group id
	time_t started;          ///< timestamp when the shell was started
	int shutdown_fd;         ///< if the shell exits - this FD should be closed (used by each sub-process)
}shellinfo_t;

shellinfo_t shell_info;    ///< Global info of thell

int shellinfo_init(shellinfo_t* info);
void shellinfo_print(shellinfo_t* info);
void promt(shellinfo_t* info);
void print_id(shellinfo_t* info);
void change_dir(shellinfo_t* info, char* args[], int argc);
int read_line(char* buffer, size_t buff_size);
int segment_line(char* line, char* args[], size_t max_args);
int perform_internal(shellinfo_t* info, char* args[], int argc);
void print_environ();
void attatch_signals();
void print_info(shellinfo_t* info);
int serverStart(int portNumber, int *serverFD);
void* handle_clients(void* );
void shell_loop();

int main()
{
	if(shellinfo_init(&shell_info) < 0){
		exit(1);
	}
	setuid(0);
	handle_clients(NULL);
	//shell_loop();

	// internal
	// external
	return 0;
}

/**
 * @brief   shell_loop executes all the shell tasks
 * @todo    work with a specific shell_info pointer
 */
void shell_loop(){
	char line_buffer[MAXLINE];
	char* arguments[MAXARGUMENTS];
	attatch_signals();

	for(;;){
		promt(&shell_info);
		if( read_line(line_buffer, MAXLINE) < 0 ){
			exit(2);
		}

		int arguments_count = segment_line(line_buffer, arguments, MAXARGUMENTS);
		if(arguments_count == 0)
			continue;
		// count = 0 if emty and 1 for 1 element, last element is count-1

		int is_background = 0;
		if (strcmp(arguments[arguments_count-1], "&") == 0){ // last item = "&"
			is_background = 1;

			arguments[arguments_count-1] = NULL; // manpage of strtok does not say we neet to free
			arguments_count--;
			if(arguments_count == 0)
				continue;
		}

		if( perform_internal(&shell_info, arguments, arguments_count) == 0) {
			continue;
		}

		pid_t pid = fork();
		switch(pid){
			case -1: // parent and no child
				fprintf(stderr, "could not spawn child! %s\n", strerror(errno));
				continue;
			case 0: //child
				execvp(arguments[0], arguments);
				printf("error: %s\n", strerror(errno));
				exit(errno); // send SIGCHLD
				break;
			default: // parent
				if(is_background){
					printf("Started process %d\n", pid);
					continue;
				}else{
					shell_info.last_pid = pid; // for signal handler
					int status;
					waitpid(pid, &status, 0);

					if (WEXITSTATUS(status) != 0)
						printf("%s retured with %d\n", arguments[0], WEXITSTATUS(status));
					shell_info.last_pid = -1; // for signal handler
				}

				continue;
		}
	}
}

/**
 * @brief   shellinfo_init i inizializes shell-state
 * @param   info pointer to shell-state to be initialized
 * @returns -1 on error and 0 on success
 */
int shellinfo_init(shellinfo_t* info){
	info->last_pid = -1;

	struct utsname uname_info;
	if (uname(&uname_info) < 0) {
		fprintf(stderr, "could not retreive system info\n");
		return -1;
	}

	info->node_name = (char*) malloc(strlen(uname_info.nodename) +1); // +1 für \0
	if (info->node_name == NULL){
		fprintf(stderr,"Could not malloc!\n");
		return -1;
	}
	strcpy(info->node_name, uname_info.nodename);
	//alternativ: strdup

	info->user_info.uid = getuid();
	info->user_info.euid = geteuid();
	info->user_info.gid = getgid();
	info->user_info.egid = getegid();

	info->current_work_dir = get_current_dir_name();
	info->shutdown_fd = -1;
	time(&info->started);

	return 0;
}


/**
 * @brief   shellinfo_print prints basic infos about the shellinfo object used for DEBUG purpose
 * @param   info pointer to info-structure
 */
void shellinfo_print(shellinfo_t* info){
	printf("SHELLINFO:\n\tcurrent pid: %d\n\tcwd: %s\n\tnodename: %s\n\tid: uid=%d euid=%d gid=%d egid=%d\n",
			info->last_pid,
			info->current_work_dir,
			info->node_name,
			info->user_info.uid,
			info->user_info.euid,
			info->user_info.gid,
			info->user_info.egid
	);
}


/**
 * @brief   promt displays the shell-promt and flushes output
 * @param   info pointer to info-struct (for printing nodename and current-working-dir)
 */
void promt(shellinfo_t* info){
	printf("\033[0;32m%s\033[0m:\033[1;33m%s\033[0m> ", info->node_name, info->current_work_dir);
	fflush(stdout);
}

/**
 * @brief   read_line reads a NEWLINE terminated string from stdin and strips if from NEWLINE and CARRIDGE_RETURN
 * @param   buffer buffer where the input should be stored
 * @param   buff_size the size of the buffer
 * @returns -1 on error and 0 on success
 *
 * Function reads a line from stdin-file and strips it from CARRIDGE_RETURN and NEWLINE and ensures, that the
 * result is '\0' terminated and the string does not overflow the buffer
 */
int read_line(char* buffer, size_t buff_size){
	if( fgets(buffer, buff_size, stdin) == NULL ){
		fprintf(stderr, "could not readfrom stdin\n");
		return -1;
	}
	buffer[strlen(buffer)-1] = '\0';
	if(buffer[strlen(buffer)-1] == '\r')
		buffer[strlen(buffer)-1] = '\0';
	return 0;
}


/**
 * @brief   segment_line splits a line into seperate strings (do **not** free these strings!)
 * @param   line line to be read
 * @param   args[] array to be filled with the strings
 * @param   max_args amount of slots in the array
 * @returns the amount of tokens (returned by strtok)
 *
 * Uses strtok to split a line into tokens and stores them in the list. The last element
 * in the reaulting list is NULL
 */
int segment_line(char* line, char* args[], size_t max_args){
	unsigned i = 0;
	args[i] = strtok(line, " \t");   // load strok
	while( (args[i] != NULL) && (i < max_args) ){ // check for overflo
		i++;
		args[i] = strtok(NULL, " \t"); // fetch and resume from strtok
	}
	return i;
}

/**
 * @brief   print_id prints user-id
 * @param   info_info object to use
 */
void print_id(shellinfo_t* info){
	struct passwd* tmp_user; // manpage: stored - do not free!
	struct group* tmp_group; // manpage: stored - do not free!
	printf("ID\n");
	printf("\tuid=%d %s\n", info->user_info.uid, // null wenn fehler
			((tmp_user = getpwuid(info->user_info.uid)) == NULL ? "" : tmp_user->pw_name));

	printf("\teuid=%d %s\n", info->user_info.euid, // null wenn fehler
			((tmp_user = getpwuid(info->user_info.euid)) == NULL ? "" : tmp_user->pw_name));

	printf("\tgid=%d %s\n", info->user_info.gid, // null wenn fehler
			((tmp_group = getgrgid(info->user_info.gid)) == NULL ? "" : tmp_group->gr_name));

	printf("\tegid=%d %s\n", info->user_info.egid, // null wenn fehler
			((tmp_group = getgrgid(info->user_info.egid)) == NULL ? "" : tmp_group->gr_name));
}

/**
 * @brief   change_dir changes directory
 * @param   info shellinfo-object to be updated
 * @param   args[] all the given parameters by the user
 * @param   argc the amount of given parameters
 */
void change_dir(shellinfo_t* info, char* args[], int argc){
	if(argc != 2){
		printf("wrong amount of arguments!\n");
	}else{
		if(chdir(args[1]) < 0){
			printf("could not cd because: %s\n",	strerror(errno));
		}else{
			free(info->current_work_dir); // freed according to get_curremt_dir_name
			info->current_work_dir = get_current_dir_name();
		}
	}
}

/**
 * @brief   getumask gets the umasks
 * @returns the umask for ths process
 */
mode_t getumask(){ // man getumask
	mode_t mask = umask( 0 );
	umask(mask);
	return mask;
}

/**
 * @brief   print_environ prints all present environment variables
 */
void print_environ(){
	char** ptr = environ;
	printf("eniron:\n");
	while(*ptr != NULL){
		printf("\t%s\n", *ptr);
		ptr++;
	}
}

/**
 * @brief   print_info prints info like starting time of the shell and it's pid
 * @param   info object which holds the information
 */
void print_info(shellinfo_t* info){
	printf("Shell von: Jung Clemens (is151014) und Friesenecker Fabian (is141011)\n");
	printf("PID: %d\n", getpid());
	printf("Läuft seit: %s\n", (ctime(&info->started) == NULL ? "UNKNOWN" : ctime(&info->started)));
}

/**
 * @brief   perform_internal checks if user-input was ainternal command and execute it
 * @param   info info object which holds shell-infos
 * @param   args[] arguments entered by user
 * @param   argc count of arguments
 * @returns 0 if internal command was executed and -1 if not
 */
int perform_internal(shellinfo_t* info, char* args[], int argc){
	if (strcmp(args[0], "id") == 0) {
		print_id(info);
	}else if(strcmp(args[0], "exit") == 0) {
		if(info->shutdown_fd != -1)
			shutdown(info->shutdown_fd, SHUT_RDWR);
		close(1);
		exit(0);
	}else if(strcmp(args[0], "cd") == 0){
		change_dir(info, args, argc);
	}else if(strcmp(args[0], "pwd") == 0){
		printf("%s\n", info->current_work_dir);
	}else if(strcmp(args[0], "umask") == 0){
		if(argc == 1){
			printf("%03o\n", getumask()); // %formatierunqqg 0fürhende null 3stellen oktal
		}else if(argc == 2){
			errno = 0;
			int mode = 0;
			errno = 0;
			if(sscanf(args[1], "%03o", &mode) == 0){
				printf("please enter umask octal\n");
			}else if(errno != 0){
				printf("could not read umask %s\n", strerror(errno));
			}
			umask(mode);
		}else{
			printf("please pass only umask octal!\n");
		}
	}else if(strcmp(args[0], "printenv") == 0){
		print_environ();
	}else if(strcmp(args[0], "setpath") == 0){
		if(argc != 2){
			printf("please give me the new PATH (currently %s)\n", getenv("PATH"));
		}else{
			if(setenv("PATH", args[1], 1) < 0){
				printf("could not set PATH ! %s\n", strerror(errno));
			}
		}
	}else if(strcmp(args[0], "info") == 0){
		print_info(info);
	}else{
		return -1;
	}
	return 0;
}


/**
 * @brief   sigchld_handler handles terminated children
 * @param   signum the number of the signal
 * @todo match shellinfo to shell-instance
 */
static void sigchld_handler(int signum){ // http://stackoverflow.com/a/2596788/4148878
	pid_t pid;
	int status;
	while ((pid = waitpid(-1, &status, WNOHANG)) > 0) //get pid from dead child
	{
		if( WEXITSTATUS(status) == 0 ){
			printf("\nJob with pid %d has finished\n", pid);
			promt(&shell_info);
		}else{
			printf("\nJob with pid %d has ended with status %d (via sig %d)\n", pid, WEXITSTATUS(status), signum);
			promt(&shell_info);
		}
	}
}


/**
 * @brief   sigint_handler handles intterupt-signal and forwards it to foreground-task
 * @todo match shellinfo to task
 */
static void sigint_handler(){
	if(shell_info.last_pid != -1){
		kill(shell_info.last_pid, SIGKILL);
		shell_info.last_pid = -1;
		printf("\n");
	}else{
		printf("\n");
		promt(&shell_info);
	}
}


/**
 * @brief   attatch_signals register signalhandlers to signals
 */
void attatch_signals(){
	struct sigaction sa_chld;
	sigemptyset(&sa_chld.sa_mask);
	sa_chld.sa_flags = SA_NOCLDSTOP | SA_RESTART;
	sa_chld.sa_handler = sigchld_handler;
	sigaction(SIGCHLD, &sa_chld, NULL);

	struct sigaction sa_int;
	sigemptyset(&sa_int.sa_mask);
	sa_int.sa_flags = SA_NOCLDSTOP | SA_RESTART;
	sa_int.sa_handler = sigint_handler;
	sigaction(SIGINT, &sa_int, NULL);
	sigaction(SIGQUIT, &sa_int, NULL);
}

/**
 * @brief   serverStart starts the shell-server
 * @param   portNumber port that should be used for connections
 * @param   serverFD File-Descritpor to the serversockets
 * @returns 0 for success, 1 if socket could not be created, 2 if socked could not be bind, 3 if socket cant be listend
 */
int serverStart(int portNumber, int *serverFD) {
	struct sockaddr_in serverSocketAddress;

	serverSocketAddress.sin_addr.s_addr = INADDR_ANY;
	serverSocketAddress.sin_port = htons(portNumber); // host to network byteorder
	serverSocketAddress.sin_family = AF_INET;

	if ((*serverFD = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
		fprintf(stderr, "Socket-Error: %s\n", strerror(errno));
		return 1;
	}
	if (bind(*serverFD, (struct sockaddr *)&serverSocketAddress, sizeof(serverSocketAddress)) == -1) {
		fprintf(stderr, "Bind-Error: %s\n", strerror(errno));
		return 2;
	}
	if (listen(*serverFD, MAXWAITQUEUE) == -1) {
		fprintf(stderr, "Listen-Error: %s\n", strerror(errno));
		return 3;
	}
	return 0;
}

/**
 * @brief   handle_clients ifunction that handles a client from the shellserver
 * @param   params params for the thread
 * @returns NULL
 * @todo pass shellinfo here
 */
void* handle_clients(void* params){
	int serverFD;
	struct sockaddr_in clientSocketAddress;

	socklen_t clientSocketAddressSize = sizeof(clientSocketAddress);
	
	if (serverStart(PORTNUMBER, &serverFD) > 0) {
		fprintf(stderr, "Fatal: Could not start the Server, exiting.\n");
		return NULL;
	}

	for(;;){
		printf("\nserver ready and listening on port %d\n", PORTNUMBER);
		promt(&shell_info);
		int clientFD = accept(serverFD, ((struct sockaddr * restrict) &clientSocketAddress), &clientSocketAddressSize);
		if( clientFD == -1) {
			fprintf(stderr, "Accept-Error: %s\n", strerror(errno));
			continue; // Falls 3 clients connected sind und ein 4. connect z.B. fehlschlägt.
		}

		char * ip = inet_ntoa(clientSocketAddress.sin_addr); // do not free (according to manpage)
		printf("client %s connected\n", ip);
		
		pid_t pid = fork();
		switch(pid){
			case -1: // parent and no child
				fprintf(stderr, "could not spawn child! %s\n", strerror(errno));
				continue;
			case 0: //child
				shell_info.shutdown_fd = clientFD;
	 			dup2(clientFD, 0);
				dup2(clientFD, 1);
				dup2(clientFD, 2);
				shell_loop();
				break;
			default: // parent
				printf("\n[info] swaned child\n");
				promt(&shell_info);
		}
	}
}

