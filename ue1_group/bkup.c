// define POSIX standards
#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE

// include some libraries
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/types.h>
#include <unistd.h>

// define constants
#define BUFFERSIZE 1024
#define MAXPATHNAME 1024

// prototypes of used functions - except the process_dir which is not
// implemented
void BackupNode(const char *srcName, const struct stat *targetInode,
                int targetfd);
int process_dir(const char *work, const struct stat *target_inode, int tfd);
int write_inode_and_name(const struct stat *srcInode, int tfd,
                         const char *srcFName);
int write_content(const char *srcFName, int tfd);

int main(int argc, char **argv) {
  // Archivnamen aus Environmentvariable "BACKUPTARGET" holen
  char *target = getenv("BACKUPTARGET");
  int targetfd;             // fd = file descriptor
  struct stat target_inode; // define the struct for our inode contents

  // Handle error on empty environment variable
  if (!target) {
    fprintf(stderr, "BACKUPTARGET Environment missing\n");
    exit(2);
  }

  // Create the Archive (creat)
  targetfd = creat(target, 0660); // creat returns a file descriptor
  if (targetfd == -1) {
    fprintf(stderr, "Can't create archiv file\n");
    exit(3);
  }

  // Read the targets inode (to check)
  if (fstat(targetfd, &target_inode) ==
      -1) { // fstat is like stat, but it works with file handles.
    fprintf(stderr, "Cannot stat Backup Target %s\n", target);
    exit(4);
  }

  // Parametercheck
  if (argc == 1) {
    BackupNode(".", &target_inode, targetfd); // call the actual backup

    // else branch, if there are more than one file to add to the archive
  } else {
    int i;
    for (i = 1; i < argc; i++) {
      BackupNode(argv[i], &target_inode, targetfd); // call the actual backup
    }
  }
  close(targetfd); // close the fd

  return 0; // end the program
}

// routine for backing up ONE FILE
void BackupNode(const char *srcName, const struct stat *targetInode,
                int targetfd) {
  struct stat srcInode; // define the struct for our inode contents

  // routine to read the inode contents into our stat - if fails
  if (lstat(srcName, &srcInode) == -1) { // lstat is like stat, but it can
                                         // handle symbolic links as well and
                                         // does not follow the symlink.
    fprintf(stderr, "Cannot stat the Backup Source %s\n", srcName);
    return;
  }

  // routine to check, wether we are archiving ourselfes. (so ignore the
  // archivefile)
  if (srcInode.st_dev == targetInode->st_dev && // no matter what links or so or
                                                // whereever it goes - it
                                                // compares inode numbr AND the
                                                // device
      srcInode.st_ino == targetInode->st_ino) {
    printf("Do not backup our own Archive-File!\n");
    return;
  }

  // HARDCORE-CHECKS
  // S_ISLNK check to get an own error message for symlinks (not really
  // necessary)
  if (S_ISLNK(srcInode.st_mode)) {
    printf("%s: Symbolic links currently not supported!\n", srcName);

  } else if (S_ISDIR(srcInode.st_mode)) {
    printf("Processing directory %s\n", srcName);
    process_dir(srcName, targetInode, targetfd);

  } else if (S_ISREG(srcInode.st_mode)) {
    printf("Archiving regular file %s\n", srcName);
    if (write_inode_and_name(&srcInode, targetfd, srcName) == -1) {
      fprintf(stderr, "%s: Archiving error: Cannot write inode and name!\n",
              srcName);
      return -1;
    } else if (write_content(srcName, targetfd) == -1) {
      fprintf(stderr, "%s: Archiving error: Cannot write content!\n", srcName);
      return -1;
    }
  } else {
    printf("%s: INFO: Only Files and Directories are currently supported!\n",
           srcName);
    return -1;
  }

  return 0;
}

// Function to process a Directory - WORK in PROGRESS as of 24.10.2016 (UE)

// Input Parameters: Working Dir, Target inode vom Archivefile,
// Target-Filedescriptor
int process_dir(const char *work, const struct stat *target_inode, int tfd) {
  DIR *dir; // DIR Datentyp für Verzeichnisse. weil opendir gibt ein DIR zurück.
  struct dirent
      *dentry; // zum speichern des direntry (das ich von readdir bekomme)
  char srcFName[MAXPATHNAME]; // Pfad-Variable
  struct stat srcInode;

  // Workingdirectory befüllen
  dir = opendir(work);
  if (dir == NULL) {
    fprintf(stderr, "Can't open dir\n");
    return -1;
  }

  while ((dentry = readdir(dir)) != NULL) {
    // if dentry->d_name unlike . and .. - then it has to be backuped.
    if (strcmp(dentry->d_name, ".") != 0 && strcmp(dentry->d_name, "..") != 0) {
      if ((strlen(srcFName) + strlen("/") + strlen(dentry->d_name)) >=
          MAXPATHNAME) {
        fprintf(stderr, "Directory name too long. Startdir: %s, Subdir: %s\n",
                work, dentry->d_name);
        return -1;
      } else {
        strcpy(srcFName, work);
        strcat(srcFName, "/");
        strcat(srcFName, dentry->d_name);

        if (lstat(srcFName, &srcInode) == -1) {
          fprintf(stderr, "Cannot Stat fuck it %s\n", work);
          return -1;
        }

        if (S_ISDIR(srcInode.st_mode)) {
          write_inode_and_name(&srcInode, tfd, srcFName);
          process_dir(srcFName, target_inode, tfd);
        } else {
          BackupNode(srcFName, target_inode, tfd);
        }
      }
    }
  }
  return 0;
}

// Function to dump the INODE contents and the filename into our archive file.
int write_inode_and_name(const struct stat *srcInode, int tfd,
                         const char *srcFName) {
  // writes inode
  if (write(tfd, srcInode, sizeof(struct stat)) != sizeof(struct stat)) {
    return -1;
  }
  // writes filename
  if (write(tfd, srcFName, strlen(srcFName) + 1) != strlen(srcFName) + 1) {
    return -1;
  }

  return 0;
}

// Function to dump the actual file contents into our archive file.
int write_content(const char *srcFName, int tfd) {

  int fd;
  char buf[BUFFERSIZE]; // create buffer with BUFFERSIZE
  ssize_t rdbytes; // because read works with singed-size-t - which is a singed
                   // datatype (because can be -1 in error) and is optimized to
                   // sit the targets computers memory. --> best practice to
                   // just make it ssize_t, because then we dont need to cast
                   // later.

  fd = open(srcFName, O_RDONLY); // open the file readonly and
                                 // return the filedescriptor to fd
  if (fd == -1) {
    fprintf(stderr, "%s: Cannot read source to write content to the archive!\n",
            srcFName);
    return -1;
  }

  // routine to buffered read from df to our buffer and save in RDBYTES the
  // amount of bytes read.UNTIL no bytes can be read (because read returns 0
  // then)
  while ((rdbytes = read(fd, buf, BUFFERSIZE)) > 0) {
    // Buffered write to the targetfiledescriptor (archivefile) and if it does
    // not write the amount of bytes we requested, something went terribly
    // wronk.
    if (write(tfd, buf, rdbytes) != rdbytes) {
      fprintf(stderr, "%s: Write to archivefile failed terribly!\n", srcFName);
      return -1;
    }
  }
  close(fd); // close the filedescritor when all goes well

  return 0;
}
