// define POSIX standards
#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE

// include some libraries
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

// define constants
#define BUFFERSIZE 1024

// prototypes of used functions
int read_block(int fd);

int main(int argc, const char *argv[]) {
  // check on the inputs
  if (argc == 1) {
    fprintf(stderr, "No Arguments, please give me some filnames!\n");
    return 0;
  }

  // variable to save the number of arguments
  int arg_num;

  // set umask
  umask(0);

  // go through all archives on the arguments and restore them.
  for (arg_num = 1; arg_num < argc; arg_num++) {

    int archive_fd = open(argv[arg_num], O_RDONLY); // open the actual archive
    // if archive_fd is empty, something went wrong open the archive.
    if (archive_fd < 0) {
      fprintf(stderr, "Could not open file %s\n", argv[arg_num]);
      exit(1);
    }

    // while-loop to read block after block. a block is one file in the
    // archive-file
    while (read_block(archive_fd) > 0) {
    }

    close(archive_fd); // close the filedescritor when all goes well
  }

  return 0;
}

// read block from filedescriptor and extract data - returns -1 for error, 0 for
// no block read, 1 successully read block
int read_block(int fd) {
  // additional safety-check
  if (fd < 0) {
    fprintf(stderr,
            "If this error message appears, something went terribly wrong!");
    return -1;
  }

  // ==== PHASE 1: READ THE FILEDESCRIPTOR ====

  struct stat target_struct; // define the struct for our inode contents
  // read exactly so long as the filedescriptor is (sizof of our target_struct)
  // ssize_t again here - singed size t
  ssize_t result = read(fd, &target_struct, sizeof(target_struct));

  if (result == 0) {
    return 0; // this is the EOF-Case when read returns 0.
  } else if (result < 0) {
    fprintf(stderr, "Could not read file from FD %d\n", fd);
    return -1; // this is the "something went terribly wrong" case.
  } else if (result != sizeof(target_struct)) {
    return -1; // this is the case when the file is incomplete (we do not get
               // the right amount of bytes like requested)
  }

  // Debug-Output - %z is for the size_t datatape and %d for decimal. shows the
  // size of the file read from the block.
  printf("[DEBUG] read %zd bytes\n", target_struct.st_size);

  // ==== PHASE 2: READ THE FILE NAME and create new file ====

  char filename[BUFFERSIZE]; // create buffer with BUFFERSIZE
  size_t used = 0;
  ssize_t read_bytes = 0;

  // routine to read from the FD into the "filename".
  do {
    read_bytes = read(fd, &filename[used], 1); // fill read_bytes
    used = used + 1;
  } while (read_bytes > 0 &&             // read_bytes has to be filled
           filename[used - 1] != '\0' && // READ until \0 appeares
           used < BUFFERSIZE // AND we still need to be inside the BUFFERSIZE.
           );

  // when we do not enter the while loop, handle the error (read_bytes < 0)
  if (read_bytes < 0) {
    fprintf(stderr, "Error while reading FD %d\n", fd);
    return -1;
  } else if (used >= BUFFERSIZE) {   // if whyever the filename gets too long
                                     // (because garbage character for example)
    filename[BUFFERSIZE - 1] = '\0'; // enforce \0 for error message
    fprintf(stderr, "Filename larger than buffer! -> skipping file %s \n",
            filename);
    return -1;
  }

  // IF it is a regular file, then create the file
  if (S_ISREG(target_struct.st_mode)) { // if regular file, do the MAGIC
    printf("[DEBUG] create file %s\n", filename);

    int new_file_fd = creat(filename, target_struct.st_mode); // create file
                                                              // and save file
                                                              // descriptor
    if (new_file_fd < 0) {
      fprintf(stderr, "Could not create file %s\n", filename);
      return -1;
    }

    // ==== PHASE 3: READ from the archive and WRITE INTO THE NEW FILE ====

    char read_buffer[BUFFERSIZE]; // create buffer with BUFFERSIZE
    size_t to_write = 0;
    size_t remaining_bytes = 0;
    size_t written_bytes = 0;

    // routine to buffered-read and write the content. We need at least one
    // iteration to start.
    do {
      remaining_bytes = target_struct.st_size - written_bytes;
      // when the archived file is empty - NOTHING WILL HAPPEN - this is when
      // target_struct.st_size returns 0 and we subtract 0 and then to_write is
      // 0.
      to_write = BUFFERSIZE < remaining_bytes ? BUFFERSIZE : remaining_bytes;
      printf("[DEBUG] just read: %zd (already read: %zd)\n", to_write,
             written_bytes);

      // try and read into the buffer the amount of bytes calculated above
      if (read(fd, read_buffer, to_write) != to_write) {
        fprintf(stderr, "Could not read enough bytes!\n");
        return -1; // less read than requested, return an error
      }

      // try and write the amount of bytes from the buffer to the new file
      if (write(new_file_fd, read_buffer, to_write) != to_write) {
        fprintf(stderr, "Could not write all bytes to file %s\n", filename);
        return -1;
      }

      written_bytes = written_bytes + to_write;
      // when successful increase the written_bytes

    } while (written_bytes < target_struct.st_size); // end the loop when the
                                                     // written_bytes is as big
                                                     // as st_size
    close(new_file_fd);
    printf("[DEBUG] file %s done\n", filename);
    return 1;

  } else if (S_ISDIR(target_struct.st_mode)) {
    if (mkdir(filename, target_struct.st_mode) < 0) {
      fprintf(stderr, "Could not create directory %s with mode: %o\n", filename,
              target_struct.st_mode);
    }
    printf("[DEBUG] create folder %s\n", filename);
  }

  else {
    fprintf(stderr, "Filetype not supported! (%s, %o)\n", filename,
            target_struct.st_mode);
    return 1;
  }

  return 1;
}
