/**
 * \file
 * \brief Chat-Server
 * \author Fabian Friesenecker
 *
 * Chatserver Programm UE3
 *
 */

#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

/**
  \def BUFFERSIZE
  Set the maximum buffer size used by various calls.
  \def MAXWAITQUEUE
  Set the maximum number of members in the waitqueue. Used in the "listen" call.
  \def PORTNUMBER
  Set the desired Port-Number the server should listen on.
  \def MAXUSERNAMELENGTH
  Set the maximum length of the Username. Longer usernames will be truncated!
*/
#define BUFFERSIZE 1024
#define MAXWAITQUEUE 10
#define PORTNUMBER 4011
#define MAXUSERNAMELENGTH 255

/**Entry of a client in the clientTable. One entry per client.
 * sockaddr_in is used, because this allows to read out the portnumber later.
 * the MUTEX of each clientEntry element is initialized when the client is added to the clientTable.
 */
typedef struct {
  int clientID;  							//!< client ID (at the moment this is the currentClientCount+1 at the time of the insertion to the list)
  int clientFD;
  pthread_mutex_t clientSendLock;			//!< MUTEX needed to synchronize the send threads. So that no more than 1 thread writes to a client filedescriptor at a time.
  struct sockaddr_in clientSocketAddress;
  socklen_t clientSocketAddressSize;		//!< to store the address size of the client (needed for accept-call)
  char UsernameString[MAXUSERNAMELENGTH];	//!< to sore the username of the client to have it at hand when needed.
} clientEntry_t;

/**
 * Table of the current server state. It holds the server filedescriptor, keeps track of the current client count and holds the table of connected clients.
 */
typedef struct {
  int serverFD;
  int currentClientCount;
  clientEntry_t **clientTable;				//!< list of pointers to clientEntry_t structs.
  pthread_mutex_t serverStateLock;			//! MUTEX needed to synchronize the access to the clientTable and the serverState.
} serverState_t;

int serverStart(int portNumber, int *serverFD);
int insertClientToServerStateList(clientEntry_t *clientToAdd);
void *clientHandler(void *);
int broadcastMessage(char *buffer);
int removeClient(clientEntry_t *clientToRemove);
int receiveUntilNullOrLinefeed(int targetFD, char *buffer, int bufferSize);

/**
 * Initially a server state struct has to be created. This is needed only once for the server. It is globally and server-wide available. Access is MUTEX'ed.
 */
serverState_t serverState;

int main() {
  serverState.serverStateLock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;		//looks strange (cast), but neccessary.

  serverState.serverFD = 0;
  serverState.clientTable = NULL;

  //! Error info of pthread_create, because it does not use errno.
  int threadErrInfo;

  //! threadID of the created thread.
  /*!
    A successful call to pthread_create() stores the ID of the new thread in the buffer pointed to by thread; this identifier is used to refer to the thread in subsequent calls to other pthreads functions.
  */
  pthread_t threadID;

  if (serverStart(PORTNUMBER, &serverState.serverFD) > 0) {
    fprintf(stderr, "Fatal: Could not start the Server, exiting.\n");
    exit(1);
  }
  fprintf(stdout, "Info: Server started successfully on Port %d.\n", PORTNUMBER);

  for (;;) {
    clientEntry_t *client = malloc(sizeof(clientEntry_t));
    client->clientSocketAddressSize = sizeof(client->clientSocketAddress);
    if ((client->clientFD = accept(serverState.serverFD,
                                   (struct sockaddr * restrict) & client->clientSocketAddress,
                                   &client->clientSocketAddressSize)) == -1) {
      fprintf(stderr, "Accept-Error: %s\n", strerror(errno));
      continue; // Falls 3 clients connected sind und ein 4. connect z.B. fehlschlägt.
    }
    client->clientID = (serverState.currentClientCount + 1);
    printf("SERVER-Info: Client Nr. %d connected!\n", client->clientID);

    // hier muss der akzeptierte client erstmal zum serverState hinzugefügt werden!
    if (insertClientToServerStateList(client) != 0) {
      // etwas ist beim malloc/realloc und der client-table schief gelaufen, also den accept
      // verwerfen und continue!
      fprintf(stderr, "Error: Cannot save client information in list!");
      continue;
    } else {
      // wenn alles gut gegangen ist wird der clientHandler gestartet.
      threadErrInfo = pthread_create(&threadID, NULL, clientHandler, (void *)client);
      if (threadErrInfo) {
        fprintf(stderr, "Error while creating the thread: %s\n", strerror(threadErrInfo));
      }
    }
  }
  return 0;
}

/**
 * @brief receiveUntilNullOrLinefeed utilizes a loop to read byte-by-byte from a filedescriptor and writes it \0-terminated into the buffer.
 * @param targetFD target filedescriptor from which the function should read.
 * @param buffer destination buffer to write the received bytes to.
 * @param bufferSize size of the buffer (for checks)
 * @return returns -1 on error, 0 on EOF and otherwise the number of total bytes read by the function.
 */
int receiveUntilNullOrLinefeed(int targetFD, char *buffer, int bufferSize) {

  int gesamtBytesGelesen = 0;
  char currentByte;

  // weil -1 ist fehler und 0 ist EOF z.B. oder connection terminiert.
  while ((recv(targetFD, &currentByte, 1, MSG_WAITALL) > 0) && (gesamtBytesGelesen < bufferSize)) {
    if (currentByte == '\0') {
      buffer[gesamtBytesGelesen] = '\0';
      return gesamtBytesGelesen;
    } else if (currentByte == '\n') {
      buffer[gesamtBytesGelesen] = '\0';
      return gesamtBytesGelesen + 1;
    } else {
      buffer[gesamtBytesGelesen] = currentByte;
      gesamtBytesGelesen++;
    }
  }
  return gesamtBytesGelesen;
}

// pthread_create will als struktur einen pointer mit void-pointer als argument.
// siehe aufbau von pthread_create: man page
void *clientHandler(void *arg) {

  int clientIDTemp;
  int receivedBytes;
  char clientReceiveBuffer[BUFFERSIZE];

  // weil ja pthread_create ein void pointer will, müssen wir hier das oben zu void pointer
  // gecastete struct wieder umcasten in clientEntry_t. Und gleich dereferenziert, damit man
  // zugreifen kann als würde es am stack liegen.
  clientEntry_t client = *((clientEntry_t *)arg);
  receivedBytes = receiveUntilNullOrLinefeed(client.clientFD, clientReceiveBuffer, BUFFERSIZE);
  printf("DBG: Receive-Function got %d Bytes from the Client!\n", receivedBytes);
  strncpy(client.UsernameString, clientReceiveBuffer, MAXUSERNAMELENGTH);
  printf("Storing connected Client Nr. %d Username: %s\n", client.clientID, client.UsernameString);

  for (;;) {
    receivedBytes = receiveUntilNullOrLinefeed(client.clientFD, clientReceiveBuffer, BUFFERSIZE);
    if (receivedBytes == -1) {
      fprintf(stderr, "Not Good: Error in Receive from Client!\n");
      // Fehlerfall. Raus aus der Loop. Client kicken.
      break;
    } else if (receivedBytes == 0) {
      printf("SERVER-Info: Client Nr. %d closed the connection! Invoking Cleanup...\n",
             client.clientID);
      // Fehlerfall. Raus aus der Loop. Client kicken.
      break;
    } else {
      printf("DBG: Receive-Function got %d Bytes from the Client!\n", receivedBytes);
      printf("Client %d wants this message to be relayed: %s\n", client.clientID,
             clientReceiveBuffer);

      // hier wird der finale String zusammengesetzt für den Client:
      time_t tempTime = time(NULL);
      char *currentTime = ctime(&tempTime);
      char *bullshit = ": "; // der Teil der noch angehängt werden soll hinten

      int totalMessageSendLength =
          MAXUSERNAMELENGTH + BUFFERSIZE + strlen(currentTime) + 2 * (strlen(bullshit));
      char broadcastSendBuffer[totalMessageSendLength];

      currentTime[strlen(currentTime) - 1] = '\0';

      memset(broadcastSendBuffer, '\0', totalMessageSendLength);

      strcat(broadcastSendBuffer, currentTime);
      strcat(broadcastSendBuffer, bullshit);
      strcat(broadcastSendBuffer, client.UsernameString);
      strcat(broadcastSendBuffer, bullshit);
      strcat(broadcastSendBuffer, clientReceiveBuffer);
      strcat(broadcastSendBuffer, "\n");

      if (broadcastMessage(broadcastSendBuffer) > 0) {
        fprintf(stderr, "Not Good: Could not broadcast message to clients!\n");
        // Fehlerfall. Raus aus der Loop. Client kicken.
        break;
      }
      printf("DBG: Message sent to the Client successfully: %s\n", broadcastSendBuffer);
    }
  }
  clientIDTemp = client.clientID;
  if ((removeClient(&client)) > 0) {
    fprintf(stderr, "Not Good: Could remove Client Nr. %d from Server State!\n", clientIDTemp);
  }
  printf("DEBUG-Info: Client Nr. %d removed from the Server State\n", clientIDTemp);

  return NULL;
}

// Broadcast benötigt nur die Info was gesendet werden soll an alle Clients. (Der Buffer)
// Die Info zu den Clients kommen aus der ServerState und dessen Liste.
int broadcastMessage(char *buffer) {
  int i;
  clientEntry_t *client;
  // Client enummerieren
  for (i = 0; i < serverState.currentClientCount; i++) {
    // zu beschickenden client auswählen
    client = serverState.clientTable[i];
    if (serverState.clientTable[i] == NULL) {
      // wenn an der ServerListe ein freier Platz gefunden wird, muss dieser übersprungen werden.
      continue;
    } else {
      // MUTEX locken
      pthread_mutex_lock(&client->clientSendLock);
      // buffer auf den clientFD vom aktuellen client schreiben.
      if (write(client->clientFD, buffer, strlen(buffer)) == -1) {
        fprintf(stderr, "Something went terribly wrong!\n");
        pthread_mutex_unlock(&client->clientSendLock);
        return 1;
      }
      fsync(client->clientFD);
      // MUTEX freigeben
      pthread_mutex_unlock(&client->clientSendLock);
    }
  }
  return 0;
}

// Initialisierungsfunktion
int serverStart(int portNumber, int *serverFD) {

  // interweb Socket
  struct sockaddr_in serverSocketAddress;

  serverSocketAddress.sin_addr.s_addr = INADDR_ANY;
  serverSocketAddress.sin_port = htons(portNumber); // host to network byteorder
  serverSocketAddress.sin_family = AF_INET;

  if ((*serverFD = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
    fprintf(stderr, "Socket-Error: %s\n", strerror(errno));
    return 1;
  }
  if (bind(*serverFD, (struct sockaddr *)&serverSocketAddress, sizeof(serverSocketAddress)) == -1) {
    fprintf(stderr, "Bind-Error: %s\n", strerror(errno));
    return 2;
  }
  if (listen(*serverFD, MAXWAITQUEUE) == -1) {
    fprintf(stderr, "Listen-Error: %s\n", strerror(errno));
    return 3;
  }
  return 0;
}

int insertClientToServerStateList(clientEntry_t *clientToAdd) {

  // TODO Nachschauen und iterieren ob was frei ist um realloc zu sparen.

  // wenn die clientTable noch leer (1. client), dann muss mal ein element malloced werden.
  if (serverState.clientTable == NULL) {
    if ((serverState.clientTable = malloc(sizeof(clientEntry_t *))) == NULL) {
      fprintf(stderr, "FATAL: Error while allocatig memory, exiting!");
      return 1;
    }
  } else {
    // realloc will eine absolute größe. also muss berechnet werden, wie groß die table aktuell ist.
    // nachdem wir den currentClientCount haben, muss dieser nur um eins inkrementiert werden und
    // einmal ein sizeof(clientEntry_t) dazugerechnet werden.
    if ((serverState.clientTable =
             realloc(serverState.clientTable,
                     ((serverState.currentClientCount + 1) * sizeof(clientEntry_t)))) == NULL) {
      fprintf(stderr, "FATAL: Error while reallocating memory, exiting!");
      return 2;
    }
  }
  // dann einfach hinten anhängen und den mutex initialisieren vom client. (das ist safe, weil eigentlich auf jeden Fall Platz sein muss!)
  pthread_mutex_lock(&serverState.serverStateLock);
  serverState.clientTable[serverState.currentClientCount] = clientToAdd;
  serverState.clientTable[serverState.currentClientCount]->clientSendLock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
  serverState.currentClientCount++;
  pthread_mutex_unlock(&serverState.serverStateLock);
  return 0;
}

int removeClient(clientEntry_t *clientToRemove) {

  int error = 0;
  pthread_mutex_lock(&serverState.serverStateLock);
  shutdown(clientToRemove->clientFD, SHUT_RDWR); // keine fehlerbehandlung: BECAUSE: WHYYYY?
  if ((close(clientToRemove->clientFD)) == -1) {
    fprintf(stderr, "Error: cannot close filedescriptor: %s\n", strerror(errno));
    error = 1; // nur intern ein logging, aber mit dem aufräumen muss weitergemacht werden!
  }
  printf("DBG: removed Client-ID in removeClient function: %d\n", clientToRemove->clientID);
  free(serverState.clientTable[clientToRemove->clientID - 1]); // direkter Tabellenzugriff, obwohl
                                                               // hier ein clientEntry_t Telement in
                                                               // die Funktion reingegeben wird,
                                                               // muss hier ein direkter Zugriff
                                                               // erfolgen und von daher ist das
                                                               // wieder Null-Indexiert und muss mit
                                                               // -1 gerechnet werden. Sonst wird
                                                               // ein falscher CLient abgeschossen.
  serverState.clientTable[clientToRemove->clientID - 1] = NULL;
  pthread_mutex_unlock(&serverState.serverStateLock);

  return error;
}
