/**
 * \file
 * \brief Chat-Client
 * \author Fabian Friesenecker
 *
 * Chatclient Programm
 *
 */

#include "rawio.c"
#include <arpa/inet.h>
#include <errno.h>
#include <errno.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define PORTNUMBER 4011
#define MAXMESSAGESIZE 1024
pthread_mutex_t histroy_lock = PTHREAD_MUTEX_INITIALIZER;

// 4.1 flush lein
// 3.9 verkettete liste
typedef struct node {
  char *msg;
  struct node *next;
} node_t;
node_t *history = NULL;

void histroy_add(char *msg) {
  node_t *element = malloc(sizeof(node_t));
  element->msg = strdup(msg);

  pthread_mutex_lock(&histroy_lock);
  element->next = history;
  history = element;
  pthread_mutex_unlock(&histroy_lock);
}

// 4. gui malen
void draw_gui() {
  int lin = get_lines();
  int col = get_cols();

  /* 0,0
   *
   *
   *lin-3
   *-------------------
   *lin-1 *******col'z
   lin                  */

  int i, j;
  for (i = 0; i < col; i++)
    writestr_raw("-", i, lin - 2);

  i = lin - 3;
  node_t *elem = history;
  while (elem != NULL && i > 0) {
    for (j = 0; j < col; j++)
      writestr_raw(" ", j, i);
    writestr_raw(elem->msg, 0, i);
    elem = elem->next;
    i--;
  }

  writestr_raw("", 0, lin - 1); // cursor zum ende
}

int send_mesage(int fd, char *message) {
  size_t len = strlen(message) + 1;
  if (write(fd, message, len) != len) // mit nall kärektär
    return -1;
  return 0;
}

int receiveUntilNullOrLinefeed(int targetFD, char *buffer, int bufferSize) {

  int gesamtBytesGelesen = 0;
  char currentByte;

  // weil -1 ist fehler und 0 ist EOF z.B. oder connection terminiert.
  while ((recv(targetFD, &currentByte, 1, MSG_WAITALL) > 0) && (gesamtBytesGelesen < bufferSize)) {
    if (currentByte == '\0') {
      buffer[gesamtBytesGelesen] = '\0';
      return gesamtBytesGelesen;
    } else if (currentByte == '\n') {
      buffer[gesamtBytesGelesen] = '\0';
      return gesamtBytesGelesen + 1;
    } else {
      buffer[gesamtBytesGelesen] = currentByte;
      gesamtBytesGelesen++;
    }
  }
  return gesamtBytesGelesen;
}

void *server_handler(void *parm) {
  int socket_fd = *((int *)parm);

  char line_buffer[MAXMESSAGESIZE];

  for (;;) {
    memset(line_buffer, '\0', MAXMESSAGESIZE);
    if (receiveUntilNullOrLinefeed(socket_fd, line_buffer, MAXMESSAGESIZE - 1) <= 0) {
      break;
    }
    histroy_add(line_buffer);
    draw_gui();
  }
  // TODO vll wenn ich zu viel zeit hab noch user bscheid sagen
  exit(0);
}

int main() {
  // 0. username abfragen
  char line_buffer[MAXMESSAGESIZE];
  printf("Enter username: "), fflush(stdout);
  if (fgets(line_buffer, MAXMESSAGESIZE, stdin) == NULL) {
    fprintf(stderr, "could not read input\n");
    exit(1);
  }
  if (line_buffer[strlen(line_buffer) - 1] == '\n')
    line_buffer[strlen(line_buffer) - 1] = '\0';

  char *name = strdup(line_buffer);

  // 1. verbindung aufbauen
  // 1.1 structs anlegen
  printf("connecting to server..."), fflush(stdout);
  struct sockaddr_in clientSocketAddress;
  in_addr_t ipaddress = inet_addr("127.0.0.1");
  clientSocketAddress.sin_addr.s_addr = ipaddress;
  clientSocketAddress.sin_port = htons(PORTNUMBER); // host to network byteorder
  clientSocketAddress.sin_family = AF_INET;
  // 1.2 socket

  int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (socket_fd == -1) {
    fprintf(stderr, "could not create socket %s\n", strerror(errno));
    exit(2);
  }

  // 1.3 connect
  if (connect(socket_fd, &clientSocketAddress, sizeof(clientSocketAddress)) == -1) {
    fprintf(stderr, "could not connect to server %s\n", strerror(errno));
    close(socket_fd);
    exit(3);
  }
  printf("done\n");
  // 2. username senden

  printf("zänd uzernäme..."), fflush(stdout);
  if (send_mesage(socket_fd, name) == -1) {
    if (shutdown(socket_fd, SHUT_RDWR) != -1) {
      close(socket_fd);
    }
    fprintf(stderr, "could not send username %s\n", strerror(errno));
  }
  printf("done\n");

  // 3. neuen fred anlegen
  pthread_t threadID;
  int err = pthread_create(&threadID, NULL, server_handler, (void *)&socket_fd);
  if (err) {
    fprintf(stderr, "could not launch thread %s\n", strerror(err));
    if (shutdown(socket_fd, SHUT_RDWR) != -1) {
      close(socket_fd);
    }
    exit(4);
  }
  clearscr();
  draw_gui();

  int i;
  int col = get_cols();
  int lin = get_lines();
  // 3a: receive fred
  // 3b: sende fred
  for (;;) {
    for (i = 0; i < col; i++)
      writestr_raw(" ", i, lin - 1);
    gets_raw(line_buffer, MAXMESSAGESIZE, 0, lin - 1);

    if (strcmp(line_buffer, "/quit") == 0)
      break;
    if (strlen(line_buffer) > 0 && send_mesage(socket_fd, line_buffer) == -1) {
      fprintf(stderr, "could not send message %s\n", strerror(errno));
      break;
    }
  }
  if (shutdown(socket_fd, SHUT_RDWR) != -1) {
    close(socket_fd);
  }
}
