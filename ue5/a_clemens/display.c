#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <errno.h>

#include "share.h"

int main()
{
	char line[MAX_MSG_LEN];

	struct mq_attr queue_attributes = {0, MAX_MSG_COUNT, MAX_MSG_LEN, 0, {0,0,0,0}}; // __pad ist letztes arg
	
	mqd_t queue;
	queue = mq_open(QUEUE_NAME, O_RDONLY, 0600, &queue_attributes);
	if(queue == -1){
		fprintf(stderr, "could not open %s %s\n", QUEUE_NAME, strerror(errno));
		return 1;
	}

	for(;;){
		unsigned int prio = -1;
		ssize_t rec = mq_receive(queue, line, MAX_MSG_LEN,  &prio);
		if( rec == -1 )	
			break;

		printf("%s\n", line);
		
		if(strcmp(line, "quit") == 0){
			break;
		}
	}
	mq_unlink(QUEUE_NAME);
	return 0;
}
