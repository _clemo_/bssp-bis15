#define _POSIX_C_SOURCE 200112L

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>
#include <semaphore.h>


#include "share.h"


int main()
{
	char line[MAX_MSG_LEN];
	int fd;
	struct shm_for_msg* shm;
	sem_t* sem_free_for_write_msg; 
	sem_t* sem_ready_to_read_msg; 
	sem_t* sem_ready_to_join; 
	int64_t version = 0;

	unlink(SHM_NAME);

	fd = shm_open(SHM_NAME , O_RDWR | O_CREAT, 0600);
	if(fd == -1){
		fprintf(stderr, "Could not open shared memory %s\n", strerror(errno));
		return 1;
	}

	if( ftruncate(fd, sizeof(shm_for_msg_t)) == -1){
		fprintf(stderr, "could not fill shared memory %s\n", strerror(errno));
		return 1;
	}

	shm = mmap(NULL, sizeof(shm_for_msg_t), PROT_WRITE, MAP_SHARED, fd, 0);
	if( shm == MAP_FAILED ){
		fprintf(stderr, "could not map memory %s\n", strerror(errno));
		close(fd);
		// unlink shm?
		return 1;
	}

	sem_unlink(SEM_FREE_FOR_WRITE_MSG);
	sem_unlink(SEM_READY_TO_READ_MSG);
	sem_unlink(SEM_READY_FOR_JOIN);

	// habe gelesen -> menu darf schreiben
	sem_free_for_write_msg = sem_open(SEM_FREE_FOR_WRITE_MSG, O_RDWR | O_CREAT, 0600, 1);

	// menu hat geschrieben -> display darf lesen
	sem_ready_to_read_msg = sem_open(SEM_READY_TO_READ_MSG, O_RDWR | O_CREAT, 0600, 0);
	
	sem_ready_to_join = sem_open(SEM_READY_FOR_JOIN, O_RDWR | O_CREAT, 0600, 0);
	shm->amount_displays = 0;
	sem_post(sem_ready_to_join);

	printf("Press enter if all clients are connected !\n");
	fgetc(stdin);
	sem_wait(sem_ready_to_join); // ensure no more clients connect

	//TODO errorhandling

	for(;;){
		size_t line_len;
		printf("> "), fflush(stdout);
	
		if(fgets(line, MAX_MSG_LEN, stdin) == NULL)
			break;

		line_len = strlen(line);
		if(line[line_len-1] == '\n'){
			line_len -= 1;	
			line[line_len] = '\0';
		}

		// send messge to queue
		/// wait for all displays to be ready
		int i;
		for(i = 0; i < shm->amount_displays; ++i){
			sem_wait(sem_free_for_write_msg);
			printf("%d/%d\n", i, shm->amount_displays);
		}
		memcpy(shm->message, line, MAX_MSG_LEN);
		errno = 0;
		shm->version = version++;
		if(errno != 0){
			perror("could not read current timestamp\n");
		}
		/// open for all displays
		for(i = 0; i < shm->amount_displays; ++i){
			sem_post(sem_ready_to_read_msg);
		}

		if(strcmp(line, "quit") == 0){
			break;
		}
	}

	sem_close(sem_free_for_write_msg);
	sem_close(sem_ready_to_read_msg);
	sem_close(sem_ready_to_join);

	munmap(shm, sizeof(shm_for_msg_t));
	close(fd);

	sem_unlink(SEM_FREE_FOR_WRITE_MSG);
	sem_unlink(SEM_READY_TO_READ_MSG);
	sem_unlink(SEM_READY_FOR_JOIN);

	return 0;
}
