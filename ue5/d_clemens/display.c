#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "share.h"

int main()
{	
	char line[MAX_MSG_LEN];
	FILE* reader = fopen(PIPE_NAME, "r");
	if(reader == NULL){
		perror("Could not open pipe for reading\n");
		return 1;
	}
	for(;;){
		if(fgets(line, MAX_MSG_LEN, reader) == NULL){
			perror("Could not read from pipe!\n");
			break;
		}
		printf("%s", line), fflush(stdout);
		if(strcmp(line, "QUIT") == 0)
			break;
	}
	unlink(PIPE_NAME);
	return 0;
}
