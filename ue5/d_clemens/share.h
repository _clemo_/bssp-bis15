#ifndef _SHARE_H
#define _SHARE_H

#define MAX_MSG_LEN 256
#define MAX_MSG_COUNT 10
#define QUEUE_NAME "/is151014_mq_q"
#define PIPE_NAME "is151014_uppercase"
#endif
