#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <mqueue.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>

#include "share.h"

int main()
{
	char line[MAX_MSG_LEN];

	struct mq_attr queue_attributes = {0, MAX_MSG_COUNT, MAX_MSG_LEN, 0, {0,0,0,0}}; // __pad ist letztes arg
	
	mqd_t queue;
	queue = mq_open(QUEUE_NAME, O_RDONLY, 0600, &queue_attributes);
	if(queue == -1){
		fprintf(stderr, "could not open %s %s\n", QUEUE_NAME, strerror(errno));
		return 1;
	}
	printf("queue opened\n");

	if(unlink(PIPE_NAME_UPPER) == 0){
		printf("deleted old pipe %s\n", PIPE_NAME_UPPER);
	}
	if(unlink(PIPE_NAME_LOWER) == 0){
		printf("deleted old pipe %s\n", PIPE_NAME_LOWER);
	}

	if(mkfifo(PIPE_NAME_UPPER, 0600) < 0 || mkfifo(PIPE_NAME_LOWER, 0600) < 0 ){
		perror("Could not create pipe!\n");
		return 1;
	}
	else{
		printf("pipes ready\n");
	}

	FILE* writer_upper = fopen(PIPE_NAME_UPPER, "w");
	FILE* writer_lower = fopen(PIPE_NAME_LOWER, "w");

	printf("files open\n");

	if(writer_upper == NULL || writer_lower == NULL){
		perror("Could not open pipes for writing\n");
		// leave pipe open
		// do not destroy mq
		return 1;
	}

	for(;;){
		unsigned int prio = -1;
		ssize_t rec = mq_receive(queue, line, MAX_MSG_LEN,  &prio);
		if( rec == -1 )	
			break;

		size_t line_len = strlen(line);
		unsigned i;
		for(i = 0; i < line_len; ++i){
			fprintf(writer_upper,"%c", toupper(line[i]));
			fprintf(writer_lower,"%c", tolower(line[i]));
		}
		fprintf(writer_lower, "\n"), fflush(writer_lower);
		fprintf(writer_upper, "\n"), fflush(writer_upper);
		printf("sent messages to pipes!\n");

		if(strcmp(line, "quit") == 0){
			break;
		}
	}
	// pipe is unlinked in display
	mq_unlink(QUEUE_NAME);
	return 0;
}
