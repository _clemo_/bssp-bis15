#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include "share.h"

int main(int argc, const char *argv[])
{
	if( argc != 2 ){
		printf("give me the number of the display (1 or 2)\n");
		return 1;
	}
	char* pipe_name = argv[1][0] == '1' ? PIPE_NAME_UPPER : PIPE_NAME_LOWER;
	printf("open pipe %s\n", pipe_name);

	char line[MAX_MSG_LEN];
	FILE* reader = fopen(pipe_name , "r");
	if(reader == NULL){
		perror("Could not open pipe for reading\n");
		return 1;
	}else{
		printf("opened pipe %s\n", pipe_name);
	}
	for(;;){
		if(fgets(line, MAX_MSG_LEN, reader) == NULL){
			if(errno != 0)
				perror("Could not read from pipe!\n");
			break;
		}
		printf("%s", line), fflush(stdout);
		if(strcmp(line, "QUIT") == 0 || strcmp(line, "quit") == 0)
			break;
	}
	unlink(pipe_name);
	return 0;
}
