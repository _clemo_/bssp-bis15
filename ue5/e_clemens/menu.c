#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <errno.h>
#include <sched.h>

#include "share.h"

int main()
{
	char line[MAX_MSG_LEN];

	struct mq_attr queue_attributes = {0, MAX_MSG_COUNT, MAX_MSG_LEN, 0, {0,0,0,0}}; // __pad is last arg
	
	mqd_t queue;
	queue = mq_open(QUEUE_NAME, O_CREAT|O_WRONLY, 0600, &queue_attributes);
	if(queue == -1){
		fprintf(stderr, "could not open %s %s\n", QUEUE_NAME, strerror(errno));
		return 1;
	}

	for(;;){
	size_t line_len;
		printf("> "), fflush(stdout);
	
		if(fgets(line, MAX_MSG_LEN, stdin) == NULL)
			break;

		line_len = strlen(line);
		if(line[line_len-1] == '\n'){
			line_len -= 1;	
			line[line_len] = '\0';
		}

		// send messge to queue 
		// TODO check msq queue len	
		int informed = 0;
		do{
			mq_getattr(queue, &queue_attributes);
			if(queue_attributes.mq_curmsgs >= MAX_MSG_COUNT){
				if(informed == 0){
					printf("MSGQ_QUEUE FULL waiting...\n");
					informed = 1;
				}
				sched_yield();
			}
		}while(queue_attributes.mq_curmsgs >= MAX_MSG_LEN);
		
		if( mq_send( queue, line, line_len+1, 0 /* prio */ ) == -1){
			fprintf(stderr, "could not send msg, %s\n", strerror(errno));
			break;
		}
		
		if(strcmp(line, "quit") == 0){
			break;
		}
	}
	return 0;
}
