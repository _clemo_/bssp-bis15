
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>

MODULE_LICENSE("Dual BSD/GPL");

static int hello_init(void)
{
	printk(KERN_INFO "cdrv: Hello, kernel world!\n");
	return 0;
}

static void hello_exit(void)
{
	printk(KERN_INFO "cdrv: Goodbye, kernel world!\n");
}

module_init(hello_init);
module_exit(hello_exit);

