#ifndef _SHARE_H
#define _SHARE_H

#define MAX_MSG_LEN 256

#define SHM_NAME "/is141011_sh1"
#define SEM_FREE_FOR_WRITE_MSG "/is141011_for_write"
#define SEM_READY_TO_READ_MSG "/is141011_for_read"

struct shm_for_msg{
	char message[MAX_MSG_LEN];
	int version;
}shm_for_msg_t;

#endif
