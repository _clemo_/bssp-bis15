/**
 * \file
 * \brief Messagequeue UE5
 * \author Fabian Friesenecker
 */
#include "share.h"
#include <string.h>
#include <stdio.h>

//for mq_open
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>

int main(int argc, char **argv) {
	char line [MAX_MSG_LEN];
	mqd_t queue;
	queue = mq_open(QUEUE_NAME, O_RDONLY);  	//open queue readonly

	if (queue == -1) {
		perror(QUEUE_NAME);
		return 1;
	}

	//TODO: check attributes with mq_getattr
	for(;;) {
		unsigned int prio = 7;					// int not null to check if it works
		ssize_t read_cnt = mq_receive(queue, line, MAX_MSG_LEN, &prio);
		if (read_cnt == -1) {
			perror(QUEUE_NAME);
			mq_close(queue);
		}
		if (!read_cnt) {
			continue;
		}
		line[read_cnt -1] = '\0';
		printf("prio: %u, content: %s\n", prio, line);

		if (!strcmp(line, "quit")) {
					break;
		}
	}
	mq_close(queue);
	mq_unlink(QUEUE_NAME);
	return 0;
}
