/**
 * \file
 * \brief Shared Lib for Messagequeue UE5
 * \author Fabian Friesenecker
 */
#ifndef SHARE_H_
#define SHARE_H_

#define MAX_MSG_LEN 256
#define MAX_MSG_COUNT 10
#define QUEUE_NAME "/is141011"

#endif
