#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <mqueue.h>
#include <errno.h>
#include <semaphore.h>

#include "share.h"

int main()
{
	char line[MAX_MSG_LEN];
	sem_t* sem_free_for_write_msg;
	sem_t* sem_ready_to_read_msg;

	int fd = shm_open(SHM_NAME , O_RDWR, 0600);
	if(fd == -1){
		fprintf(stderr, "Could not open shared memory %s\n", strerror(errno));
		return 1;
	}

	struct shm_for_msg* shm;
	shm = mmap(NULL, sizeof(shm_for_msg_t), PROT_WRITE, MAP_SHARED, fd, 0);
	if( shm == MAP_FAILED ){
		fprintf(stderr, "could not map memory %s\n", strerror(errno));
		close(fd);
		// unlink shm?
		return 1;
	}

	// habe gelesen -> menu darf schreiben
	sem_free_for_write_msg = sem_open(SEM_FREE_FOR_WRITE_MSG, O_RDWR);

	// menu hat geschrieben -> display darf lesen
	sem_ready_to_read_msg = sem_open(SEM_READY_TO_READ_MSG, O_RDWR);
	if(sem_ready_to_read_msg == SEM_FAILED || sem_free_for_write_msg == SEM_FAILED){
		fprintf(stderr, "could not open all required semaphors!%s\n", strerror(errno));
		return 1;
	}

	for(;;){
		sem_wait(sem_ready_to_read_msg);
		memcpy(line, shm->message, MAX_MSG_LEN);
		sem_post(sem_free_for_write_msg);

		line[MAX_MSG_LEN-1] = '\0';

		printf("%s\n", line);

		if(strcmp(line, "quit") == 0){
			break;
		}
	}
	munmap(shm, sizeof(shm_for_msg_t));
	close(fd);
	sem_unlink(SEM_FREE_FOR_WRITE_MSG);
	sem_unlink(SEM_READY_TO_READ_MSG);
	return 0;
}
