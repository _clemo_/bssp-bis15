#define _POSIX_C_SOURCE 200112L

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>
#include <semaphore.h>
#include <stdlib.h>

#include "share.h"

int main(int argc, char **argv) {
	char line[MAX_MSG_LEN];
	int fd;
	long int anzahl;
	struct shm_for_msg* shm;
	sem_t* sem_free_for_write_msg;
	sem_t* sem_ready_to_read_msg;

	anzahl = strtol(argv[1], NULL ,10);

	unlink(SHM_NAME);

	fd = shm_open(SHM_NAME , O_RDWR | O_CREAT, 0600);
	if(fd == -1){
		fprintf(stderr, "Could not open shared memory %s\n", strerror(errno));
		return 1;
	}

	if( ftruncate(fd, sizeof(shm_for_msg_t)) == -1){
		fprintf(stderr, "could not fill shared memory %s\n", strerror(errno));
		return 1;
	}

	shm = mmap(NULL, sizeof(shm_for_msg_t), PROT_WRITE, MAP_SHARED, fd, 0);
	if( shm == MAP_FAILED ){
		fprintf(stderr, "could not map memory %s\n", strerror(errno));
		close(fd);
		// unlink shm?
		return 1;
	}

	sem_unlink(SEM_FREE_FOR_WRITE_MSG);
	sem_unlink(SEM_READY_TO_READ_MSG);
	sem_unlink(SEM_READY_TO_RUMBLE);

	// habe gelesen -> menu darf schreiben
	sem_free_for_write_msg = sem_open(SEM_FREE_FOR_WRITE_MSG, O_RDWR | O_CREAT, 0600, 1);

	// menu hat geschrieben -> display darf lesen
	sem_ready_to_read_msg = sem_open(SEM_READY_TO_READ_MSG, O_RDWR | O_CREAT, 0600, 0);


	int i;
	for (i=1; i <= anzahl; i++) {
		sem_wait(sem_free_for_write_msg);
	}

	for(;;){
		size_t line_len;
		printf("> "), fflush(stdout);

		if(fgets(line, MAX_MSG_LEN, stdin) == NULL)
			break;

		line_len = strlen(line);
		if(line[line_len-1] == '\n'){
			line_len -= 1;
			line[line_len] = '\0';
		}

		memcpy(shm->message, line, MAX_MSG_LEN);

		for (i = 1; i <= anzahl; i++) {
			sem_post(sem_ready_to_read_msg);
		}

		if(strcmp(line, "quit") == 0){
			break;
		}
	}

	munmap(shm, sizeof(shm_for_msg_t));
	close(fd);
	return 0;
}
