/**
 * \file
 * \brief Messagequeue UE5
 * \author Fabian Friesenecker
 */
#include "share.h"
#include <string.h>
#include <stdio.h>

//for mq_open
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <ctype.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>


int main() {
	char line [MAX_MSG_LEN];
	mqd_t queue;
	queue = mq_open(QUEUE_NAME, O_RDONLY);  	//open queue readonly
	unlink(PIPE_NAME);
	int pipe;
	pipe = mkfifo(PIPE_NAME, 0666);
	int pipefd;
	pipefd = open(PIPE_NAME, O_WRONLY);

	if (queue == -1) {
		perror(QUEUE_NAME);
		return 1;
	}

	if (pipe == -1) {
			perror(PIPE_NAME);
			return 1;
	}

	//TODO: check attributes with mq_getattr
	for(;;) {
		unsigned int prio = 7;					// int not null to check if it works
		ssize_t read_cnt = mq_receive(queue, line, MAX_MSG_LEN, &prio);
		if (read_cnt == -1) {
			perror(QUEUE_NAME);
			mq_close(queue);
		}
		if (!read_cnt) {
			continue;
		}
		line[read_cnt -1] = '\0';

		printf("prio: %u, converting content to uppercase: %s\n", prio, line);
		for(int i = 0; i < read_cnt; i++) {
		      line[i] = toupper(line[i]);
		}

		printf("redirecting the following content to the pipe: %s\n", line);

		if ((write(pipefd, line, strlen(line)+1)) == -1) {
			fprintf(stderr, "Error in Writing to the pipe: %s", strerror(errno));
		}

		if (!strcmp(line, "QUIT")) {
				break;
			}

	}
	mq_close(queue);
	mq_unlink(QUEUE_NAME);
	return 0;
}
