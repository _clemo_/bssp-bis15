/**
 * \file
 * \brief Messagequeue UE5
 * \author Fabian Friesenecker
 */
#include "share.h"
#include <string.h>
#include <stdio.h>

//for mq_open
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <ctype.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

int main() {

	char line [MAX_MSG_LEN];

	int pipefd;
	if ((pipefd = open(PIPE_NAME, O_RDONLY)) == -1) {
		fprintf(stderr, "Error in opening the Pipe: %s", strerror(errno));
	}

	for (;;) {
		ssize_t read_cnt = read(pipefd, line, MAX_MSG_LEN);
		if (read_cnt == -1) {
			fprintf(stderr, "Error in reading the Pipe: %s", strerror(errno));
			close(pipefd);
		}

		if (!read_cnt) {
			continue;
		}

		line[read_cnt -1] = '\0';

		if (!strcmp(line, "QUIT")) {
			break;
		}
		printf("content: %s\n", line);
	}
}
