/**
 * \file
 * \brief Messagequeue UE5
 * \author Fabian Friesenecker
 */
#include "share.h"
#include <string.h>
#include <stdio.h>

//for mq_open
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <ctype.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

int main() {
	char line [MAX_MSG_LEN];

	//create the mq and handle error.
	struct mq_attr attr = {0, MAX_MSG_COUNT, MAX_MSG_LEN, 0, {0,0,0,0}};		//mq attributes - isso
	mqd_t queue;
	mq_unlink(QUEUE_NAME);
	queue = mq_open(QUEUE_NAME, O_WRONLY | O_CREAT, 0600, &attr);	//only created if not existing
	if (queue == -1) {
		perror(QUEUE_NAME);
		return 1;
	}

	//TODO: check attributes with mq_getattr
	for(;;) {
		int len;
		printf("> ");
		fgets(line, MAX_MSG_LEN, stdin);
		len = strlen(line);
		if (len > 0 && line[len-1] == '\n') {	//insert null if linebreak
			line[len - 1] = '\0';
			len--;								//if we need it later, the len has correct length!
		}
		//send message to message queue.
		if (mq_send(queue, line, strlen(line) + 1, 0) == -1 ) {
			perror(QUEUE_NAME);
			mq_close(queue);
			return 1;
		}
		if (!strcmp(line, "quit")) {
			break;
		}
	}
	mq_close(queue);
	return 0;
}
