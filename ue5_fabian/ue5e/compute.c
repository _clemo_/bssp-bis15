/**
 * \file
 * \brief Messagequeue UE5
 * \author Fabian Friesenecker
 */
#include "share.h"
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <ctype.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>


int main() {
	char line [MAX_MSG_LEN];

	mqd_t queue;
	queue = mq_open(QUEUE_NAME, O_RDONLY);  	//open queue readonly

	unlink(PIPE_CAPITAL_NAME);
	unlink(PIPE_LOWERCASE_NAME);

	int pipeCapital;
	pipeCapital = mkfifo(PIPE_CAPITAL_NAME, 0666);
	int pipeCapitalFD;
	pipeCapitalFD = open(PIPE_CAPITAL_NAME, O_WRONLY);

	int pipeLowercase;
	pipeLowercase = mkfifo(PIPE_LOWERCASE_NAME, 0666);
	int pipeLowercaseFD;
	pipeLowercaseFD = open(PIPE_LOWERCASE_NAME, O_WRONLY);

	if (queue == -1) {
		perror(QUEUE_NAME);
		return 1;
	}
	if (pipeCapital == -1) {
		perror(PIPE_CAPITAL_NAME);
		return 1;
	}
	if (pipeLowercase == -1) {
		perror(PIPE_LOWERCASE_NAME);
		return 1;
	}


	//TODO: check attributes with mq_getattr
	for(;;) {
		unsigned int prio = 7;					// int not null to check if it works
		ssize_t read_cnt = mq_receive(queue, line, MAX_MSG_LEN, &prio);
		if (read_cnt == -1) {
			perror(QUEUE_NAME);
			mq_close(queue);
		}
		if (!read_cnt) {
			continue;
		}
		line[read_cnt -1] = '\0';

		printf("redirecting the following content to the pipe-lowercase: %s\n", line);

		if ((write(pipeLowercaseFD, line, strlen(line)+1)) == -1) {
			fprintf(stderr, "Error in Writing to the pipe-lowercase: %s", strerror(errno));
		}

		printf("converting content to uppercase: %s\n", line);
		for(int i = 0; i < read_cnt; i++) {
		      line[i] = toupper(line[i]);
		}

		printf("redirecting the following content to the pipe-capital: %s\n", line);

		if ((write(pipeCapitalFD, line, strlen(line)+1)) == -1) {
			fprintf(stderr, "Error in Writing to the pipecapital: %s", strerror(errno));
		}

		if (!strcmp(line, "QUIT")) {
				break;
			}

	}
	mq_close(queue);
	mq_unlink(QUEUE_NAME);
	return 0;
}
