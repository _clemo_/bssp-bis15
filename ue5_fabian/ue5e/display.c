/**
 * \file
 * \brief Messagequeue UE5
 * \author Fabian Friesenecker
 */
#include "share.h"
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <ctype.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

int main(int argc, char **argv) {

	char line [MAX_MSG_LEN];
	int display;
	display = atoi(argv[1]);
	int pipefd;

	if (display == 1) {
		if ((pipefd = open(PIPE_CAPITAL_NAME, O_RDONLY)) == -1) {
			fprintf(stderr, "Error in opening the Pipe: %s", strerror(errno));
		}

		for (;;) {
			ssize_t read_cnt = read(pipefd, line, MAX_MSG_LEN);
			if (read_cnt == -1) {
				fprintf(stderr, "Error in reading the Pipe: %s", strerror(errno));
				close(pipefd);
			}

			if (!read_cnt) {
				continue;
			}

			line[read_cnt -1] = '\0';

			if (!strcmp(line, "QUIT")) {
				break;
			}
			printf("content: %s\n", line);
		}
	} else if (display == 2) {
		if ((pipefd = open(PIPE_LOWERCASE_NAME, O_RDONLY)) == -1) {
			fprintf(stderr, "Error in opening the Pipe: %s", strerror(errno));
		}

		for (;;) {
			ssize_t read_cnt = read(pipefd, line, MAX_MSG_LEN);
			if (read_cnt == -1) {
				fprintf(stderr, "Error in reading the Pipe: %s", strerror(errno));
				close(pipefd);
			}

			if (!read_cnt) {
				continue;
			}

			line[read_cnt -1] = '\0';

			if (!strcmp(line, "quit")) {
				break;
			}
			printf("content: %s\n", line);
		}
	} else {
		printf("Please specify either display 1 or 2 as argument!");
	}
}
