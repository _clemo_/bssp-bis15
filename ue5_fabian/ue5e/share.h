/**
 * \file
 * \brief Shared Lib for Messagequeue UE5
 * \author Fabian Friesenecker
 */
#ifndef SHARE_H_
#define SHARE_H_

#define MAX_MSG_LEN 256
#define MAX_MSG_COUNT 10
#define QUEUE_NAME "/is141011_mq"
#define PIPE_CAPITAL_NAME "/tmp/is141011_capital" //absolute path
#define PIPE_LOWERCASE_NAME "/tmp/is141011_lowercase" //absolute path

#endif
