/**
 * \file
 * \brief Kernel-Module with ioctl and proc support
 * \author Fabian Friesenecker is141011
 *
 * Kernel-Module UE6 + UE7 + UE8
 *
 */

#define DEBUG

#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/version.h>
#include <linux/errno.h>
#include <linux/printk.h>
#include <linux/moduleparam.h>
#include <linux/proc_fs.h>
#include <linux/types.h>
#include <linux/cdev.h>
#include <linux/semaphore.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <linux/completion.h>
#include <linux/jiffies.h>
#include <linux/time.h>
#include <linux/delay.h>

#include <linux/seq_file.h>

#include "my_ioctl.h"

/**
 * Module License has not to be missing here!
 */
MODULE_LICENSE("Dual BSD/GPL");

/**
  \def DRVNAME
  Was ist das hier? WTF?
  \def BUFFER_SIZE
  Größe des Buffers in jedem Gerät. Für KMALLOC erforderlich.
  \def MINOR_START
  Bei welcher Nummer die Gerätedateien starten sollen. (Ein geladener Treiber macht überlicherweise ein Major-Number und mehrer Minor-Nummern)
  \def MINOR_COUNT
  Wieviele Untergeräte sollen sollen angelegt werden. (mydev0, mydev1, mydev2, mydev3, mydev4)
*/
#define DRVNAME KBUILD_MODNAME
#define BUFFER_SIZE 1024
#define MINOR_START 0
#define MINOR_COUNT 5

#define MIN_VALUE_OF(a,b) ( (a) > (b) ? (b) : (a))  ///< returns the smaller one of two values

#define PROC_DIR_NAME "is141011"
#define PROC_FILE_NAME "info"

//Funktionsprototypen.
static int mydev_open(struct inode *inode, struct file *filp);
static int mydev_release(struct inode *inode, struct file *filp);
static ssize_t mydev_read(struct file *filp, char __user *buff, size_t count, loff_t *offset);
static ssize_t mydev_write(struct file *filp, const char __user *buff, size_t count, loff_t *offset);
long mydev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);


static int __init cdrv_init(void);
static void __init cdrv_exit(void);

/**
 * Function to call at the kernel module insertion (entry point only on insertion)
 */
module_init(cdrv_init);
/**
 * Function to call at the kernel modul unload (entry point only on unload)
 */
module_exit(cdrv_exit);

/**
 * Prototypes for PROC Interface
 */
static int my_seq_file_open(struct inode* inode, struct file* filp);
static void* my_start(struct seq_file* sf, loff_t* pos);
static void* my_next(struct seq_file* sf, void* v, loff_t* pos);
static void my_stop(struct seq_file* sf, void* it);
static int my_show(struct seq_file* sf, void* it);

static struct proc_dir_entry* proc_parent = NULL; // /proc/is141011
static struct proc_dir_entry* proc_entry = NULL;  // /proc/is141011/info

/**
 * struct my_cdev existiert pro device genau einmal. beinhaltet den buffer, die semaphoren und alles was im "gerät" abgespeichert werden soll.
 */
typedef struct my_cdev {
	char *buffer; 							//< zeigt auf den buffer mit 1024 bytes
	int curentBufferSize;					//< addon Prüfung: hält den Wert der aktuellen Buffergröße.
	int current_length;						//< zähler des aktuellen pufferstand
	int inUse; 								//< ist das Gerät gerade "in Use"
	struct cdev cdev;						//< die wird vom kernel benötigt
	struct semaphore sync;					//< Semaphore zum Syncen wann immer Device-Zugriff besteht.
	int readOpenCount;						//< Counts how often the device is opened at the moment FOR READ!
	int isWriteOpened;						//< "bool" to check if the device is currently opened to write.
	int isAppendOpened;						//< addon prüfung: check if O_APPEND was requested.
	int deviceNumber;						//< dirtyHack to count the device number. To have it EASY by hand!
	wait_queue_head_t waitForRead; 			//< used for blocking read/write
	wait_queue_head_t waitForIOCTLclear; 	//< @see https://www.kernel.org/doc/Documentation/scheduler/completion.txt
	int maxAllowedReadOpens;				//< Number of allowed open handles on the driver. <=0 no limit.

	unsigned long long totalOpenCount;		//< total count of open syscalls
	unsigned long long  totalCloseCount;	//< total count of close syscalls
	unsigned long long  totalReadCount;		//< total count of read syscalls
	unsigned long long  totalWriteCount;	//< total count of write syscalls
	unsigned long long  totalClearCount;	//< total count of clear syscalls

	struct timespec64 totalTimeRead;		//< total time consumed for read operations
	struct timespec64 totalTimeWrite;		//< total time consumed for write operations

	} my_cdev_t;

static struct class *my_class;		//< class needed for device_create (and class_create)
static my_cdev_t *my_devs;			//<	array aus mehreren "my_cdev_t" - für jedes minor-device wird eines angelegt.

/**
 * init function
 * @return 0 for success. other for error.
 */
static int __init cdrv_init(void) {
	int i, j;
	signed long error_code; 				// because of ERR_PTR and IS_ERR: see http://lxr.free-electrons.com/source/tools/virtio/linux/err.h#L17
	dev_t first_device;						// needed for cleanup of region down in goto sections
	struct device* current_device = NULL;  	// for error handling later on then outside of the init loop

	/**
	 * @brief initialize file_operations mydev_fcalls
	 * make this struct fucking static - COSTED us 2h to find the error.
	 * otherwise pointers to our calls will be thrown away after the init ran.
	 */
	static struct file_operations mydev_fcalls =  {
		.owner = THIS_MODULE,
		.open = mydev_open,
		.release = mydev_release,
		.read = mydev_read,
		.write = mydev_write,
		.unlocked_ioctl = mydev_ioctl
	};

	static struct file_operations myproc_fcalls ={
		.owner = THIS_MODULE,
		.open = my_seq_file_open,
		.read = seq_read,
		.llseek = seq_lseek,
		.release = seq_release
	};


	pr_info("cdrv: Hello Kernhell. Starting Satan initialization...\n");

	error_code = alloc_chrdev_region(&first_device, MINOR_START, MINOR_COUNT, DRVNAME);
	if (error_code) {
		pr_devel("cdrv: allog_chrdev_region failed.\n");
		return error_code;
	}

	pr_devel("cdrv: major %d, minor %d\n", MAJOR(first_device), MINOR(first_device));

	//Allocate Memory for our struct as much as needed for all minor devices. my_cdev_t times the MINOR_COUNT
	my_devs = kmalloc(sizeof(my_cdev_t) * MINOR_COUNT, GFP_KERNEL);
	if ( my_devs == NULL ) {
		//TODO ERRORHANDLING
		error_code = -ENOMEM;
		pr_alert("cdrv: Error in Allocating Memory for all Device-Structs!\n");
		goto fail_devs_allocated;
	}

	//create a class (directory) in /sys/class
	my_class = class_create(THIS_MODULE, "my_driver_class");
	if (IS_ERR(my_class)) {
		error_code = (signed long) my_class;
		pr_alert("cdrv: could not create class %s - trying to kfree...\n", "my_driver_class");
		goto fail_class_created;
	}

	proc_parent = proc_mkdir(PROC_DIR_NAME, NULL);
	if(proc_parent == NULL){
		pr_warn("cdrv: init: create proc failed\n");
		goto fail_proc_dir_created;
	}

	proc_entry = proc_create_data(PROC_FILE_NAME, 0664, proc_parent, &myproc_fcalls, NULL);
	if(proc_entry == NULL){
		pr_warn("cdrv: init: create proc-data failed\n");
		goto fail_cdevs_created;
	}

	// init all minors
	for (i = 0; i < MINOR_COUNT; i++) {
		dev_t cur_devnr = MKDEV(MAJOR(first_device), MINOR(first_device) + i);

		cdev_init(&my_devs[i].cdev, &mydev_fcalls);		// pro device syscalls registrieren

		my_devs[i].cdev.owner = THIS_MODULE;			// ist nötig für kernel

		// init our own structure and elements
		my_devs[i].curentBufferSize = BUFFER_SIZE;		// init mit 1024 bytes für den Anfang.
		my_devs[i].current_length = 0;
		my_devs[i].buffer = NULL;
		my_devs[i].inUse = 0;							// Initialize as not-in-use
		my_devs[i].readOpenCount = 0;
		my_devs[i].isWriteOpened = 0;
		my_devs[i].deviceNumber = i;
		my_devs[i].maxAllowedReadOpens = 0;
		my_devs[i].isAppendOpened = 0;

		//init counters
		my_devs[i].totalClearCount  = 0;
		my_devs[i].totalCloseCount = 0;
		my_devs[i].totalOpenCount  = 0;
		my_devs[i].totalReadCount = 0;
		my_devs[i].totalWriteCount = 0;

		//init timers
		my_devs[i].totalTimeRead.tv_nsec = 0;
		my_devs[i].totalTimeRead.tv_sec = 0;
		my_devs[i].totalTimeWrite.tv_nsec = 0;
		my_devs[i].totalTimeWrite.tv_sec = 0;

		// stuff needed for the waitqueue
		init_waitqueue_head(&my_devs[i].waitForRead);
		init_waitqueue_head(&my_devs[i].waitForIOCTLclear);

		sema_init(&my_devs[i].sync, 1);

		current_device = device_create(my_class, NULL, cur_devnr, NULL, "mydev%d", MINOR(cur_devnr));	//aktuelles device createn und dev_t einfüllen in current-device (außerhalb der schleife definiert)
		if( IS_ERR(current_device) ) {
			error_code = (signed long) current_device;
			pr_alert("cdrv: Device creation for minor %d failed! Trying to device_destroy and cdev_del ALL THE THINGS. \n", MINOR(cur_devnr));
			goto fail_cdevs_created;
		}

		error_code = cdev_add(&my_devs[i].cdev, cur_devnr, 1); // adds the device to the system. make it "live".
		if ( error_code < 0 ) {
			pr_alert("cdrv: Could not add device %d to the system! Trying to device_destroy \n", i);
			goto fail_cdevs_add;
		}
	}

	pr_info("cdrv: Modules installed and loaded successfull. Devices created.\n");
	return 0;


// cleanups and rollbacks - no way found to test them until now!
// this are all the fail-conditions. ULTRA MAGIC LOGIC from CLEMOO involved here :-)
fail_cdevs_add:
	device_destroy(my_class, my_devs[i].cdev.dev);
	pr_devel("cdrv: fail_cdevs_add cleanup finished \n");

fail_cdevs_created:
	for(j = 0; j <= i; ++j){
		if (j < i) 		// bis i-1 müssen alle destroyed werden, aber deleted dürfen dann alle.
			device_destroy(my_class, my_devs[j].cdev.dev);
		cdev_del(&my_devs[j].cdev);
	}
	pr_devel("cdrv: fail_cdevs_created cleanup finished \n");
	remove_proc_entry(PROC_FILE_NAME, proc_parent);
	pr_devel("cdrv: remove_proc_entry cleanup finished \n");

fail_proc_dir_created:
	remove_proc_entry(PROC_DIR_NAME, NULL);

fail_class_created:
	class_destroy(my_class);

fail_devs_allocated:
	kfree(my_devs);
	my_devs = NULL;
	pr_devel("cdrv: fail_devs_allocated cleanup finished \n");

return error_code;
}



static void __exit cdrv_exit(void){
	int i;
	pr_devel("cdrv: Goodbye, Kernhell! Let's hope that Satan does not reincarnate!\n");
	for (i = 0; i < MINOR_COUNT; i++) {
		device_destroy(my_class, my_devs[i].cdev.dev);
		cdev_del(&my_devs[i].cdev);
		if (my_devs[i].buffer) {
			kfree(my_devs[i].buffer);
		}
		pr_devel("cdrv: cleanup device /dev/mydev%d. \n", i);
	}

	remove_proc_entry(PROC_FILE_NAME, proc_parent);
	remove_proc_entry(PROC_DIR_NAME, NULL);

	class_destroy(my_class);
	unregister_chrdev_region(my_devs[0].cdev.dev, MINOR_COUNT);
	kfree(my_devs);
	pr_devel("cdrv: cleanup my character driver %s", DRVNAME);
}

static int mydev_open(struct inode *inode, struct file *filp) {
	// zeiger auf meine device struktur. berechnet mit container_of
	my_cdev_t *dev = container_of(inode->i_cdev, my_cdev_t, cdev);

	// damit ich später bei read und write darauf zugreifen kann (weil kein inode als parameter)
	filp->private_data = dev;
	pr_devel("cdrv: DBG: fill dev* with container_of and query *dev and filp.\n");

	// will auf device zugreifen... sem anfordern als erstes .. down_interruptible kann dazu zum block fuehren bis der andere mit open fertig ist
	if (down_interruptible(&dev->sync)) {
		return -ERESTARTSYS;
	}
	dev->totalOpenCount += 1;

	pr_devel("cdrv: DBG: fetched semaphore.\n");

	if (filp->f_mode & FMODE_WRITE) {
		// return if device is busy
		pr_devel("cdrv: Open called and inUse is: %d", dev->inUse);
		if (dev->inUse == 1) {
			pr_devel("cdrv: Open called but device is still write-locked!\n");
			up(&dev->sync);
			return -EBUSY;
		}
		// set device to busy
		dev->inUse = 1;
		pr_devel("cdrv: Device write-locked and inUse is: %d \n", dev->inUse);

		// ab hier kann ich aufs device zugreifen
		if (dev->buffer == NULL) {
			dev->buffer = kmalloc(dev->curentBufferSize, GFP_KERNEL);

			if (dev->buffer == NULL){
				pr_alert("cdrv: could not allocate memory\n");
				up(&dev->sync);
				return -ENOMEM;
			}
		}

		pr_devel("cdrv: DBG: write opened and ggf. malloced.\n");
		dev->isWriteOpened = 1;				//set the opened for isWriteOpened to 1!

		//add-on: prüfung checken ob O_APPEND gesetzt ist.
		if (filp->f_flags & O_APPEND) {
			pr_devel("cdrv: O_APPEND is set for write!\n");
			dev->isAppendOpened = 1;
		}
	} else {
		if (dev->buffer == NULL) {
			pr_alert("cdrv: /dev/mydev%d was never opened for write before and inUse is: %d \n", dev->deviceNumber, dev->inUse);
			up(&dev->sync);
			return -ENOSR;
		}
	}

	//don't forget to increment the "how many times open for read" counter.
	if (filp->f_mode & FMODE_READ) {
		// implement the open-limit-for-read enforcement here:
		if ( (dev->maxAllowedReadOpens <= 0) || (dev->readOpenCount <= dev->maxAllowedReadOpens) ) {
			dev->readOpenCount++;
			pr_devel("cdrv: Device /dev/mydev%d opened. Opened for ReadOnly currently %d times. Is write openend: %s. Device ready to use.\n",dev->deviceNumber, dev->readOpenCount, (dev->isWriteOpened == 1 ? "Yes" : "No") );
			up(&dev->sync);
			pr_devel("cdrv: DBG: sema given away.\n");
			return 0;
		} else {
			up(&dev->sync);
			return -EBUSY;
		}
	}

	up(&dev->sync);
	return 0;
}

static int mydev_release(struct inode *inode, struct file *filp) {
	my_cdev_t *dev = filp->private_data;

	pr_info("cdrv: close called!");

	if (down_interruptible(&dev->sync)){
			return -ERESTARTSYS;
	}
	dev->totalCloseCount++;

	// set device to "free to write" only if it was in write use.
	if (filp->f_mode & FMODE_WRITE) {
		pr_devel("cdrv: Close called! Previous Syscall was RW and inUse is: %d and isWriteOpened is: %d", dev->inUse, dev->isWriteOpened);
		dev->inUse = 0;
		dev->isWriteOpened = 0;
		pr_devel("cdrv: Device unlocked and inUse is: %d and isWriteOpened reset to %d \n", dev->inUse, dev->isWriteOpened);
	} else {
		pr_devel("cdrv: Close called! Previous Syscall was ReadOnly and inUse is: %d. Device still opened for read %d times.", dev->inUse, dev->readOpenCount);
	}

	if (filp->f_mode & FMODE_READ) {
			dev->readOpenCount--;
	}

	up(&dev->sync);
	return 0;
}

static ssize_t mydev_read(struct file *filp, char __user *buff, size_t count, loff_t *offset) {
	my_cdev_t *dev = filp->private_data;
	int to_copy;
	int allowed_count;
	int not_copied;
	int length;
	int read_bytes;
	int total_read_bytes = 0;
	long returnOfWaitEvent;

	//timestamp stuff
	struct timespec64 timeStampStart;
	struct timespec64 timeStampStop;
	struct timespec64 ts;

	if (down_interruptible(&dev->sync)) {
		pr_warn("cdrv: read: could not get semaphore\n");
		return -ERESTARTSYS;
	}

	timeStampStart = current_kernel_time();

#ifdef DEBUG
	udelay(100L);
#endif

	dev->totalReadCount += 1;

	if (buff == NULL) {
		timeStampStop = current_kernel_time();
		dev->totalTimeRead = timespec_add((timespec_sub(timeStampStop, timeStampStart)), dev->totalTimeRead);
		up(&dev->sync);
		return -EINVAL;
	}

	up(&dev->sync);

	while( count != 0 ) {
		if (down_interruptible(&dev->sync)) {
			timeStampStop = current_kernel_time();
			dev->totalTimeRead = timespec_add((timespec_sub(timeStampStop, timeStampStart)), dev->totalTimeRead);
			return -ERESTARTSYS;
		}

		// out of bounds? (OFFSET wise)
		if( *offset > dev->current_length ) {
			up(&dev->sync);
			timeStampStop = current_kernel_time();
			dev->totalTimeRead = timespec_add((timespec_sub(timeStampStop, timeStampStart)), dev->totalTimeRead);
			return total_read_bytes;
		}

		allowed_count = dev->current_length - *offset;					// wie viel zeichen duerfen gelesen werden... in der curr_leng steht wie viel gerade im buffer steht
		to_copy = (allowed_count < count) ? allowed_count : count;

		if( to_copy == 0 ) {	// grad nix zum lesen da
			if( (filp->f_flags & O_NDELAY) || dev->isWriteOpened == 0 ) {	// wir warten nicht weil 1. NODELAY gesetzt oder "es wird nix kommen."
				pr_info("cdrv: mydev_read(): O_NDELAY specified, returning EOF.\n");
				timeStampStop = current_kernel_time();
				dev->totalTimeRead = timespec_add((timespec_sub(timeStampStop, timeStampStart)), dev->totalTimeRead);
				up(&dev->sync);
				return total_read_bytes;
			} else {			// O_NDELAY not set - so going to sleep until there is something
				up(&dev->sync);
				ts = current_kernel_time();
				pr_devel("cdrv: mydev_read(): good night! Timestamp: %ld, nsec: %09ld\n", ts.tv_sec, ts.tv_nsec);
				// aufwachen unter folgender bedingung: *offset hat sich geändert ODER der Writer eingegangen ist. Es wird nicht gewartet bis zum Sanktnimmerleinstag, weil dann ja isWriteOpened 0 wird irgendwann....
				returnOfWaitEvent = wait_event_interruptible(dev->waitForRead, ( *offset != dev->current_length) || (dev->isWriteOpened == 0) );
				if( returnOfWaitEvent ) {
					timeStampStop = current_kernel_time();
					if (down_interruptible(&dev->sync)) {
						return -ERESTARTSYS;
					}
					dev->totalTimeRead = timespec_add((timespec_sub(timeStampStop, timeStampStart)), dev->totalTimeRead);
					up(&dev->sync);
					return returnOfWaitEvent;	//wait_event_interruptible könnte eine ERRNO zurückliefern (lt. Clemoo)
				}
				ts = current_kernel_time();
				pr_info("cdrv: mydev_read(): good morning! Timestamp: %ld, nsec: %09ld\n", ts.tv_sec, ts.tv_nsec);
				continue;
			}
		}

		//aber hier muss irgendwas im puffer zum lesen sein
		pr_info("cdrv: mydev_read(): copy_to_user %d bytes\n", to_copy);

		not_copied = copy_to_user(buff, dev->buffer + *offset, to_copy);		// lese buffer adresse + offset ..
		read_bytes = to_copy - not_copied;

		length = *offset + read_bytes;	//aktuelle position nach dem read

		//damit wieder die current_length wieder stimmt
		if (dev->current_length < length) {
			dev->current_length = length;
		}

		timeStampStop = current_kernel_time();
		dev->totalTimeRead = timespec_add((timespec_sub(timeStampStop, timeStampStart)), dev->totalTimeRead);
		up(&dev->sync);

		*offset += read_bytes;		// genaue anzahl .. fileposition nachziehen, damit es wieder stimmt fuer das naechste mal

		total_read_bytes += read_bytes;
		count -= read_bytes;
	}
	pr_info("cdrv: read has finished and %d bytes were read from device.\n", read_bytes);
	return total_read_bytes;
}

static ssize_t mydev_write(struct file *filp, const char __user *buff, size_t count, loff_t *offset) {
	my_cdev_t *dev = filp->private_data;

	int to_copy;
	int allowed_count;
	int not_copied;
	int length;
	int written_bytes = 0;
	int total_written_bytes = 0;
	long returnOfWaitEvent;

	//timestamp stuff
	struct timespec64 timeStampStart;
	struct timespec64 timeStampStop;
	struct timespec64 ts;

	if (down_interruptible(&dev->sync)) {
		return -ERESTARTSYS;
	}

	timeStampStart = current_kernel_time();

#ifdef DEBUG
	udelay(100L);
#endif

	dev->totalWriteCount += 1;

	if (buff == NULL) {
		timeStampStop = current_kernel_time();
		dev->totalTimeWrite = timespec_add( (timespec_sub(timeStampStop, timeStampStart)), dev->totalTimeWrite);
		up(&dev->sync);
		return -EINVAL;
	}

	up(&dev->sync);

	while( count != 0 ) {
		if (down_interruptible(&dev->sync)) {
			return -ERESTARTSYS;
		}
		//add-on Prüfung: move the offset to the current length - safety check further down.
		if (dev->isAppendOpened == 1) {
			*offset = dev->current_length;
		}

		pr_info("cdrv: Write called! Current offset is: %lld and count is %zd\n", *offset, count);
		allowed_count = dev->curentBufferSize - *offset;		//Prüfnug: if offset is set to current_length and it is already 1024, the allowed_count is zero anyway...
		to_copy = (allowed_count < count) ? allowed_count : count;
		pr_devel("cdrv: DBG: allowed_count=%d, count=%zd, to_copy=%d", allowed_count, count, to_copy);

		if( to_copy == 0 ) {					// es gibt nix zum schreiben (mehr)
			if( filp->f_flags & O_NDELAY ) {	// wir warten nicht und tschüssi
				timeStampStop = current_kernel_time();
				dev->totalTimeWrite = timespec_add( (timespec_sub(timeStampStop, timeStampStart)), dev->totalTimeWrite);
				up(&dev->sync);
				return total_written_bytes;
			} else {
				up(&dev->sync);
				ts = current_kernel_time();
				pr_devel("cdrv: mydev_write(): good night! Timestamp: %ld, nsec: %09ld\n", ts.tv_sec, ts.tv_nsec);
				// Wenn Bufferlänge gleich current_length, dann ist der Buffer offenbar voll, und wir dürfen nur aufwachen, wenn der Buffer wieder ungleich length - also Bedingung.)
				returnOfWaitEvent = wait_event_interruptible(dev->waitForIOCTLclear, BUFFER_SIZE != dev->current_length);
				if( returnOfWaitEvent ) {
					timeStampStop = current_kernel_time();
					if (down_interruptible(&dev->sync)) {
						return -ERESTARTSYS;
					}
					dev->totalTimeWrite = timespec_add( (timespec_sub(timeStampStop, timeStampStart)), dev->totalTimeWrite);
					up(&dev->sync);
					return returnOfWaitEvent;
				}
				ts = current_kernel_time();
				pr_devel("cdrv: mydev_write(): good morning! Timestamp: %ld, nsec: %09ld\n", ts.tv_sec, ts.tv_nsec);
				*offset = 0;//offset hier NULLEN, weil ja aufgeweckt wird er nur, wenn ioctl clear gelaufen ist. ;-)
				continue;
			}
		}

		//unsigned long copy_from_user (	void *  	to,		const void __user *  	from,	 	unsigned long  	n);
		pr_devel("cdrv: DBG: Values before Memmove: devBuffer: %p, userBuffer: %p, copied_total: %d, to_copy: %d\n", dev->buffer, buff, total_written_bytes, to_copy);
		not_copied = copy_from_user(dev->buffer + *offset, buff + total_written_bytes, to_copy);
		written_bytes = to_copy - not_copied;

		length = *offset + written_bytes;		//aktuelle position nach dem write

		if (dev->current_length < length) {		//damit die current_length wieder stimmt
			dev->current_length = length;
		}

		*offset += (written_bytes);				// auch der offset muss erhöht werden

		timeStampStop = current_kernel_time();
		dev->totalTimeWrite = timespec_add( (timespec_sub(timeStampStop, timeStampStart)), dev->totalTimeWrite);

		up(&dev->sync);							// semaphore früher abgeben damit aufgeweckte reader sie gleich holen können...

		//fabi: kostet performance, aber output soll WIRKLICH VORHER kommen, vor da irgendwer wieder was liest.
		pr_info("cdrv: write was called and %d Bytes were written to device. Current Offset is now: %lld \n", written_bytes, *offset);

		total_written_bytes += written_bytes;
		count -= written_bytes;
	}
	pr_info("cdrv: wrote %d bytes\n", total_written_bytes);
	if (total_written_bytes > 0) {
		wake_up(&dev->waitForRead);
		pr_info("cdrv: mydev_write(): wake_up waitQueueRead\n");
	}
	return total_written_bytes;
}

long mydev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg) {
	my_cdev_t *dev = filp->private_data;
	int TEMPreadOpenCount;
	int TEMPisWriteOpened;
	int oldReadLimit = 0;				// temp variable
	int oldBufferSize = 0;				// addon Prüfung: temp variable

	pr_info("cdrv: ioctl called: got cmd %u\n", cmd);
	if (_IOC_TYPE(cmd) != IOC_NR_TYPE) {
		pr_info("cdrv: --> ioctl(): got wrong magic! was %u instead of 'z' = 172\n", _IOC_TYPE(cmd));
		return -EINVAL;
	}

	switch (_IOC_NR(cmd)) {
		case IOC_NR_OPENREADCNT:
			if (down_interruptible(&dev->sync)) {
				return -ERESTARTSYS;
			}
			pr_devel("cdrv: --> ioctl(): readOpenCount of /dev/mydev%d requested from the user space!\n", dev->deviceNumber);
			TEMPreadOpenCount = dev->readOpenCount;
			up(&dev->sync);
			return TEMPreadOpenCount;
		case IOC_NR_OPENWRITECNT:
			pr_devel("cdrv: --> ioctl(): isWriteOpened requested from user space!\n");
			if (down_interruptible(&dev->sync)) {
				return -ERESTARTSYS;
			}
			TEMPisWriteOpened = dev->isWriteOpened;
			up(&dev->sync);
			return TEMPisWriteOpened;
		case IOC_NR_CLEAR:
			pr_devel("cdrv: --> ioctl(): flush buffer requested from user space!\n");
			if (down_interruptible(&dev->sync)) {
				return -ERESTARTSYS;
			}
			if (dev->buffer != NULL) {
				dev->current_length = 0;
				dev->totalClearCount += 1;
				wake_up_all(&dev->waitForIOCTLclear);
				pr_info("cdrv: ioctl(): wake_up waitForIOCTLclear\n");
				up(&dev->sync);
				return 0;
			}
			break;
		case IOC_NR_MAX_READERS:
			if (down_interruptible(&dev->sync)) {
				return -ERESTARTSYS;
			}
			oldReadLimit = dev->maxAllowedReadOpens;
			dev->maxAllowedReadOpens = arg;
			pr_devel("cdrv: --> ioctl(): change of maximum allowed readers requested. New maxAllowedReadOpens are %d, return old number to ioctl: %d.\n", dev->maxAllowedReadOpens, oldReadLimit);
			up(&dev->sync);
			return oldReadLimit;
		//ADDON: Prüfung
		case IOC_NR_HALF_SIZE:
			if (down_interruptible(&dev->sync)) {
				return -ERESTARTSYS;
			}
			oldBufferSize = dev->curentBufferSize;
			dev->curentBufferSize = dev->curentBufferSize/2; 		// half the buffer size
			if (dev->curentBufferSize < 1) {		// undo the operation, if buffer would drop below 1 byte
				dev->curentBufferSize = oldBufferSize;
				pr_devel("cdrv: --> ioctl(): half of the Buffer requested but buffersize would go below 1 byte.");
				return -1;
			}
			pr_devel("cdrv: --> ioctl(): half of the Buffer requested. New curentBufferSize is %d, return oldBufferSize to ioctl: %d.\n", dev->curentBufferSize, oldBufferSize);

			if (dev->buffer != NULL) {
				kfree(dev->buffer);		//delete the buffer
				dev->buffer = kmalloc(dev->curentBufferSize, GFP_KERNEL);  //recreate the buffer with new size
				if (dev->buffer == NULL){
					pr_alert("cdrv: could not allocate memory\n");
					up(&dev->sync);
					return -ENOMEM;
				}
				dev->current_length = 0;							// clear the buffer
				wake_up_all(&dev->waitForIOCTLclear);
				pr_info("cdrv: ioctl(): wake_up waitForIOCTLclear\n");  //wake up waiting writers
				return oldBufferSize;
			} else {
				pr_alert("cdrv: --> ioctl(): /dev/mydev%d was never opened for write before, therefore no buffer is allocated. only setting the limit.", dev->deviceNumber);
			}
			break;
		//ADDON: Prüfung
		case IOC_NR_DOUBLE_SIZE:
			if (down_interruptible(&dev->sync)) {
				return -ERESTARTSYS;
			}
			oldBufferSize = dev->curentBufferSize;
			dev->curentBufferSize = dev->curentBufferSize*2; 		// addon Prüfung: double the buffer size in the device struct

			pr_devel("cdrv: --> ioctl(): double of the Buffer requested. New curentBufferSize is %d, return oldBufferSize to ioctl: %d.\n", dev->curentBufferSize, oldBufferSize);

			if (dev->buffer != NULL) { //reallocation is needed
				kfree(dev->buffer);		//delete the buffer
				dev->buffer = kmalloc(dev->curentBufferSize, GFP_KERNEL);  //recreate the buffer with new size
				if (dev->buffer == NULL){
					pr_alert("cdrv: could not allocate memory\n");
					up(&dev->sync);
					return -ENOMEM;
				}
				dev->current_length = 0;	//clear the buffer
				wake_up_all(&dev->waitForIOCTLclear);
				pr_info("cdrv: ioctl(): wake_up waitForIOCTLclear\n");  //wake up waiting writers
			} else {
				pr_alert("cdrv: --> ioctl(): /dev/mydev%d was never opened for write before, therefore no buffer is allocated. only setting the limit.", dev->deviceNumber);
			}
			break;
		default:
			pr_alert("cdrv: --> ioctl(): unknown ioctl command %d\n", cmd);
			up(&dev->sync);
			return -EINVAL;
		}

	up(&dev->sync);
	return 0;
}

static int my_seq_file_open(struct inode* inode, struct file* filp){
	static struct seq_operations my_seq_ops = {
		.start = my_start,
		.next = my_next,
		.stop = my_stop,
		.show = my_show
	};

	return seq_open(filp, &my_seq_ops);
}

static void* my_start(struct seq_file* sf, loff_t* pos){
	pr_info("cdrv: my_start: start\n");
	pr_devel("cdrv: my_start: pos=%lld\n", *pos);
	if(*pos == 0){
		return my_devs;
	}

	return NULL;
}

static void* my_next(struct seq_file* sf, void* v, loff_t* pos){
	pr_info("cdrv: my_next: next\n");
	(*pos)++;
	if( *pos >= MINOR_COUNT ) {
		return NULL;
	}
	return &my_devs[*pos];
}

/**
 * @brief   my_show
 * @param   sf
 * @param   it points of result of my_next --> points to current device
 * @returns
 */
static int my_show(struct seq_file* sf, void* it){
	my_cdev_t* dev = (my_cdev_t*) it;
	int index = MINOR(dev->cdev.dev);

	// index:
	// 1) MINOR
	// 2) dev-my_devs
	pr_devel("cdrv: my_show: current index: %d\n", index);

	if (down_interruptible(&dev->sync)) {
		return -ERESTARTSYS;
	}

	if(index == 0){
			seq_printf(sf, "DEVICE-NAME | LENGTH | READ | WRITE | OPEN | CLOSE | CLEAR | Readtime(s) | Writetime(s) | OpenRLimit | Buffsize\n");
			seq_printf(sf, "------------+--------+------+-------+------+-------+-------+-------------+--------------+------------+----------\n");
	}
	seq_printf(sf,   "mydev%d      | %6d | %5llu | %4llu | %4llu | %5llu | %5llu | %7ld,%03ld | %8ld,%03ld | %10d | %8d \n",
				index, dev->current_length, dev->totalReadCount, dev->totalWriteCount,
				dev->totalOpenCount, dev->totalCloseCount, dev->totalClearCount, dev->totalTimeRead.tv_sec, dev->totalTimeRead.tv_nsec/1000000,
				dev->totalTimeWrite.tv_sec, dev->totalTimeWrite.tv_nsec/1000000, dev->maxAllowedReadOpens, dev->curentBufferSize);
	up(&dev->sync);
	return 0;
}

static void my_stop(struct seq_file* sf, void* it){
	pr_info("cdrv: my_stop: stop\n");
	// possible cleanups here -> not neccessary here
}
